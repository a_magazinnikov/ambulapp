﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AmbulApp.Module.BusinessObjects;
using AmbulApp.Module.BusinessLogic;

namespace AmbulApp.Module.Win.Controls
{
    public partial class A05 : A0Base
    {
        private Check targerSeqCheck;

        public A05(Check CurrentCheck, List<Check> AllChecks) : base(CurrentCheck, AllChecks)
        {
            InitializeComponent();
            this.UpdateLayoutItemsFontSize(this.MainLayout);
            targerSeqCheck = _AllChecks.Where(chk => chk.Sequence == CurrentCheck.TargetSequence).FirstOrDefault();
            if (targerSeqCheck == null)
                throw new Exception($"The check with the sequence{CurrentCheck.Sequence} has a wrong or missing TargetSequence");

            targerSeqCheck.Changed += TargerSeqCheck_Changed;
            this.txtAverage.Text = CurrentCheck.TargetValue.ToString();
        }

        protected virtual void NewProcess()
        {
            try
            {
                double LoP;
                double HiP;

                Check RefencedCheck = targerSeqCheck;
                List<int> RuntimeText = new List<int>();
                RuntimeText = new List<int>(Array.ConvertAll(RefencedCheck.RunTimeText.Split(','), new Converter<string, int>((s) => { return Convert.ToInt32(s); })));

                List<int> ValueInputs = new List<int>(Array.ConvertAll(CurrentCheck.ValueInputs.Split(','), new Converter<string, int>((s) => { return Convert.ToInt32(s); })));

                List<double> IndexValues = new List<double>();
                foreach (var index in ValueInputs)
                {
                    IndexValues.Add(RuntimeText.ElementAt(index - 1));
                }

                double TargetValue = 0;
                int Count = ValueInputs.Count();
                if (Count > 0)
                {
                    TargetValue = IndexValues.Sum() / Count;
                    CurrentCheck.TargetValue = TargetValue;
                    this.txtAverage.Text = TargetValue.ToString();
                }
                else
                {
                    TargetValue = 0;
                    this.txtAverage.Text = "";
                    CurrentCheck.TargetValue = 0;
                }

                switch (CurrentCheck.VarianceType)
                {
                    case AmbulApp.Module.BusinessLogic.VarianceType.DefaultValue:

                        break;

                    case AmbulApp.Module.BusinessLogic.VarianceType.Percentage:
                        LoP = Convert.ToDouble(TargetValue - (TargetValue * (CurrentCheck.LoPercentage / 100.00)));
                        HiP = Convert.ToDouble(TargetValue + (TargetValue * (CurrentCheck.HiPercentage / 100.00)));
                        //HACK here is where we save the calculated Hi and Low %
                        //CurrentCheck.CalculatedLowPercentage = LoP;
                        //CurrentCheck.CalculatedHighPercentage = HiP;
                        CalculateStatus(IndexValues, LoP, HiP, CurrentCheck);
                        break;

                    case AmbulApp.Module.BusinessLogic.VarianceType.Tollerance:
                        LoP = Convert.ToDouble(TargetValue - CurrentCheck.LoTolerance);
                        HiP = Convert.ToDouble(TargetValue + CurrentCheck.HiTolerance);

                        //HACK here is where we save the calculated Hi and Low limits
                        //CurrentCheck.CalculatedLowLimit = LoP;
                        //CurrentCheck.CalculatedHighLimit = HiP;

                        CalculateStatus(IndexValues, LoP, HiP, CurrentCheck);
                        break;

                    case AmbulApp.Module.BusinessLogic.VarianceType.MinOfBoth:

                        //HACK we don't need this case at the moment
                        //double LowMin = new List<double>() { LoP, CurrentCheck.LoPercentage }.Min(number => number);
                        //double HighMin = new List<double>() { HiP, CurrentCheck.HiPercentage }.Min(number => number);

                        //CalculateStatus(IndexValues, LowMin, HighMin, CurrentCheck);
                        break;

                    case AmbulApp.Module.BusinessLogic.VarianceType.MaxOfBoth:
                        //HACK we don't need this case at the moment
                        //double LowMax = new List<double>() { LoP, CurrentCheck.LoPercentage }.Max(number => number);
                        //double HighMax = new List<double>() { HiP, CurrentCheck.HiPercentage }.Max(number => number);
                        //CalculateStatus(IndexValues, LowMax, HighMax, CurrentCheck);
                        break;

                    case AmbulApp.Module.BusinessLogic.VarianceType.FromOtherStep:
                        //H~ACK we don't need this case at the moment~
                        //CalculateStatus(IndexValues, RefencedCheck.LoPercentage, RefencedCheck.HiPercentage, RefencedCheck);
                        break;

                    case AmbulApp.Module.BusinessLogic.VarianceType.PercentagePlusFromOtherStep:
                        //HACK we don't need this case at the moment
                        //LoP = Convert.ToDouble(TargetValue - (TargetValue * (RefencedCheck.LoPercentage / 100.00)));
                        //HiP = Convert.ToDouble(TargetValue + (TargetValue * (RefencedCheck.HiPercentage / 100.00)));
                        //CalculateStatus(IndexValues, LoP, HiP, CurrentCheck);
                        break;
                }
            }
            catch (Exception)
            {
                CurrentCheck.OK = OkType.Ko;
                //TODO set av to zero but in the control, if I do it in the check it will not show back in the U.I
                CurrentCheck.RunTimeText = "Wrong string";
                //HACK this should be a silent catch since it might throw errors while typing
                //throw;
            }
        }

        private void TargerSeqCheck_Changed(object sender, DevExpress.Xpo.ObjectChangeEventArgs e)
        {
            if (e.PropertyName == nameof(Check.RunTimeText))
            {
                Debug.WriteLine(string.Format("{0}:{1}", "targerSeqCheck.RunTimeText", targerSeqCheck.RunTimeText));
                NewProcess();
            }
        }

        public A05() : base(null, null)
        {
            InitializeComponent();
        }

        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(sender, e);
            if (e.PropertyName == nameof(this.RuntimeTextValue))
            {
                this.txtValueInputs.Text = this.RuntimeTextValue;
            }
        }
    }
}