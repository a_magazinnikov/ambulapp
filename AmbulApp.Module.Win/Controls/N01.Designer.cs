﻿namespace AmbulApp.Module.Win.Controls
{
    partial class N01
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainLayout = new DevExpress.XtraLayout.LayoutControl();
            this.RuntimeText = new DevExpress.XtraEditors.MemoEdit();
            this.InternalText = new System.Windows.Forms.Label();
            this.StepDescription = new System.Windows.Forms.Label();
            this.MainGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutItemInternalText = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutItemStepDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.LiRuntimeText = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RuntimeText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).BeginInit();
            this.SuspendLayout();
            // 
            // MainLayout
            // 
            this.MainLayout.Controls.Add(this.RuntimeText);
            this.MainLayout.Controls.Add(this.InternalText);
            this.MainLayout.Controls.Add(this.StepDescription);
            this.MainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayout.Location = new System.Drawing.Point(0, 0);
            this.MainLayout.Name = "MainLayout";
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(725, 2, 650, 400);
            this.MainLayout.Root = this.MainGroup;
            this.MainLayout.Size = new System.Drawing.Size(625, 112);
            this.MainLayout.TabIndex = 1;
            this.MainLayout.Text = "layoutControl1";
            // 
            // RuntimeText
            // 
            this.RuntimeText.Location = new System.Drawing.Point(12, 72);
            this.RuntimeText.Name = "RuntimeText";
            this.RuntimeText.Size = new System.Drawing.Size(601, 28);
            this.RuntimeText.StyleController = this.MainLayout;
            this.RuntimeText.TabIndex = 9;
            // 
            // InternalText
            // 
            this.InternalText.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.InternalText.Location = new System.Drawing.Point(12, 12);
            this.InternalText.Name = "InternalText";
            this.InternalText.Size = new System.Drawing.Size(601, 26);
            this.InternalText.TabIndex = 8;
            this.InternalText.Text = "InternalText";
            // 
            // StepDescription
            // 
            this.StepDescription.Location = new System.Drawing.Point(12, 42);
            this.StepDescription.Name = "StepDescription";
            this.StepDescription.Size = new System.Drawing.Size(601, 26);
            this.StepDescription.TabIndex = 7;
            this.StepDescription.Text = "StepDescription";
            // 
            // MainGroup
            // 
            this.MainGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.MainGroup.GroupBordersVisible = false;
            this.MainGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutItemInternalText,
            this.LayoutItemStepDescription,
            this.LiRuntimeText});
            this.MainGroup.Name = "Root";
            this.MainGroup.Size = new System.Drawing.Size(625, 112);
            this.MainGroup.TextVisible = false;
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.Control = this.InternalText;
            this.LayoutItemInternalText.Location = new System.Drawing.Point(0, 0);
            this.LayoutItemInternalText.Name = "LayoutItemInternalText";
            this.LayoutItemInternalText.Size = new System.Drawing.Size(605, 30);
            this.LayoutItemInternalText.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutItemInternalText.TextVisible = false;
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Control = this.StepDescription;
            this.LayoutItemStepDescription.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.LayoutItemStepDescription.Location = new System.Drawing.Point(0, 30);
            this.LayoutItemStepDescription.Name = "LayoutItemStepDescription";
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(605, 30);
            this.LayoutItemStepDescription.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutItemStepDescription.TextVisible = false;
            // 
            // LiRuntimeText
            // 
            this.LiRuntimeText.Control = this.RuntimeText;
            this.LiRuntimeText.Location = new System.Drawing.Point(0, 60);
            this.LiRuntimeText.Name = "LiRuntimeText";
            this.LiRuntimeText.Size = new System.Drawing.Size(605, 32);
            this.LiRuntimeText.TextSize = new System.Drawing.Size(0, 0);
            this.LiRuntimeText.TextVisible = false;
            // 
            // N01
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainLayout);
            this.Name = "N01";
            this.Size = new System.Drawing.Size(625, 112);
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RuntimeText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraLayout.LayoutControl MainLayout;
        protected System.Windows.Forms.Label InternalText;
        protected System.Windows.Forms.Label StepDescription;
        public DevExpress.XtraLayout.LayoutControlGroup MainGroup;
        public DevExpress.XtraLayout.LayoutControlItem LayoutItemInternalText;
        public DevExpress.XtraLayout.LayoutControlItem LayoutItemStepDescription;
        public DevExpress.XtraEditors.MemoEdit RuntimeText;
        public DevExpress.XtraLayout.LayoutControlItem LiRuntimeText;
    }
}
