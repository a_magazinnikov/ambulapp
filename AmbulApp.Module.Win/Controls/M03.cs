﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;

namespace AmbulApp.Module.Win.Controls
{
    public partial class M03 : M01
    {
        public M03()
        {
            InitializeComponent();
        }

        public M03(Check CurrentCheck, List<Check> AllChecks) : base(CurrentCheck, AllChecks)
        {
            InitializeComponent();
            this.RuntimeText.Font = new Font(this.RuntimeText.Font.FontFamily, this.EditorsFontSize);
            this.RuntimeText.TextChanged += RuntimeText_TextChanged;
        }

        public override int StartTabIndex(int StartIndex)
        {
            this.Radio.TabIndex = StartIndex + 1;
            this.RuntimeText.TabIndex = StartIndex + 2;
            this.LastTabIndex = this.RuntimeText.TabIndex;
            return LastTabIndex;
        }

        private void RuntimeText_TextChanged(object sender, EventArgs e)
        {
            this.RuntimeTextValue = this.RuntimeText.Text;
        }

        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(sender, e);
            if (e.PropertyName == nameof(RuntimeTextValue))
            {
                this.RuntimeText.Text = this.RuntimeTextValue;
            }
        }
    }
}