﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;

namespace AmbulApp.Module.Win.Controls
{
    public class A0Base : M01
    {
        public A0Base(Check CurrentCheck, List<Check> AllChecks) : base(CurrentCheck, AllChecks)
        {
        }

        public A0Base() : base()
        {
        }

        protected virtual void Process()
        {
            double LoP = 0, HiP = 0, LoT = 0, HiT = 0, LowLim = 0, HighLim = 0;
            Check RefencedCheck = null;
            LoP = Convert.ToDouble(TargetValue - (TargetValue * (CurrentCheck.LoPercentage / 100.00)));
            HiP = Convert.ToDouble(TargetValue + (TargetValue * (CurrentCheck.HiPercentage / 100.00)));
            LoT = TargetValue - CurrentCheck.LoTolerance;
            HiT = TargetValue + CurrentCheck.HiTolerance;
            switch (CurrentCheck.VarianceType)
            {
                case VarianceType.DefaultValue:

                    break;

                case VarianceType.Percentage:

                    LowLim = LoP;
                    HighLim = HiP;
                    break;

                case VarianceType.Tollerance:

                    LowLim = LoT;
                    HighLim = HiT;
                    break;

                case VarianceType.MinOfBoth:
                    LowLim = new List<double>() { LoP, LoT }.Max(number => number);
                    HighLim = new List<double>() { HiP, HiT }.Min(number => number);

                    break;

                case VarianceType.MaxOfBoth:
                    LowLim = new List<double>() { LoP, LoT }.Min(number => number);
                    HighLim = new List<double>() { HiP, HiT }.Max(number => number);

                    break;

                case VarianceType.FromOtherStep:

                    RefencedCheck = _AllChecks.FirstOrDefault(chk => chk.Sequence == CurrentCheck.VarianceSeq);
                    if (RefencedCheck != null)
                    {
                        var RuntimeTextToInt = double.Parse(RefencedCheck.RunTimeText);

                        LowLim = Convert.ToDouble(TargetValue - (TargetValue * (RuntimeTextToInt / 100.00)));
                        HighLim = Convert.ToDouble(TargetValue + (TargetValue * (RuntimeTextToInt / 100.00)));
                    }
                    break;

                case VarianceType.PercentagePlusFromOtherStep:
                    RefencedCheck = _AllChecks.FirstOrDefault(chk => chk.Sequence == CurrentCheck.VarianceSeq);
                    if (RefencedCheck != null)
                    {
                        var RuntimeTextToInt = double.Parse(RefencedCheck.RunTimeText);

                        LowLim = Convert.ToDouble(TargetValue - (TargetValue * ((RuntimeTextToInt + CurrentCheck.LoPercentage) / 100.00)));
                        HighLim = Convert.ToDouble(TargetValue + (TargetValue * ((RuntimeTextToInt + CurrentCheck.HiPercentage) / 100.00)));
                    }
                    break;
            }
            this.CurrentCheck.CalculatedLowLimit = LowLim;
            this.CurrentCheck.CalculatedHighLimit = HighLim;
            if (this.ReadValue.IsWithin(LowLim, HighLim))
            {
                this.OkValue = OkType.Ok;
            }
            else
            {
                this.OkValue = OkType.Ko;
            }

            //Check RefencedCheck = null;
            //List<int> RuntimeText = new List<int>();
            //if (CurrentCheck.VarianceType == BusinessLogic.VarianceType.FromOtherStep)
            //{
            //    RefencedCheck = _AllChecks.FirstOrDefault(chk => chk.Sequence == CurrentCheck.TargetSequence);
            //    if (RefencedCheck != null)
            //        RuntimeText = new List<int>(Array.ConvertAll(RefencedCheck.RunTimeText.Split(','),
            //            Convert.ToInt32));
            //}

            //if (CurrentCheck.VarianceType == BusinessLogic.VarianceType.Percentage)
            //{
            //    LoP = Convert.ToDouble(TargetValue - (TargetValue * (CurrentCheck.LoPercentage / 100.00)));
            //    HiP = Convert.ToDouble(TargetValue + (TargetValue * (CurrentCheck.HiPercentage / 100.00)));
            //    if (this.ReadValue.IsWithin(LoP, HiP))
            //    {
            //        this.OkValue = OkType.Ok;
            //    }
            //    else
            //    {
            //        this.OkValue = OkType.Ko;
            //    }
            //}
            //if (CurrentCheck.VarianceType == BusinessLogic.VarianceType.Tollerance)
            //{
            //    if (this.ReadValue.IsWithin(Convert.ToDouble(this.CurrentCheck.LoTolerance), Convert.ToDouble(this.CurrentCheck.HiPercentage)))
            //    {
            //        this.OkValue = OkType.Ok;
            //    }
            //    else
            //    {
            //        this.OkValue = OkType.Ko;
            //    }
            //}

            //if (CurrentCheck.ValueInputs != null)
            //{
            //    List<int> ValueInputs = new List<int>(Array.ConvertAll(CurrentCheck.ValueInputs.Split(','), Convert.ToInt32));
            //    //check.ValueInputs.Split(',').Select(int.Parse).ToList();
            //    List<double> IndexValues = new List<double>();
            //    foreach (var index in ValueInputs)
            //    {
            //        IndexValues.Add(RuntimeText.ElementAt(index - 1));
            //    }

            //    double TargetValue = 0;
            //    int Count = ValueInputs.Count();
            //    if (Count > 0)
            //    {
            //        TargetValue = IndexValues.Sum() / Count;
            //    }
            //    else
            //    {
            //        TargetValue = 0;
            //    }
            //    LoP = Convert.ToDouble(TargetValue - (TargetValue * (CurrentCheck.LoPercentage / 100.00)));
            //    HiP = Convert.ToDouble(TargetValue + (TargetValue * (CurrentCheck.HiPercentage / 100.00)));

            //    switch (CurrentCheck.VarianceType)
            //    {
            //        case AmbulApp.Module.BusinessLogic.VarianceType.DefaultValue:

            //            break;

            //        case AmbulApp.Module.BusinessLogic.VarianceType.Percentage:

            //            CalculateStatus(IndexValues, LoP, HiP, CurrentCheck);
            //            break;

            //        case AmbulApp.Module.BusinessLogic.VarianceType.Tollerance:

            //            CalculateStatus(IndexValues, CurrentCheck.LoPercentage, CurrentCheck.HiPercentage, CurrentCheck);
            //            break;

            //        case AmbulApp.Module.BusinessLogic.VarianceType.MinOfBoth:

            //            double LowMin = new List<double>() { LoP, CurrentCheck.LoPercentage }.Min(number => number);
            //            double HighMin = new List<double>() { HiP, CurrentCheck.HiPercentage }.Min(number => number);

            //            CalculateStatus(IndexValues, LowMin, HighMin, CurrentCheck);
            //            break;

            //        case AmbulApp.Module.BusinessLogic.VarianceType.MaxOfBoth:

            //            double LowMax = new List<double>() { LoP, CurrentCheck.LoPercentage }.Max(number => number);
            //            double HighMax = new List<double>() { HiP, CurrentCheck.HiPercentage }.Max(number => number);
            //            CalculateStatus(IndexValues, LowMax, HighMax, CurrentCheck);
            //            break;

            //        case AmbulApp.Module.BusinessLogic.VarianceType.FromOtherStep:
            //            CalculateStatus(IndexValues, RefencedCheck.LoPercentage, RefencedCheck.HiPercentage, RefencedCheck);
            //            break;

            //        case AmbulApp.Module.BusinessLogic.VarianceType.PercentagePlusFromOtherStep:
            //            LoP = Convert.ToDouble(TargetValue - (TargetValue * (RefencedCheck.LoPercentage / 100.00)));
            //            HiP = Convert.ToDouble(TargetValue + (TargetValue * (RefencedCheck.HiPercentage / 100.00)));
            //            CalculateStatus(IndexValues, LoP, HiP, CurrentCheck);
            //            break;
            //    }
            //}
        }

        protected void CalculateStatus(List<double> IndexValues, double LoP, double HiP, Check CurrentCheck, bool IsPercentage = false)
        {
            bool ValuesOutOfRange = IndexValues.Any(item => item.IsWithin(LoP, HiP) == false);
            StringBuilder builder = new StringBuilder();
            int NewIndex = 1;
            foreach (var s in IndexValues)
            {
                builder.Append(NewIndex.ToString() + "°:").Append(s + " ");
                NewIndex = NewIndex + 1;
            }
           

            if (ValuesOutOfRange)
                CurrentCheck.OK = OkType.Ko;
            else
                CurrentCheck.OK = OkType.Ok;

            CurrentCheck.RunTimeText = builder.ToString().TrimEnd(new char[] { ' ' });
            //TODO save the percentage
            //if (IsPercentage)
            //{
            //    CurrentCheck.CalculatedLowPercentage = LoP;
            //    CurrentCheck.CalculatedHighPercentage = HiP;
            //}
            //else
            //{
            //    CurrentCheck.CalculatedLowLimit = LoP;
            //    CurrentCheck.CalculatedHighLimit = HiP;
            //}
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            this.SuspendLayout();
            // 
            // InternalText
            // 
            this.InternalText.Size = new System.Drawing.Size(651, 20);
            // 
            // StepDescription
            // 
            this.StepDescription.Size = new System.Drawing.Size(516, 34);
            // 
            // Radio
            // 
            this.Radio.Location = new System.Drawing.Point(532, 36);
            this.Radio.Size = new System.Drawing.Size(131, 34);
            // 
            // MainLayout
            // 
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(725, 2, 650, 400);
            this.MainLayout.Size = new System.Drawing.Size(675, 82);
            this.MainLayout.Controls.SetChildIndex(this.StepDescription, 0);
            this.MainLayout.Controls.SetChildIndex(this.Radio, 0);
            this.MainLayout.Controls.SetChildIndex(this.InternalText, 0);
            // 
            // MainGroup
            // 
            this.MainGroup.Size = new System.Drawing.Size(675, 82);
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.Size = new System.Drawing.Size(655, 24);
            // 
            // LayoutItemOkKo
            // 
            this.LayoutItemOkKo.Location = new System.Drawing.Point(520, 24);
            this.LayoutItemOkKo.MaxSize = new System.Drawing.Size(0, 0);
            this.LayoutItemOkKo.MinSize = new System.Drawing.Size(54, 29);
            this.LayoutItemOkKo.Size = new System.Drawing.Size(135, 38);
            this.LayoutItemOkKo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Default;
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(520, 38);
            // 
            // A0Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "A0Base";
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            this.ResumeLayout(false);

        }
    }
}