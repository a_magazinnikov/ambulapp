﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AmbulApp.Module.BusinessObjects;

namespace AmbulApp.Module.Win.Controls
{
    public partial class N01 : CustomControlBase
    {
        public N01(Check CurrentCheck, List<Check> AllChecks) : base(CurrentCheck, AllChecks)
        {
            InitializeComponent();
            this.InternalText.Font = this.GetInternalTextFont();
            this.StepDescription.Font = this.GetStepDescriptionFont();
            this.RuntimeText.TextChanged += RuntimeText_TextChanged;
            MouseWheelRedirector.Attach(this.RuntimeText);
            this.RuntimeText.Font = new Font(this.RuntimeText.Font.FontFamily, this.EditorsFontSize);
            //this.RuntimeText.Multiline = true;
            //this.RuntimeText.Size = new Size(300, 100);
            //this.RuntimeText.Font = this.GetRuntimeTextFont();
        }

        public N01()
        {
            InitializeComponent();
        }

        private void RuntimeText_TextChanged(object sender, EventArgs e)
        {
            this.RuntimeTextValue = this.RuntimeText.Text;
            double CalculatedTarget = 0;
            double.TryParse(this.RuntimeText.Text, out CalculatedTarget);
            CurrentCheck.CalculatedTargeValue = CalculatedTarget;
        }

        public override int StartTabIndex(int StartIndex)
        {
            this.RuntimeText.TabIndex = StartIndex + 1;
            this.LastTabIndex = this.RuntimeText.TabIndex;
            return LastTabIndex;
        }

        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(sender, e);
            if (e.PropertyName == nameof(StepDescriptionValue))
            {
                this.StepDescription.Text = this.StepDescriptionValue;
            }
            if (e.PropertyName == nameof(InternalTextValue))
            {
                this.InternalText.Text = InternalTextValue;
            }
            if (e.PropertyName == nameof(RuntimeTextValue))
            {
                this.RuntimeText.Text = this.RuntimeTextValue;
            }
        }
    }
}