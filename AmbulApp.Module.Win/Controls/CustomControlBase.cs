﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AmbulApp.Module.BusinessLogic;

using DevExpress.XtraLayout;
using System.Configuration;
using AmbulApp.Module.BusinessObjects;

namespace AmbulApp.Module.Win.Controls
{
    public partial class CustomControlBase : DevExpress.XtraEditors.XtraUserControl, INotifyPropertyChanged
    {
        public CustomControlBase()
        {
            InitializeComponent();
        }

        private float labelFontSize = 13;
        public float EditorsFontSize = 13;
        public void DisableMouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta != 0)
                DevExpress.Utils.DXMouseEventArgs.GetMouseArgs(e).Handled = true;
        }
        public void UpdateLayoutItemsFontSize(LayoutControl Layout)
        {
            foreach (BaseLayoutItem baseLayoutItem in Layout.Items)
            {
                LayoutItem layoutItem = (baseLayoutItem as LayoutItem);
                if (layoutItem != null)
                {
                    layoutItem.AppearanceItemCaption.Font = new Font(layoutItem.AppearanceItemCaption.Font.FontFamily, labelFontSize);
                }
                LayoutControlItem LayoutControlItem = (baseLayoutItem as LayoutControlItem);
                if (LayoutControlItem != null)
                {
                    LayoutControlItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
                }
            }

            foreach (Control control in Layout.Controls)
            {
                CalcEdit CalcEditoControl = control as CalcEdit;
                if(CalcEditoControl!=null)
                {
                    CalcEditoControl.Properties.AllowMouseWheel = false;
                    CalcEditoControl.MouseWheel += this.DisableMouseWheel;
                }
            }
        }

        public bool IsLoading
        {
            get { return isLoading; }
            set
            {
                if (isLoading == value)
                    return;
                isLoading = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(IsLoading)));
            }
        }

        private bool isLoading;
        private Check currentCheck;

        public CustomControlBase(Check CurrentCheck, List<Check> AllChecks)
        {
            InitializeComponent();

            this.IsLoading = true;
            if (this.DesignMode)
            {
                return;
            }
            labelFontSize = float.Parse(ConfigurationManager.AppSettings["LabelFontSize"]);
            EditorsFontSize = float.Parse(ConfigurationManager.AppSettings["EditorsFontSize"]);
            this.CurrentCheck = CurrentCheck;

            _AllChecks = AllChecks;
            if (CurrentCheck.HideIfSequence != null)
            {
                watchCheck = _AllChecks.Where(chk => chk.Sequence == CurrentCheck.HideIfSequence).FirstOrDefault();
                watchCheck.Changed += EvaluateRelatedValuesChange;
                EvaluateRelatedValuesChange(this, null);
            }
        }

        private void EvaluateRelatedValuesChange(object sender, DevExpress.Xpo.ObjectChangeEventArgs e)
        {
            if ((this.CurrentCheck.HideIfValue == watchCheck.OK) || (this.watchCheck.Hidden))
            {
                this.CurrentCheck.Hidden = true;
                this.Enabled = false;
            }
            else
            {
                this.CurrentCheck.Hidden = false;
                this.Enabled = true;
            }
        }

        protected List<Check> _AllChecks;

        public int LastTabIndex
        {
            get { return lastTabIndex; }
            set
            {
                if (lastTabIndex == value)
                    return;
                lastTabIndex = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(LastTabIndex)));
            }
        }

        public virtual int StartTabIndex(int StartIndex)
        {
            throw new NotImplementedException();
        }

        public virtual Font GetInternalTextFont()
        {
            return new System.Drawing.Font("Tahoma", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
        }

        public virtual Font GetRuntimeTextFont()
        {
            return new System.Drawing.Font("Tahoma", 13F, FontStyle.Regular);
        }

        public virtual Font GetStepDescriptionFont()
        {
            return new System.Drawing.Font("Tahoma", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic))));
        }

        protected virtual void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(sender, e);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private TargetType targetType;
        private VarianceType varianceType;
        private int lastTabIndex;
        private double lowTolerance;
        private double highTolerance;
        private double lowPercentage;
        private double highPercentage;
        private double targetValue;
        private double readValue;
        private string runtimeTextValue;
        private OkType okValue;
        private string internalText;
        private string stepDescriptionValue;
        protected Check watchCheck;

        [Bindable(true)]
        public String StepDescriptionValue
        {
            get { return stepDescriptionValue; }
            set
            {
                if (stepDescriptionValue == value)
                    return;
                stepDescriptionValue = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(StepDescriptionValue)));
            }
        }

        [Bindable(true)]
        public string InternalTextValue
        {
            get { return internalText; }
            set
            {
                if (internalText == value)
                    return;
                internalText = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(InternalTextValue)));
            }
        }

        [Bindable(true)]
        public OkType OkValue
        {
            get { return okValue; }
            set
            {
                if (okValue == value)
                    return;
                okValue = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(OkValue)));
            }
        }

        [Bindable(true)]
        public string RuntimeTextValue
        {
            get { return runtimeTextValue; }
            set
            {
                if (runtimeTextValue == value)
                    return;
                runtimeTextValue = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(RuntimeTextValue)));
            }
        }

        [Bindable(true)]
        public double ReadValue
        {
            get { return readValue; }
            set
            {
                if (readValue == value)
                    return;
                readValue = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(ReadValue)));
            }
        }

        [Bindable(true)]
        public double TargetValue
        {
            get { return targetValue; }
            set
            {
                if (targetValue == value)
                    return;
                targetValue = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(TargetValue)));
            }
        }

        [Bindable(true)]
        public double HighPercentageValue
        {
            get { return highPercentage; }
            set
            {
                if (highPercentage == value)
                    return;
                highPercentage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(HighPercentageValue)));
            }
        }

        [Bindable(true)]
        public double LowPercentageValue
        {
            get { return lowPercentage; }
            set
            {
                if (lowPercentage == value)
                    return;
                lowPercentage = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(LowPercentageValue)));
            }
        }

        [Bindable(true)]
        public double HighToleranceValue
        {
            get { return highTolerance; }
            set
            {
                if (highTolerance == value)
                    return;
                highTolerance = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(HighToleranceValue)));
            }
        }

        [Bindable(true)]
        public double LowToleranceValue
        {
            get { return lowTolerance; }
            set
            {
                if (lowTolerance == value)
                    return;
                lowTolerance = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(LowToleranceValue)));
            }
        }

        [Bindable(true)]
        public VarianceType VarianceType
        {
            get { return varianceType; }
            set
            {
                if (varianceType == value)
                    return;
                varianceType = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(VarianceType)));
            }
        }

        [Bindable(true)]
        public TargetType TargetType
        {
            get { return targetType; }
            set
            {
                if (targetType == value)
                    return;
                targetType = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(TargetType)));
            }
        }

        protected Check CurrentCheck { get => currentCheck; set => currentCheck = value; }
    }
}