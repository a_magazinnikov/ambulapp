﻿using System;
using System.Linq;

namespace AmbulApp.Module.Win.Controls
{
    public static class Extensions
    {
        public static bool IsWithin(this double value, double minimum, double maximum)
        {
            if (value.CompareTo(minimum) < 0)
                return false;
            if (value.CompareTo(maximum) > 0)
                return false;
            return true;
        }
        public static bool IsWithin(this int value, double minimum, double maximum)
        {
            if (value.CompareTo(minimum) < 0)
                return false;
            if (value.CompareTo(maximum) > 0)
                return false;
            return true;
        }
        public static bool IsWithin(this decimal value, decimal minimum, decimal maximum)
        {
            if (value.CompareTo(minimum) < 0)
                return false;
            if (value.CompareTo(maximum) > 0)
                return false;
            return true;
        }
    }
}
