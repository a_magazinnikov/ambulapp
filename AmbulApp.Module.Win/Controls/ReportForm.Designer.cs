﻿namespace AmbulApp.Module.Win.Controls
{
    partial class ReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainFLowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // MainFLowPanel
            // 
            this.MainFLowPanel.AutoScroll = true;
            this.MainFLowPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainFLowPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.MainFLowPanel.Location = new System.Drawing.Point(0, 0);
            this.MainFLowPanel.Name = "MainFLowPanel";
            this.MainFLowPanel.Size = new System.Drawing.Size(800, 450);
            this.MainFLowPanel.TabIndex = 1;
            this.MainFLowPanel.WrapContents = false;
            // 
            // ReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.MainFLowPanel);
            this.Name = "ReportForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.FlowLayoutPanel MainFLowPanel;
    }
}