﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;

namespace AmbulApp.Module.Win.Controls
{
    public partial class M04 : M03
    {
        public M04(Check CurrentCheck, List<Check> AllChecks) : base(CurrentCheck, AllChecks)
        {
            InitializeComponent();
            this.Radio.Properties.Items.Add(new DevExpress.XtraEditors.Controls.RadioGroupItem(AmbulApp.Module.BusinessLogic.OkType.Na, "NA"));
        }
        public M04():base(null,null)
        {
            InitializeComponent();
           
        }
    }
}
