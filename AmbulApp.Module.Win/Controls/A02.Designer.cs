﻿namespace AmbulApp.Module.Win.Controls
{
    partial class A02
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.LiLowPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiLowTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiTargetValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            this.SuspendLayout();
            // 
            // LiLowPercentage
            // 
            this.LiLowPercentage.Location = new System.Drawing.Point(342, 100);
            // 
            // LiHighPercentage
            // 
            this.LiHighPercentage.Location = new System.Drawing.Point(513, 100);
            // 
            // LiLowTolerance
            // 
            this.LiLowTolerance.Location = new System.Drawing.Point(684, 100);
            this.LiLowTolerance.Size = new System.Drawing.Size(171, 40);
            // 
            // LiHighTolerance
            // 
            this.LiHighTolerance.Location = new System.Drawing.Point(855, 100);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 100);
            // 
            // LiTargetValue
            // 
            this.LiTargetValue.Location = new System.Drawing.Point(171, 100);
            // 
            // LiRuntimeText
            // 
            this.LiRuntimeText.Location = new System.Drawing.Point(0, 76);
            this.LiRuntimeText.Size = new System.Drawing.Size(1198, 24);
            // 
            // InternalText
            // 
            this.InternalText.Size = new System.Drawing.Size(1194, 31);
            // 
            // StepDescription
            // 
            this.StepDescription.Location = new System.Drawing.Point(12, 47);
            this.StepDescription.Size = new System.Drawing.Size(1194, 37);
            // 
            // Radio
            // 
            this.Radio.Location = new System.Drawing.Point(1016, 112);
            // 
            // MainLayout
            // 
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(630, 1, 650, 400);
            this.MainLayout.OptionsView.UseDefaultDragAndDropRendering = false;
            this.MainLayout.Size = new System.Drawing.Size(1218, 160);
            this.MainLayout.Controls.SetChildIndex(this.StepDescription, 0);
            this.MainLayout.Controls.SetChildIndex(this.Radio, 0);
            this.MainLayout.Controls.SetChildIndex(this.InternalText, 0);
            // 
            // MainGroup
            // 
            this.MainGroup.Size = new System.Drawing.Size(1218, 160);
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.Size = new System.Drawing.Size(1198, 35);
            // 
            // LayoutItemOkKo
            // 
            this.LayoutItemOkKo.Location = new System.Drawing.Point(1004, 100);
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Location = new System.Drawing.Point(0, 35);
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(1198, 41);
            // 
            // A02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "A02";
            this.Size = new System.Drawing.Size(1218, 160);
            ((System.ComponentModel.ISupportInitialize)(this.LiLowPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiLowTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiTargetValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
