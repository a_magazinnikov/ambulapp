﻿namespace AmbulApp.Module.Win.Controls
{
    partial class A01
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PercentageLow = new DevExpress.XtraEditors.CalcEdit();
            this.LiLowPercentage = new DevExpress.XtraLayout.LayoutControlItem();
            this.PercentageHigh = new DevExpress.XtraEditors.CalcEdit();
            this.LiHighPercentage = new DevExpress.XtraLayout.LayoutControlItem();
            this.ToleranceLow = new DevExpress.XtraEditors.CalcEdit();
            this.LiLowTolerance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ToleranceHigh = new DevExpress.XtraEditors.CalcEdit();
            this.LiHighTolerance = new DevExpress.XtraLayout.LayoutControlItem();
            this.Misurato = new DevExpress.XtraEditors.CalcEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Target = new DevExpress.XtraEditors.CalcEdit();
            this.LiTargetValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.RuntimeText = new DevExpress.XtraEditors.TextEdit();
            this.LiRuntimeText = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentageLow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiLowPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentageHigh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToleranceLow.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiLowTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToleranceHigh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Misurato.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Target.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiTargetValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RuntimeText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).BeginInit();
            this.SuspendLayout();
            // 
            // InternalText
            // 
            this.InternalText.Size = new System.Drawing.Size(1195, 22);
            // 
            // StepDescription
            // 
            this.StepDescription.Location = new System.Drawing.Point(12, 38);
            this.StepDescription.Size = new System.Drawing.Size(1195, 20);
            // 
            // Radio
            // 
            this.Radio.Location = new System.Drawing.Point(1076, 86);
            this.Radio.Properties.ItemsLayout = DevExpress.XtraEditors.RadioGroupItemsLayout.Flow;
            this.Radio.Size = new System.Drawing.Size(131, 25);
            // 
            // MainLayout
            // 
            this.MainLayout.Controls.Add(this.RuntimeText);
            this.MainLayout.Controls.Add(this.Target);
            this.MainLayout.Controls.Add(this.Misurato);
            this.MainLayout.Controls.Add(this.ToleranceHigh);
            this.MainLayout.Controls.Add(this.ToleranceLow);
            this.MainLayout.Controls.Add(this.PercentageHigh);
            this.MainLayout.Controls.Add(this.PercentageLow);
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(725, 0, 650, 400);
            this.MainLayout.Size = new System.Drawing.Size(1219, 123);
            this.MainLayout.Controls.SetChildIndex(this.StepDescription, 0);
            this.MainLayout.Controls.SetChildIndex(this.Radio, 0);
            this.MainLayout.Controls.SetChildIndex(this.InternalText, 0);
            this.MainLayout.Controls.SetChildIndex(this.PercentageLow, 0);
            this.MainLayout.Controls.SetChildIndex(this.PercentageHigh, 0);
            this.MainLayout.Controls.SetChildIndex(this.ToleranceLow, 0);
            this.MainLayout.Controls.SetChildIndex(this.ToleranceHigh, 0);
            this.MainLayout.Controls.SetChildIndex(this.Misurato, 0);
            this.MainLayout.Controls.SetChildIndex(this.Target, 0);
            this.MainLayout.Controls.SetChildIndex(this.RuntimeText, 0);
            // 
            // MainGroup
            // 
            this.MainGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LiLowPercentage,
            this.LiHighPercentage,
            this.LiLowTolerance,
            this.LiHighTolerance,
            this.layoutControlItem1,
            this.LiTargetValue,
            this.LiRuntimeText});
            this.MainGroup.Size = new System.Drawing.Size(1219, 123);
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.Size = new System.Drawing.Size(1199, 26);
            // 
            // LayoutItemOkKo
            // 
            this.LayoutItemOkKo.Location = new System.Drawing.Point(1064, 74);
            this.LayoutItemOkKo.Size = new System.Drawing.Size(135, 29);
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Location = new System.Drawing.Point(0, 26);
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(1199, 24);
            // 
            // PercentageLow
            // 
            this.PercentageLow.Location = new System.Drawing.Point(423, 86);
            this.PercentageLow.Name = "PercentageLow";
            this.PercentageLow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PercentageLow.Size = new System.Drawing.Size(98, 20);
            this.PercentageLow.StyleController = this.MainLayout;
            this.PercentageLow.TabIndex = 11;
            // 
            // LiLowPercentage
            // 
            this.LiLowPercentage.Control = this.PercentageLow;
            this.LiLowPercentage.Location = new System.Drawing.Point(342, 74);
            this.LiLowPercentage.Name = "LiLowPercentage";
            this.LiLowPercentage.Size = new System.Drawing.Size(171, 29);
            this.LiLowPercentage.Text = "Low%";
            this.LiLowPercentage.TextSize = new System.Drawing.Size(65, 13);
            // 
            // PercentageHigh
            // 
            this.PercentageHigh.Location = new System.Drawing.Point(594, 86);
            this.PercentageHigh.Name = "PercentageHigh";
            this.PercentageHigh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PercentageHigh.Size = new System.Drawing.Size(98, 20);
            this.PercentageHigh.StyleController = this.MainLayout;
            this.PercentageHigh.TabIndex = 12;
            // 
            // LiHighPercentage
            // 
            this.LiHighPercentage.Control = this.PercentageHigh;
            this.LiHighPercentage.Location = new System.Drawing.Point(513, 74);
            this.LiHighPercentage.Name = "LiHighPercentage";
            this.LiHighPercentage.Size = new System.Drawing.Size(171, 29);
            this.LiHighPercentage.Text = "High%";
            this.LiHighPercentage.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ToleranceLow
            // 
            this.ToleranceLow.Location = new System.Drawing.Point(765, 86);
            this.ToleranceLow.Name = "ToleranceLow";
            this.ToleranceLow.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ToleranceLow.Size = new System.Drawing.Size(99, 20);
            this.ToleranceLow.StyleController = this.MainLayout;
            this.ToleranceLow.TabIndex = 13;
            // 
            // LiLowTolerance
            // 
            this.LiLowTolerance.Control = this.ToleranceLow;
            this.LiLowTolerance.Location = new System.Drawing.Point(684, 74);
            this.LiLowTolerance.Name = "LiLowTolerance";
            this.LiLowTolerance.Size = new System.Drawing.Size(172, 29);
            this.LiLowTolerance.Text = "Low Tol";
            this.LiLowTolerance.TextSize = new System.Drawing.Size(65, 13);
            // 
            // ToleranceHigh
            // 
            this.ToleranceHigh.Location = new System.Drawing.Point(937, 86);
            this.ToleranceHigh.Name = "ToleranceHigh";
            this.ToleranceHigh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ToleranceHigh.Size = new System.Drawing.Size(135, 20);
            this.ToleranceHigh.StyleController = this.MainLayout;
            this.ToleranceHigh.TabIndex = 14;
            // 
            // LiHighTolerance
            // 
            this.LiHighTolerance.Control = this.ToleranceHigh;
            this.LiHighTolerance.Location = new System.Drawing.Point(856, 74);
            this.LiHighTolerance.Name = "LiHighTolerance";
            this.LiHighTolerance.Size = new System.Drawing.Size(208, 29);
            this.LiHighTolerance.Text = "Hi Tol";
            this.LiHighTolerance.TextSize = new System.Drawing.Size(65, 13);
            // 
            // Misurato
            // 
            this.Misurato.Location = new System.Drawing.Point(81, 86);
            this.Misurato.Name = "Misurato";
            this.Misurato.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Misurato.Size = new System.Drawing.Size(98, 20);
            this.Misurato.StyleController = this.MainLayout;
            this.Misurato.TabIndex = 15;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.Misurato;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(171, 29);
            this.layoutControlItem1.Text = "Measurement";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(65, 13);
            // 
            // Target
            // 
            this.Target.Location = new System.Drawing.Point(252, 86);
            this.Target.Name = "Target";
            this.Target.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Target.Size = new System.Drawing.Size(98, 20);
            this.Target.StyleController = this.MainLayout;
            this.Target.TabIndex = 17;
            // 
            // LiTargetValue
            // 
            this.LiTargetValue.Control = this.Target;
            this.LiTargetValue.CustomizationFormText = "Target";
            this.LiTargetValue.Location = new System.Drawing.Point(171, 74);
            this.LiTargetValue.Name = "LiTargetValue";
            this.LiTargetValue.Size = new System.Drawing.Size(171, 29);
            this.LiTargetValue.Text = "Target";
            this.LiTargetValue.TextSize = new System.Drawing.Size(65, 13);
            // 
            // RuntimeText
            // 
            this.RuntimeText.Location = new System.Drawing.Point(81, 62);
            this.RuntimeText.Name = "RuntimeText";
            this.RuntimeText.Size = new System.Drawing.Size(1126, 20);
            this.RuntimeText.StyleController = this.MainLayout;
            this.RuntimeText.TabIndex = 18;
            // 
            // LiRuntimeText
            // 
            this.LiRuntimeText.Control = this.RuntimeText;
            this.LiRuntimeText.Location = new System.Drawing.Point(0, 50);
            this.LiRuntimeText.Name = "LiRuntimeText";
            this.LiRuntimeText.Size = new System.Drawing.Size(1199, 24);
            this.LiRuntimeText.Text = "Runtime Text";
            this.LiRuntimeText.TextSize = new System.Drawing.Size(65, 13);
            // 
            // A01
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "A01";
            this.Size = new System.Drawing.Size(1219, 123);
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentageLow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiLowPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentageHigh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToleranceLow.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiLowTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToleranceHigh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Misurato.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Target.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiTargetValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RuntimeText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.CalcEdit ToleranceLow;
        private DevExpress.XtraEditors.CalcEdit PercentageHigh;
        private DevExpress.XtraEditors.CalcEdit PercentageLow;
        private DevExpress.XtraEditors.CalcEdit ToleranceHigh;
        private DevExpress.XtraEditors.CalcEdit Misurato;
        private DevExpress.XtraEditors.CalcEdit Target;
        protected DevExpress.XtraLayout.LayoutControlItem LiLowPercentage;
        protected DevExpress.XtraLayout.LayoutControlItem LiHighPercentage;
        protected DevExpress.XtraLayout.LayoutControlItem LiLowTolerance;
        protected DevExpress.XtraLayout.LayoutControlItem LiHighTolerance;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        protected DevExpress.XtraLayout.LayoutControlItem LiTargetValue;
        private DevExpress.XtraEditors.TextEdit RuntimeText;
        protected DevExpress.XtraLayout.LayoutControlItem LiRuntimeText;
    }
}
