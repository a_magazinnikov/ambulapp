﻿using AmbulApp.Module.BusinessLogic;

namespace AmbulApp.Module.Win.Controls
{
    partial class M01
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainLayout = new DevExpress.XtraLayout.LayoutControl();
            this.InternalText = new System.Windows.Forms.Label();
            this.Radio = new DevExpress.XtraEditors.RadioGroup();
            this.StepDescription = new System.Windows.Forms.Label();
            this.MainGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutItemInternalText = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutItemStepDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutItemOkKo = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).BeginInit();
            this.SuspendLayout();
            // 
            // MainLayout
            // 
            this.MainLayout.Controls.Add(this.InternalText);
            this.MainLayout.Controls.Add(this.Radio);
            this.MainLayout.Controls.Add(this.StepDescription);
            this.MainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainLayout.Location = new System.Drawing.Point(0, 0);
            this.MainLayout.Name = "MainLayout";
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(725, 2, 650, 400);
            this.MainLayout.Root = this.MainGroup;
            this.MainLayout.Size = new System.Drawing.Size(677, 100);
            this.MainLayout.TabIndex = 0;
            this.MainLayout.Text = "layoutControl1";
            // 
            // InternalText
            // 
            this.InternalText.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.InternalText.Location = new System.Drawing.Point(12, 12);
            this.InternalText.Name = "InternalText";
            this.InternalText.Size = new System.Drawing.Size(653, 36);
            this.InternalText.TabIndex = 8;
            this.InternalText.Text = "InternalText";
            // 
            // Radio
            // 
            this.Radio.Location = new System.Drawing.Point(475, 52);
            this.Radio.Name = "Radio";
            this.Radio.Properties.AllowMouseWheel = false;
            this.Radio.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.Radio.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(AmbulApp.Module.BusinessLogic.OkType.Ok, "OK"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(AmbulApp.Module.BusinessLogic.OkType.Ko, "KO")});
            this.Radio.Properties.ItemsLayout = DevExpress.XtraEditors.RadioGroupItemsLayout.Flow;
            this.Radio.Size = new System.Drawing.Size(190, 36);
            this.Radio.StyleController = this.MainLayout;
            this.Radio.TabIndex = 4;
            // 
            // StepDescription
            // 
            this.StepDescription.Location = new System.Drawing.Point(12, 52);
            this.StepDescription.Name = "StepDescription";
            this.StepDescription.Size = new System.Drawing.Size(459, 36);
            this.StepDescription.TabIndex = 7;
            this.StepDescription.Text = "StepDescription";
            // 
            // MainGroup
            // 
            this.MainGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.MainGroup.GroupBordersVisible = false;
            this.MainGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutItemInternalText,
            this.LayoutItemStepDescription,
            this.LayoutItemOkKo});
            this.MainGroup.Name = "Root";
            this.MainGroup.Size = new System.Drawing.Size(677, 100);
            this.MainGroup.TextVisible = false;
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.Control = this.InternalText;
            this.LayoutItemInternalText.Location = new System.Drawing.Point(0, 0);
            this.LayoutItemInternalText.Name = "LayoutItemInternalText";
            this.LayoutItemInternalText.Size = new System.Drawing.Size(657, 40);
            this.LayoutItemInternalText.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutItemInternalText.TextVisible = false;
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Control = this.StepDescription;
            this.LayoutItemStepDescription.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.LayoutItemStepDescription.Location = new System.Drawing.Point(0, 40);
            this.LayoutItemStepDescription.Name = "LayoutItemStepDescription";
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(463, 40);
            this.LayoutItemStepDescription.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutItemStepDescription.TextVisible = false;
            // 
            // LayoutItemOkKo
            // 
            this.LayoutItemOkKo.Control = this.Radio;
            this.LayoutItemOkKo.Location = new System.Drawing.Point(463, 40);
            this.LayoutItemOkKo.MaxSize = new System.Drawing.Size(194, 40);
            this.LayoutItemOkKo.MinSize = new System.Drawing.Size(194, 40);
            this.LayoutItemOkKo.Name = "LayoutItemOkKo";
            this.LayoutItemOkKo.Size = new System.Drawing.Size(194, 40);
            this.LayoutItemOkKo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LayoutItemOkKo.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutItemOkKo.TextVisible = false;
            // 
            // M01
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainLayout);
            this.Name = "M01";
            this.Size = new System.Drawing.Size(677, 100);
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected System.Windows.Forms.Label InternalText;
        protected System.Windows.Forms.Label StepDescription;
        public DevExpress.XtraEditors.RadioGroup Radio;
        public DevExpress.XtraLayout.LayoutControl MainLayout;
        public DevExpress.XtraLayout.LayoutControlGroup MainGroup;
        public DevExpress.XtraLayout.LayoutControlItem LayoutItemInternalText;
        public DevExpress.XtraLayout.LayoutControlItem LayoutItemOkKo;
        public DevExpress.XtraLayout.LayoutControlItem LayoutItemStepDescription;
    }
}
