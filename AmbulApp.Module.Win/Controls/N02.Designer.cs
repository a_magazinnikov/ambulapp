﻿namespace AmbulApp.Module.Win.Controls
{
    partial class N02
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).BeginInit();
            this.SuspendLayout();
            // 
            // MainLayout
            // 
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(725, 2, 650, 400);
            this.MainLayout.Size = new System.Drawing.Size(621, 202);
            this.MainLayout.Controls.SetChildIndex(this.StepDescription, 0);
            this.MainLayout.Controls.SetChildIndex(this.InternalText, 0);
            this.MainLayout.Controls.SetChildIndex(this.RuntimeText, 0);
            // 
            // InternalText
            // 
            this.InternalText.Size = new System.Drawing.Size(597, 26);
            // 
            // StepDescription
            // 
            this.StepDescription.Location = new System.Drawing.Point(12, 42);
            this.StepDescription.Size = new System.Drawing.Size(597, 24);
            // 
            // MainGroup
            // 
            this.MainGroup.Size = new System.Drawing.Size(621, 202);
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.MaxSize = new System.Drawing.Size(0, 30);
            this.LayoutItemInternalText.MinSize = new System.Drawing.Size(24, 30);
            this.LayoutItemInternalText.Size = new System.Drawing.Size(601, 30);
            this.LayoutItemInternalText.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Location = new System.Drawing.Point(0, 30);
            this.LayoutItemStepDescription.MaxSize = new System.Drawing.Size(0, 28);
            this.LayoutItemStepDescription.MinSize = new System.Drawing.Size(24, 28);
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(601, 28);
            this.LayoutItemStepDescription.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            // 
            // RuntimeText
            // 
            this.RuntimeText.Location = new System.Drawing.Point(12, 70);
            this.RuntimeText.Height = 300;
            this.RuntimeText.Properties.ScrollBars= System.Windows.Forms.ScrollBars.None;
            //this.RuntimeText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RuntimeText.Size = new System.Drawing.Size(597, 120);
            // 
            // LiRuntimeText
            // 
            this.LiRuntimeText.Location = new System.Drawing.Point(0, 58);
            this.LiRuntimeText.Size = new System.Drawing.Size(601, 124);
            // 
            // N02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "N02";
            this.Size = new System.Drawing.Size(621, 202);
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
