﻿using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AmbulApp.Module.Win.Controls
{
    public partial class ReportForm : Form
    {
        private const string StepDescriptionBindingProperty = "StepDescriptionValue";
        private const string InternalTextBindingProperty = "InternalTextValue";
        private const string OkValueBindingProperty = "OkValue";
        private const string RuntimeTextProperty = "RuntimeTextValue";
        private const string ReadValueProperty = "ReadValue";
        private const string TargetValueProperty = "TargetValue";
        private const string HighToleranceValueProperty = "HighToleranceValue";
        private const string LowToleranceValueProperty = "LowToleranceValue";
        private const string HighPercentageValueProperty = "HighPercentageValue";
        private const string LowPercentageValueProperty = "LowPercentageValue";
        private const string ExternalAccesoryViewId = "ExternalAccessoriesForm";
        private const string InternalModuleViewId = "InternalModulesForm";

        //Dictionary<Check, Check> dictionary = new Dictionary<Check, Check>();
        private System.Collections.Generic.List<Tuple<Check, Check>> DataList = new System.Collections.Generic.List<Tuple<Check, Check>>();

        private XafApplication _App;
        private Intervention _CurrentIntervention;
        private DevExpress.ExpressApp.View _CurrentView;

        public ReportForm(XafApplication App, Intervention CurrentIntervention, DevExpress.ExpressApp.View CurrentView)
        {
            InitializeComponent();
            WindowsFormsSettings.SmartMouseWheelProcessing = false;
            this.Text = $"{CurrentIntervention.Equipment.Brand} {CurrentIntervention.Equipment.Model} {CurrentIntervention.Equipment.SerialNumber} {CurrentIntervention.MaintenancePlan.OptionalDescription} ";
            SetTouchUIModeOn();
            this.Load += ReportForm_Load;
            _App = App;
            _CurrentIntervention = CurrentIntervention;
            _CurrentView = CurrentView;
            this.WindowState = FormWindowState.Maximized;
            this.SizeChanged += Form1_SizeChanged;
            this.FormClosing += Form1_FormClosing;
            var NonVisualChecks = "P01,P02,P03,P04,T10,T20,T21,T30,T31,T32,T40,T41,T42,T51,T60".Split(',');

            var BaseWidth = Convert.ToInt16(this.Width * 0.98);
            var CurrentTabIndex = 1;
            var AllChecks = _CurrentIntervention.Checks.OrderBy(e => e.Sequence).ToList();
            foreach (Check check in AllChecks)
            {
                #region M0X

                if (check.StepType.Code == "M01")
                {
                    var value = new M01(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    value.Width = BaseWidth;
                    SetControlBindings(BaseWidth, check, value, AllChecks);

                    this.MainFLowPanel.Controls.Add(value);
                }
                if (check.StepType.Code == "M02")
                {
                    var value = new M02(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }
                if (check.StepType.Code == "M03")
                {
                    var value = new M03(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }
                if (check.StepType.Code == "M04")
                {
                    var value = new M04(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }

                #endregion M0X

                #region A0X

                if (check.StepType.Code == "A01")
                {
                    var value = new A01(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);

                    this.MainFLowPanel.Controls.Add(value);
                }
                if (check.StepType.Code == "A02")
                {
                    var value = new A02(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }
                if (check.StepType.Code == "A03")
                {
                    var value = new A03(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }
                if (check.StepType.Code == "A04")
                {
                    var value = new A04(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }
                if (check.StepType.Code == "A05")
                {
                    var value = new A05(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }

                #endregion A0X

                #region N0X

                if (check.StepType.Code == "N01")
                {
                    var value = new N01(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }
                if (check.StepType.Code == "N02")
                {
                    var value = new N02(check, AllChecks);
                    CurrentTabIndex = value.StartTabIndex(CurrentTabIndex);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                    this.MainFLowPanel.Controls.Add(value);
                }

                #endregion N0X

                if (NonVisualChecks.Any(nv => nv == check.StepType.Code))
                {
                    var value = new CustomControlBase(check, AllChecks);
                    SetControlBindings(BaseWidth, check, value, AllChecks);
                }
            }

            

            Button ShowExternalAccesories = new Button() { Text = "Show external accesories" };
            ShowExternalAccesories.Click += new EventHandler(delegate { ShowView(ExternalAccesoryViewId, typeof(ExternalAccessory)); });

           
           
            Button ShowInternalModules = new Button() { Text = "Show internal modules" };
            ShowInternalModules.Click += new EventHandler(delegate { ShowView(InternalModuleViewId, typeof(InternalModule)); });

            this.MainFLowPanel.Controls.Add(ShowExternalAccesories);
            this.MainFLowPanel.Controls.Add(ShowInternalModules);
        }

        private void ReportForm_Load(object sender, EventArgs e)
        {
            foreach (Control control in this.MainFLowPanel.Controls)
            {
                var CustomControlBase = control as CustomControlBase;
                if (CustomControlBase != null)
                {
                    CustomControlBase.IsLoading = false;
                }
            }
        }

        private void SetTouchUIModeOn()
        {
            DevExpress.XtraEditors.WindowsFormsSettings.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.True;
            DevExpress.XtraEditors.WindowsFormsSettings.TouchScaleFactor = float.Parse(ConfigurationManager.AppSettings["TouchScaleFactor"]);
            DevExpress.XtraEditors.WindowsFormsSettings.ScrollUIMode = ScrollUIMode.Touch;
            DevExpress.XtraEditors.WindowsFormsSettings.ShowTouchScrollBarOnMouseMove = false;
        }

        private void SetTouchUIModeOff()
        {
            DevExpress.XtraEditors.WindowsFormsSettings.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.Default;
            DevExpress.XtraEditors.WindowsFormsSettings.TouchScaleFactor = 1F;
            DevExpress.XtraEditors.WindowsFormsSettings.ScrollUIMode = ScrollUIMode.Default;
            DevExpress.XtraEditors.WindowsFormsSettings.ShowTouchScrollBarOnMouseMove = true;
        }

        private static void SetControlBindings(short BaseWidth, Check check, CustomControlBase value, List<Check> AllChecks)
        {
            value.Width = BaseWidth;
            //value.IsLoading = true;//TODO remove IsLoading
            //HACK update mode should be OnValidation,
            const DataSourceUpdateMode UpdateMode = DataSourceUpdateMode.OnPropertyChanged;

            value.DataBindings.Add(OkValueBindingProperty, check, nameof(Check.OK), false, DataSourceUpdateMode.OnPropertyChanged);

            //value.DataBindings.Add(StepDescriptionBindingProperty, check, nameof(Check.StepDescription), false, UpdateMode);
            //value.DataBindings.Add(InternalTextBindingProperty, check, nameof(Check.InternalText), false, UpdateMode);

            value.DataBindings.Add(StepDescriptionBindingProperty, check, nameof(Check.InternalText), false, UpdateMode);
            value.DataBindings.Add(InternalTextBindingProperty, check, nameof(Check.StepDescription), false, UpdateMode);
            value.DataBindings.Add(RuntimeTextProperty, check, nameof(Check.RunTimeText), false, UpdateMode);
            value.DataBindings.Add(ReadValueProperty, check, nameof(Check.ReadValue), false, UpdateMode);

            value.DataBindings.Add(HighToleranceValueProperty, check, nameof(Check.HiTolerance), false, UpdateMode);
            value.DataBindings.Add(LowToleranceValueProperty, check, nameof(Check.LoTolerance), false, UpdateMode);
            value.DataBindings.Add(HighPercentageValueProperty, check, nameof(Check.HiPercentage), false, UpdateMode);
            value.DataBindings.Add(LowPercentageValueProperty, check, nameof(Check.LoPercentage), false, UpdateMode);
            value.DataBindings.Add(nameof(Check.VarianceType), check, nameof(Check.VarianceType), false, UpdateMode);
            value.DataBindings.Add(nameof(Check.TargetType), check, nameof(Check.TargetType), false, UpdateMode);

            Check ReferenceStep;

            switch (check.TargetType)
            {
                case TargetType.DefaultValue:
                    value.DataBindings.Add(TargetValueProperty, check, nameof(Check.TargetValue), false, UpdateMode);
                    break;

                case TargetType.Predefined:
                    value.DataBindings.Add(TargetValueProperty, check, nameof(Check.TargetValue), false, UpdateMode);
                    break;

                case TargetType.InputRuntime:
                    value.DataBindings.Add(TargetValueProperty, check, nameof(Check.TargetValue), false, UpdateMode);
                    break;

                case TargetType.FromOtherStep:
                    ReferenceStep = AllChecks.FirstOrDefault(step => step.TargetSequence == check.TargetSequence);
                    value.DataBindings.Add(TargetValueProperty, check, nameof(Check.CalculatedTargeValue), false, UpdateMode);
                    break;

                case TargetType.InputRuntimeFromText:
                    value.DataBindings.Add(TargetValueProperty, check, nameof(Check.CalculatedTargeValue), false, UpdateMode);
                    break;
            }
            //value.IsLoading = false;
        }

        //private void TargetSq_Changed(object sender, DevExpress.Xpo.ObjectChangeEventArgs e)
        //{
        //    //if(e.PropertyName== nameof(Check.RunTimeText))
        //    //{
        //    //}
        //    if (e.NewValue == null)
        //        return;

        //    Check key = sender as Check;
        //    var List = DataList.Where(t => t.Item1 == key).DefaultIfEmpty();
        //    foreach (var item in List)
        //    {
        //        Process(item.Item2, item.Item1);
        //    }
        //}

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SetTouchUIModeOff();
            if (this._CurrentView.ObjectSpace.IsModified)
            {
                this._CurrentView.ObjectSpace.CommitChanges();
                this._CurrentView.RefreshDataSource();
                this._CurrentView.Refresh();
            }
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            foreach (Control control in this.MainFLowPanel.Controls)
            {
                control.Width = Convert.ToInt16(this.Width * 0.98);
            }
        }

        public ReportForm()
        {
            InitializeComponent();
        }

      

        private void ShowView(string ViewId,Type ViewObjectType)
        {
            
            CollectionSourceBase collectionSource = _App.CreateCollectionSource(_App.CreateObjectSpace(), ViewObjectType, ViewId);
            collectionSource.Criteria["Main"] = new BinaryOperator("Equipment.Oid", this._CurrentIntervention.Equipment.Oid);
            DevExpress.ExpressApp.ListView view = _App.CreateListView(ViewId, collectionSource, true);

            //ShowViewParameters parameters = new ShowViewParameters(view);

            //parameters.TargetWindow = TargetWindow.NewWindow;

            //_App.ShowViewStrategy.ShowView(parameters, null);
            _App.ShowViewStrategy.ShowViewInPopupWindow(view, null, null);
        }
    }
}