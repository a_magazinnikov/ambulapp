﻿namespace AmbulApp.Module.Win.Controls
{
    partial class A04
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.LiLowPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiLowTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighTolerance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiTargetValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            this.SuspendLayout();
            // 
            // LiLowPercentage
            // 
            this.LiLowPercentage.Location = new System.Drawing.Point(342, 91);
            this.LiLowPercentage.Size = new System.Drawing.Size(171, 40);
            // 
            // LiHighPercentage
            // 
            this.LiHighPercentage.Location = new System.Drawing.Point(513, 91);
            this.LiHighPercentage.Size = new System.Drawing.Size(171, 40);
            // 
            // LiLowTolerance
            // 
            this.LiLowTolerance.Location = new System.Drawing.Point(684, 91);
            this.LiLowTolerance.Size = new System.Drawing.Size(171, 40);
            // 
            // LiHighTolerance
            // 
            this.LiHighTolerance.Location = new System.Drawing.Point(855, 91);
            this.LiHighTolerance.Size = new System.Drawing.Size(148, 40);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 91);
            this.layoutControlItem1.Size = new System.Drawing.Size(171, 40);
            // 
            // LiTargetValue
            // 
            this.LiTargetValue.Location = new System.Drawing.Point(171, 91);
            this.LiTargetValue.Size = new System.Drawing.Size(171, 40);
            // 
            // LiRuntimeText
            // 
            this.LiRuntimeText.Location = new System.Drawing.Point(0, 67);
            this.LiRuntimeText.Size = new System.Drawing.Size(1197, 24);
            // 
            // InternalText
            // 
            this.InternalText.Size = new System.Drawing.Size(1193, 29);
            // 
            // StepDescription
            // 
            this.StepDescription.Location = new System.Drawing.Point(12, 45);
            this.StepDescription.Size = new System.Drawing.Size(1193, 30);
            // 
            // Radio
            // 
            this.Radio.Location = new System.Drawing.Point(1015, 103);
            // 
            // MainLayout
            // 
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(725, 0, 650, 400);
            this.MainLayout.OptionsView.UseDefaultDragAndDropRendering = false;
            this.MainLayout.Size = new System.Drawing.Size(1217, 151);
            this.MainLayout.Controls.SetChildIndex(this.StepDescription, 0);
            this.MainLayout.Controls.SetChildIndex(this.Radio, 0);
            this.MainLayout.Controls.SetChildIndex(this.InternalText, 0);
            // 
            // MainGroup
            // 
            this.MainGroup.Size = new System.Drawing.Size(1217, 151);
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.Size = new System.Drawing.Size(1197, 33);
            // 
            // LayoutItemOkKo
            // 
            this.LayoutItemOkKo.Location = new System.Drawing.Point(1003, 91);
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Location = new System.Drawing.Point(0, 33);
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(1197, 34);
            // 
            // A04
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "A04";
            this.Size = new System.Drawing.Size(1217, 151);
            ((System.ComponentModel.ISupportInitialize)(this.LiLowPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiLowTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiHighTolerance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiTargetValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiRuntimeText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
