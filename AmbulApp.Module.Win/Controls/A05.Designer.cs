﻿namespace AmbulApp.Module.Win.Controls
{
    partial class A05
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtValueInputs = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtAverage = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValueInputs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAverage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // InternalText
            // 
            this.InternalText.Size = new System.Drawing.Size(648, 28);
            // 
            // StepDescription
            // 
            this.StepDescription.Location = new System.Drawing.Point(12, 44);
            this.StepDescription.Size = new System.Drawing.Size(648, 31);
            // 
            // Radio
            // 
            this.Radio.Enabled = false;
            this.Radio.Location = new System.Drawing.Point(534, 79);
            this.Radio.Size = new System.Drawing.Size(126, 25);
            // 
            // MainLayout
            // 
            this.MainLayout.Controls.Add(this.txtValueInputs);
            this.MainLayout.Controls.Add(this.txtAverage);
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(725, 1, 650, 400);
            this.MainLayout.Size = new System.Drawing.Size(672, 116);
            this.MainLayout.Controls.SetChildIndex(this.txtAverage, 0);
            this.MainLayout.Controls.SetChildIndex(this.StepDescription, 0);
            this.MainLayout.Controls.SetChildIndex(this.Radio, 0);
            this.MainLayout.Controls.SetChildIndex(this.InternalText, 0);
            this.MainLayout.Controls.SetChildIndex(this.txtValueInputs, 0);
            // 
            // MainGroup
            // 
            this.MainGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1});
            this.MainGroup.Size = new System.Drawing.Size(672, 116);
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.Size = new System.Drawing.Size(652, 32);
            // 
            // LayoutItemOkKo
            // 
            this.LayoutItemOkKo.Location = new System.Drawing.Point(522, 67);
            this.LayoutItemOkKo.Size = new System.Drawing.Size(130, 29);
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Location = new System.Drawing.Point(0, 32);
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(652, 35);
            // 
            // txtValueInputs
            // 
            this.txtValueInputs.Enabled = false;
            this.txtValueInputs.Location = new System.Drawing.Point(47, 79);
            this.txtValueInputs.Name = "txtValueInputs";
            this.txtValueInputs.Size = new System.Drawing.Size(363, 20);
            this.txtValueInputs.StyleController = this.MainLayout;
            this.txtValueInputs.TabIndex = 9;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtValueInputs;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 67);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(402, 29);
            this.layoutControlItem1.Text = "Values";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(31, 13);
            // 
            // txtAverage
            // 
            this.txtAverage.Enabled = false;
            this.txtAverage.Location = new System.Drawing.Point(449, 79);
            this.txtAverage.Name = "txtAverage";
            this.txtAverage.Size = new System.Drawing.Size(81, 20);
            this.txtAverage.StyleController = this.MainLayout;
            this.txtAverage.TabIndex = 10;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtAverage;
            this.layoutControlItem2.Location = new System.Drawing.Point(402, 67);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(120, 29);
            this.layoutControlItem2.Text = "Avg";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(31, 13);
            // 
            // A05
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "A05";
            this.Size = new System.Drawing.Size(672, 116);
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtValueInputs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAverage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtValueInputs;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txtAverage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}
