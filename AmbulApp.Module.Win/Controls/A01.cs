﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;

namespace AmbulApp.Module.Win.Controls
{
    public partial class A01 : A0Base
    {
        public A01() //: base()
        {
            InitializeComponent();
            RuntimeTextEnable();
        }

        public virtual void RuntimeTextEnable()
        {
            this.LiRuntimeText.Visibility = LayoutVisibility.Never;
        }

        public A01(Check CurrentCheck, List<Check> AllChecks) : base(CurrentCheck, AllChecks)
        {
            InitializeComponent();
            this.Misurato.Properties.Buttons.Clear();
            this.Target.Properties.Buttons.Clear();
            this.PercentageHigh.Properties.Buttons.Clear();
            this.PercentageLow.Properties.Buttons.Clear();
            this.ToleranceHigh.Properties.Buttons.Clear();
            this.ToleranceLow.Properties.Buttons.Clear();

            this.UpdateLayoutItemsFontSize(this.MainLayout);
            if (this.DesignMode)
                return;

            RuntimeTextEnable();
            this.RuntimeText.TextChanged += RuntimeText_TextChanged;
            this.Misurato.ValueChanged += Misurato_TextChanged;
            this.PercentageHigh.ValueChanged += PercentageHigh_ValueChanged;
            this.PercentageLow.ValueChanged += PercentageLow_ValueChanged;
            this.ToleranceLow.ValueChanged += ToleranceLow_ValueChanged;
            this.ToleranceHigh.ValueChanged += ToleranceHigh_ValueChanged;
            this.Target.ValueChanged += Target_ValueChanged;
            this.DisableOkKoNA();
            switch (this.CurrentCheck.VarianceType)
            {
                case VarianceType.DefaultValue:
                    break;
                //HACK we can also use ContentVisible
                case VarianceType.Percentage:
                    this.LiHighTolerance.Enabled = false;
                    this.LiLowTolerance.Enabled = false;
                    break;

                case VarianceType.Tollerance:
                    this.LiLowPercentage.Enabled = false;
                    this.LiHighPercentage.Enabled = false;

                    break;

                case VarianceType.MinOfBoth:
                    break;

                case VarianceType.MaxOfBoth:
                    break;

                case VarianceType.FromOtherStep:
                    this.LiLowPercentage.Enabled = false;
                    this.LiHighPercentage.Enabled = false;
                    this.LiHighTolerance.Enabled = false;
                    this.LiLowTolerance.Enabled = false;
                    break;

                case VarianceType.PercentagePlusFromOtherStep:

                    this.LiHighTolerance.Enabled = false;
                    this.LiLowTolerance.Enabled = false;
                    break;
            }
            if (CurrentCheck.TargetType == TargetType.FromOtherStep)
            {
                var ReferenceStep = AllChecks.FirstOrDefault(step => step.Sequence == CurrentCheck.TargetSequence);
                if (ReferenceStep != null) ReferenceStep.Changed += ReferenceStep_Changed;
                this.Target.Enabled = false;
            }
            if (CurrentCheck.TargetType == TargetType.InputRuntimeFromText)
            {
                this.Target.Enabled = false;
            }
            if (CurrentCheck.TargetType == TargetType.InputRuntime)
            {
                this.Target.Enabled = true;
            }
            if (CurrentCheck.TargetType == TargetType.DefaultValue || CurrentCheck.TargetType == TargetType.Predefined)
            {
                this.Target.Enabled = false;
            }

            this.RuntimeText.Font = new Font(this.RuntimeText.Font.FontFamily, this.EditorsFontSize);
            this.Misurato.Font = new Font(this.Misurato.Font.FontFamily, this.EditorsFontSize);
            
            this.Target.Font = new Font(this.Target.Font.FontFamily, this.EditorsFontSize);
            this.PercentageHigh.Font = new Font(this.PercentageHigh.Font.FontFamily, this.EditorsFontSize);
            this.PercentageLow.Font = new Font(this.PercentageHigh.Font.FontFamily, this.EditorsFontSize);
            this.ToleranceHigh.Font = new Font(this.PercentageHigh.Font.FontFamily, this.EditorsFontSize);
            this.ToleranceLow.Font = new Font(this.PercentageHigh.Font.FontFamily, this.EditorsFontSize);
        }

        private void RuntimeText_TextChanged(object sender, EventArgs e)
        {
            this.RuntimeTextValue = this.RuntimeText.Text;
            if (CurrentCheck.TargetType == TargetType.InputRuntimeFromText)
            {
                double RuntimeTextToDouble = 0;
                double.TryParse(this.RuntimeTextValue, out RuntimeTextToDouble);
                this.CurrentCheck.CalculatedTargeValue = RuntimeTextToDouble;
            }
            if (!this.IsLoading)
                Process();
        }

        private void ReferenceStep_Changed(object sender, DevExpress.Xpo.ObjectChangeEventArgs e)
        {
            if (e.PropertyName == nameof(Check.CalculatedTargeValue) && CurrentCheck.TargetType == TargetType.FromOtherStep)
            {
                this.CurrentCheck.CalculatedTargeValue = Convert.ToDouble(e.NewValue);
                this.Target.Value = Convert.ToDecimal(this.CurrentCheck.CalculatedTargeValue);
            }
            if (!this.IsLoading)
                Process();
        }

        private void Target_ValueChanged(object sender, EventArgs e)
        {
            this.CurrentCheck.CalculatedTargeValue = Convert.ToDouble(this.Target.Value);
            if (CurrentCheck.TargetType == TargetType.InputRuntime)
            {
                this.TargetValue = this.CurrentCheck.CalculatedTargeValue;
            }
            if (!this.IsLoading)
                Process();
        }

        public override int StartTabIndex(int StartIndex)
        {
            this.Misurato.TabIndex = StartIndex + 1;
            this.PercentageLow.TabIndex = StartIndex + 2;
            this.PercentageHigh.TabIndex = StartIndex + 3;
            this.PercentageLow.TabIndex = StartIndex + 4;
            this.Radio.TabIndex = StartIndex + 5;
            this.LastTabIndex = this.Radio.TabIndex;
            return LastTabIndex;
        }

        private void ToleranceHigh_ValueChanged(object sender, EventArgs e)
        {
            this.HighToleranceValue = Convert.ToDouble(ToleranceHigh.Value);
            if (!this.IsLoading)
                Process();
        }

        private void ToleranceLow_ValueChanged(object sender, EventArgs e)
        {
            this.LowToleranceValue = Convert.ToDouble(ToleranceLow.Value);
            if (!this.IsLoading)
                Process();
        }

        private void PercentageLow_ValueChanged(object sender, EventArgs e)
        {
            this.LowPercentageValue = Convert.ToDouble(PercentageLow.Value);
            if (!this.IsLoading)
                Process();
        }

        private void PercentageHigh_ValueChanged(object sender, EventArgs e)
        {
            this.HighPercentageValue = Convert.ToDouble(PercentageHigh.Value);
            if (!this.IsLoading)
                Process();
        }

        private void Misurato_TextChanged(object sender, EventArgs e)
        {
            this.ReadValue = Convert.ToDouble(this.Misurato.Value);
            if (!this.IsLoading)
                Process();
        }

        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(sender, e);
            //if (this.IsLoading)
            //    return;

            if (e.PropertyName == nameof(ReadValue))
            {
                this.Misurato.Value = Convert.ToDecimal(ReadValue);
            }
            if (e.PropertyName == nameof(RuntimeTextValue))
            {
                this.RuntimeText.Text = this.RuntimeTextValue;
            }
            //if (e.PropertyName == nameof(ReadValue))
            //{
            //    this.Misurato.Value = Convert.ToDecimal(ReadValue);
            //    //Evaluate();
            //}
            if (e.PropertyName == nameof(TargetValue))
            {
                this.Target.Value = Convert.ToDecimal(this.TargetValue);
            }
            if (e.PropertyName == nameof(LowPercentageValue))
            {
                this.PercentageLow.EditValue = Convert.ToDecimal(LowPercentageValue);
            }
            if (e.PropertyName == nameof(HighPercentageValue))
            {
                this.PercentageHigh.EditValue = Convert.ToDecimal(HighPercentageValue);
            }
            if (e.PropertyName == nameof(LowPercentageValue))
            {
                this.PercentageLow.EditValue = Convert.ToDecimal(LowPercentageValue);
            }
            if (e.PropertyName == nameof(LowToleranceValue))
            {
                this.ToleranceLow.EditValue = Convert.ToDecimal(LowToleranceValue);
            }
            if (e.PropertyName == nameof(HighToleranceValue))
            {
                this.ToleranceHigh.EditValue = Convert.ToDecimal(HighToleranceValue);
            }
        }

        //protected void Evaluate()
        //{
        //    switch (VarianceType)
        //    {
        //        case VarianceType.DefaultValue:
        //            break;

        //        case VarianceType.Percentage:
        //            if (this.Misurato.Value.IsWithin(Convert.ToDecimal(this.CurrentCheck.LoPercentage), Convert.ToDecimal(this.CurrentCheck.HiPercentage)))
        //            {
        //                this.OkValue = OkType.Ok;
        //            }
        //            else
        //            {
        //                this.OkValue = OkType.Ko;
        //            }
        //            break;

        //        case VarianceType.Tollerance:
        //            if (this.Misurato.Value.IsWithin(Convert.ToDecimal(this.CurrentCheck.LoTolerance), Convert.ToDecimal(this.CurrentCheck.HiTolerance)))
        //            {
        //                this.OkValue = OkType.Ok;
        //            }
        //            else
        //            {
        //                this.OkValue = OkType.Ko;
        //            }
        //            break;

        //        case VarianceType.MinOfBoth:
        //            break;

        //        case VarianceType.MaxOfBoth:
        //            break;

        //        case VarianceType.FromOtherStep:
        //            break;

        //        case VarianceType.PercentagePlusFromOtherStep:
        //            break;
        //    }
        //}

        protected virtual void DisableOkKoNA()
        {
            this.Radio.Enabled = false;
        }
    }
}