﻿namespace AmbulApp.Module.Win.Controls
{
    partial class M02
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).BeginInit();
            this.MainLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).BeginInit();
            this.SuspendLayout();
            // 
            // InternalText
            // 
            this.InternalText.Size = new System.Drawing.Size(651, 36);
            // 
            // StepDescription
            // 
            this.StepDescription.Location = new System.Drawing.Point(12, 52);
            this.StepDescription.Size = new System.Drawing.Size(457, 36);
            // 
            // Radio
            // 
            this.Radio.Location = new System.Drawing.Point(473, 52);
            // 
            // MainLayout
            // 
            this.MainLayout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(725, 2, 650, 400);
            this.MainLayout.Size = new System.Drawing.Size(675, 100);
            this.MainLayout.Controls.SetChildIndex(this.StepDescription, 0);
            this.MainLayout.Controls.SetChildIndex(this.Radio, 0);
            this.MainLayout.Controls.SetChildIndex(this.InternalText, 0);
            // 
            // MainGroup
            // 
            this.MainGroup.Size = new System.Drawing.Size(675, 100);
            // 
            // LayoutItemInternalText
            // 
            this.LayoutItemInternalText.Size = new System.Drawing.Size(655, 40);
            // 
            // LayoutItemOkKo
            // 
            this.LayoutItemOkKo.Location = new System.Drawing.Point(461, 40);
            // 
            // LayoutItemStepDescription
            // 
            this.LayoutItemStepDescription.Location = new System.Drawing.Point(0, 40);
            this.LayoutItemStepDescription.Size = new System.Drawing.Size(461, 40);
            // 
            // M02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "M02";
            this.Size = new System.Drawing.Size(675, 100);
            ((System.ComponentModel.ISupportInitialize)(this.Radio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainLayout)).EndInit();
            this.MainLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemInternalText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemOkKo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutItemStepDescription)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
    }
}
