﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;

namespace AmbulApp.Module.Win.Controls
{
    public partial class M01 : CustomControlBase
    {
        public M01(Check CurrentCheck, List<Check> AllChecks) : base(CurrentCheck, AllChecks)
        {
            InitializeComponent();
            if (this.DesignMode)
                return;

            this.Radio.EditValueChanged += Radio_EditValueChanged;
            this.InternalText.Font = this.GetInternalTextFont();
            this.StepDescription.Font = this.GetStepDescriptionFont();
            this.Radio.Font = new Font(this.Radio.Font.FontFamily, this.EditorsFontSize);
        }

        public M01() : base()
        {
            InitializeComponent();
        }

        private void Radio_EditValueChanged(object sender, EventArgs e)
        {
            this.OkValue = (OkType)this.Radio.EditValue;
        }

        public override int StartTabIndex(int StartIndex)
        {
            this.Radio.TabIndex = StartIndex + 1;
            this.LastTabIndex = this.Radio.TabIndex;
            return LastTabIndex;
        }

        protected override void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(sender, e);
            if (e.PropertyName == nameof(StepDescriptionValue))
            {
                this.StepDescription.Text = this.StepDescriptionValue;
            }
            if (e.PropertyName == nameof(OkValue))
            {
                this.Radio.EditValue = this.OkValue;
            }
            if (e.PropertyName == nameof(InternalTextValue))
            {
                this.InternalText.Text = InternalTextValue;
            }
        }
    }
}