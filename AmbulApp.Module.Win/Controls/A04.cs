﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using AmbulApp.Module.BusinessObjects;
using DevExpress.XtraLayout.Utils;

namespace AmbulApp.Module.Win.Controls
{
    public partial class A04 : A02
    {
        public A04(Check CurrentCheck, List<Check> AllChecks) : base(CurrentCheck, AllChecks)
        {
            InitializeComponent();
        }

        //public A04() : base()
        //{
        //    InitializeComponent();
        //}
        public A04()
        {
            InitializeComponent();
        }

        public override void RuntimeTextEnable()
        {
            this.LiRuntimeText.Visibility = LayoutVisibility.Always;
        }
    }
}