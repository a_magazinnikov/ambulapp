﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;
using AmbulApp.Module.Reports;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraReports.UI;

namespace AmbulApp.Module.Win.Controllers
{
    public class VisitViewController : ObjectViewController<DetailView, Visit>
    {
        public VisitViewController()
        {
            var printPdfAction =
                new SimpleAction(this, "Print PDF", DevExpress.Persistent.Base.PredefinedCategory.Unspecified)
                {
                    ConfirmationMessage = "This will print PDF. Are you sure?",
                    TargetViewType = ViewType.DetailView,
                    TargetObjectsCriteria = "Not IsNull(CustomerSignature)"
                };

            printPdfAction.Execute += PrintPdfActionOnExecute;
            //            printPdfAction.CustomizePopupWindowParams += PrintPdfActionOnCustomizePopupWindowParams;

            var sendPdfAction =
                new SimpleAction(this, "Send PDF", DevExpress.Persistent.Base.PredefinedCategory.Unspecified)
                {
                    ConfirmationMessage = "This will send PDF. Are you sure?",
                    TargetViewType = ViewType.DetailView,
                    TargetObjectsCriteria = "Not IsNull(CustomerSignature)"
                };

            sendPdfAction.Execute += SendPdfActionOnExecute;

            var printSummaryAction = new SimpleAction(this, "Summary print to pick the signature", DevExpress.Persistent.Base.PredefinedCategory.Unspecified)
            {
                ConfirmationMessage = "This will print summary report. Are you sure?",
                TargetViewType = ViewType.DetailView,
                TargetObjectsCriteria = "Not IsNullOrEmpty(Trim(CustomerSignerName))"
            };

            printSummaryAction.Execute += PrintSummaryAction_Execute;
        }

        SummaryReport reportInstance;
        private void PrintSummaryAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();
            var reportData = objectSpace.FindObject<ReportDataV2>(CriteriaOperator.Parse("[DisplayName] = 'Summary print to pick the signature'"));
            ReportsModuleV2 reportsModule = ReportsModuleV2.FindReportsModule(Application.Modules);
            XtraReport report = ReportDataProvider.ReportsStorage.LoadReport(reportData);
            reportInstance = report as SummaryReport;

            reportsModule.ReportsDataSourceHelper.SetupBeforePrint(report);
            ReportPrintTool tool = new ReportPrintTool(report);
            
            if (View.CurrentObject is Visit visit)
            {
       
                report.FilterString = $"[Oid] = '{visit.Oid}'";
                tool.PreviewForm.FormClosed += PreviewForm_FormClosed;
                tool.ShowPreviewDialog();
            }
          
        }

        private void ObjectSpace_Committed(object sender, EventArgs e)
        {
            View.Refresh(true);
        }

        private void PreviewForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            reportInstance.ObjectSpace.Committed += ObjectSpace_Committed;
        }

        //        private void PrintPdfActionOnCustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        //        {
        //            var objectSpace = Application.CreateObjectSpace();
        //            var reportParams = objectSpace.CreateObject<SendReportParams>();
        //            if (!(View.CurrentObject is Visit visit))
        //            {
        //                throw new ArgumentNullException(nameof(visit));
        //            }
        //
        //            ReportDataV2 reportData = objectSpace.FindObject<ReportDataV2>(new BinaryOperator("DisplayName", "CheckList Report"));
        //            ReportsModuleV2 reportsModule = ReportsModuleV2.FindReportsModule(Application.Modules);
        //            if (reportsModule?.ReportsDataSourceHelper != null)
        //            {
        //                var stream = new MemoryStream();
        //                foreach (var intervention in visit.Interventions)
        //                {
        //                    XtraReport report = ReportDataProvider.ReportsStorage.LoadReport(reportData);
        //                    reportsModule.ReportsDataSourceHelper.SetupBeforePrint(report);
        //                    report.FilterString = $"[Oid] = '{intervention.Oid}'";
        //                    report.ExportToPdf(stream);
        //
        //                    reportParams.Files.Add(new FileData(((XPObjectSpace)objectSpace).Session)
        //                    {
        //                        Content = stream.ToArray(),
        //                        FileName = $"{intervention.Equipment.DisplayName.Replace(":", String.Empty).Replace(" ", "_")}.pdf"
        //                    });
        //
        //                    stream.Flush();
        //                }
        //
        //                stream.Close();
        //            }
        //
        //            var view = Application.CreateDetailView(objectSpace, reportParams);
        //            view.ViewEditMode = ViewEditMode.Edit;
        //            view.Caption = "Send message";
        //            e.View = view;
        //        }

        private void PrintPdfActionOnExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (!(e.CurrentObject is Visit visit))
            {
                throw new ArgumentNullException(nameof(visit));
            }

            var objectSpace = Application.CreateObjectSpace();
            ReportDataV2 reportData =
                objectSpace.FindObject<ReportDataV2>(new BinaryOperator("DisplayName", "CheckList Report"));
            ReportsModuleV2 reportsModule = ReportsModuleV2.FindReportsModule(Application.Modules);
            if (reportsModule?.ReportsDataSourceHelper != null)
            {
                foreach (var intervention in visit.Interventions)
                {
                    XtraReport report = ReportDataProvider.ReportsStorage.LoadReport(reportData);
                    reportsModule.ReportsDataSourceHelper.SetupBeforePrint(report);
                    report.FilterString = $"[Oid] = '{intervention.Oid}'";
                    report.ExportToPdf(MakeFileName(intervention));
                }

                if (visit.Interventions.Any())
                {
                    MessageOptions options = new MessageOptions
                    {
                        Duration = 2000,
                        Message = $"Files successfully saved on {Environment.CurrentDirectory}",
                        Type = InformationType.Success,
                        Web = { Position = InformationPosition.Top },
                        Win =
                        {
                            Caption = "Information",
                            Type = WinMessageType.Flyout
                        }
                    };

                    Application.ShowViewStrategy.ShowMessage(options);
                }
            }
        }

        private void SendPdfActionOnExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (!(e.CurrentObject is Visit visit))
            {
                throw new ArgumentNullException(nameof(visit));
            }

            List<MemoryStream> list = new List<MemoryStream>();

            var sendResult = EmailHelper.Instance.SendEmail(new EmailSettings
            {
                To = visit.Company.EmailAddress,
                From = "noreply@solem.it",
                Subject = "Reports",
                Attachments = Attachments(list, visit),
                Body = "Here is body"
            });

            foreach (MemoryStream stream in list)
            {
                stream.Close();
                stream.Flush();
            }

            if (sendResult)
            {
                MessageOptions options = new MessageOptions
                {
                    Duration = 2000,
                    Message = $"Files successfully sent to {visit.Company.EmailAddress}",
                    Type = InformationType.Success,
                    Web = { Position = InformationPosition.Top },
                    Win =
                    {
                        Caption = "Information",
                        Type = WinMessageType.Flyout
                    }
                };

                Application.ShowViewStrategy.ShowMessage(options);
            }
        }

        private static string MakeFileName(Intervention intervention) =>
        (intervention.Equipment.DisplayName.Replace(":", string.Empty) + "_" +
         intervention.Visit.VisitDate.ToString("yyyy-MM-dd") + ".pdf").Replace(" ", "_");

        private List<Attachment> Attachments(List<MemoryStream> list, Visit visit)
        {
            var objectSpace = Application.CreateObjectSpace();
            List<Attachment> attachments = new List<Attachment>();
            ReportDataV2 reportData = objectSpace.FindObject<ReportDataV2>(new BinaryOperator("DisplayName", "CheckList Report"));
            ReportDataV2 summaryReportData = objectSpace.FindObject<ReportDataV2>(new BinaryOperator("DisplayName", "Summary print to pick the signature"));
            ReportsModuleV2 reportsModule = ReportsModuleV2.FindReportsModule(Application.Modules);
            if (reportsModule?.ReportsDataSourceHelper != null)
            {
                foreach (var intervention in visit.Interventions)
                {
                    XtraReport report = ReportDataProvider.ReportsStorage.LoadReport(reportData);
                    reportsModule.ReportsDataSourceHelper.SetupBeforePrint(report);
                    report.FilterString = $"[Oid] = '{intervention.Oid}'";
                    var stream = new MemoryStream();
                    report.ExportToPdf(stream);
                    var fileName = MakeFileName(intervention);
                    stream.Seek(0, SeekOrigin.Begin);
                    Attachment attachment = new Attachment(stream, fileName, "application/pdf");
                    attachments.Add(attachment);
                    list.Add(stream);
                }

                AddSummaryReport(list, visit, attachments, summaryReportData, reportsModule);

            }
            return attachments;
        }

        private static void AddSummaryReport(List<MemoryStream> list, Visit visit, List<Attachment> attachments, ReportDataV2 summaryReportData, ReportsModuleV2 reportsModule)
        {
            XtraReport summaryReport = ReportDataProvider.ReportsStorage.LoadReport(summaryReportData);
            reportsModule.ReportsDataSourceHelper.SetupBeforePrint(summaryReport);
            summaryReport.FilterString = $"[Oid] = '{visit.Oid}'";
            var stream1 = new MemoryStream();
            summaryReport.ExportToPdf(stream1);
            stream1.Seek(0, SeekOrigin.Begin);
            Attachment attachment1 = new Attachment(stream1, "RIEPILOGO.pdf", "application/pdf");
            attachments.Add(attachment1);
            list.Add(stream1);
        }
    }
}
