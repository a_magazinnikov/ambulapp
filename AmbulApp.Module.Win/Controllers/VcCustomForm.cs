﻿using AmbulApp.Module.BusinessObjects;
using AmbulApp.Module.Win.Controls;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;

namespace AmbulApp.Module.Win.Controllers
{
    public class VcCustomForm : ViewController
    {
        private SimpleAction _showForm;
        public VcCustomForm()
        {
            this.TargetObjectType = typeof(Intervention);
//            this.TargetViewType = ViewType.DetailView;
            _showForm = new SimpleAction(this, "ShowForm", PredefinedCategory.Menu)
            {
                Caption = "Show form",
                ToolTip = "Show check form for the selected intervention record",
                SelectionDependencyType = SelectionDependencyType.RequireSingleObject
            };

            _showForm.Execute += ShowForm_Execute;
        }

        private void ShowForm_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var form = new ReportForm(this.Application, e.CurrentObject as Intervention, this.View);
            form.ShowDialog();
        }

        private ListViewProcessCurrentObjectController _processCurrentObjectController;
        protected override void OnActivated()
        {
            base.OnActivated();
            _processCurrentObjectController =
                Frame.GetController<ListViewProcessCurrentObjectController>();
            if (_processCurrentObjectController != null)
            {
                _processCurrentObjectController.CustomProcessSelectedItem +=
                    processCurrentObjectController_CustomProcessSelectedItem;
            }
        }

        private void processCurrentObjectController_CustomProcessSelectedItem(
            object sender, CustomProcessListViewSelectedItemEventArgs e)
        {
            e.Handled = true;
            _showForm.DoExecute();
        }

        public SimpleAction DefaultListViewAction
        {
            get => _showForm;
            set => _showForm = value;
        }

        protected override void OnDeactivated()
        {
            if (_processCurrentObjectController != null)
            {
                _processCurrentObjectController.CustomProcessSelectedItem -=
                    processCurrentObjectController_CustomProcessSelectedItem;
            }
            base.OnDeactivated();
        }
    }
}
