﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Win.Editors;

namespace AmbulApp.Module.Win.Controllers
{
    public class MaintenancePlanLookupListViewController : ViewController<ListView>
    {
        public MaintenancePlanLookupListViewController()
        {
            TargetObjectType = typeof(MaintenancePlan);
        }

        protected override void OnActivated()
        {
            base.OnActivated();
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if (Frame.Context == TemplateContext.LookupControl || Frame.Context == TemplateContext.LookupWindow)
            {
                var listEditor = View?.Editor as GridListEditor;
                var gridView = listEditor?.GridView;
                if (gridView != null)
                {
                    gridView.OptionsBehavior.Editable = false;
                }
            }
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }
    }
}
