﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;

namespace AmbulApp.Module.Win.Controllers
{
    public class ListViewWinCustomizationController : ViewController<ListView>
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            Frame.GetController<DevExpress.ExpressApp.Win.SystemModule.WinExportController>().Active.SetItemValue("disable", false);
            Frame.GetController<DevExpress.ExpressApp.Win.SystemModule.PrintingController>().Active.SetItemValue("disable", false);
        }
    }
}
