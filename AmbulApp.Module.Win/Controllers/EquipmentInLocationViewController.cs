﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using AmbulApp.Module.Win.Controls;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.SystemModule;

namespace AmbulApp.Module.Win.Controllers
{
    public class EquipmentInLocationViewController : ViewController
    {
        public EquipmentInLocationViewController()
        {
            this.TargetObjectType = typeof(EquipmentInLocation);
        }


        private ListViewProcessCurrentObjectController _processCurrentObjectController;
        private Visit _visit;

        protected override void OnActivated()
        {
            base.OnActivated();
            _processCurrentObjectController =
                Frame.GetController<ListViewProcessCurrentObjectController>();
            if (_processCurrentObjectController != null)
            {
                _processCurrentObjectController.CustomProcessSelectedItem +=
                    processCurrentObjectController_CustomProcessSelectedItem;
            }

            if ((View as ListView)?.CollectionSource is PropertyCollectionSource collectionSource)
            {
                collectionSource.MasterObjectChanged += OnMasterObjectChanged;
                if (collectionSource.MasterObject != null) UpdateMasterObject(collectionSource.MasterObject);
            }
        }

        private void OnMasterObjectChanged(object sender, EventArgs e)
        {
            UpdateMasterObject(((PropertyCollectionSource)sender).MasterObject);
        }

        private void UpdateMasterObject(object masterObject)
        {
            _visit = masterObject as Visit;
        }

        private void processCurrentObjectController_CustomProcessSelectedItem(
            object sender, CustomProcessListViewSelectedItemEventArgs e)
        {
            e.Handled = true;
            var equipmentInLocation = e.InnerArgs.CurrentObject as EquipmentInLocation;
            var objectSpace = View.ObjectSpace;
            if (equipmentInLocation != null)
            {
                var intervention = objectSpace.FindObject<Intervention>(CriteriaOperator.Parse(
                    $"Visit is not null and Visit.Oid == '{_visit.Oid}' and Equipment is not null and Equipment.Oid == '{equipmentInLocation.Key.EquipmentOid}' and MaintenancePlan is not null and MaintenancePlan.Oid == '{equipmentInLocation.Key.MaintenancePlanOid}'"));
                if (intervention == null)
                {

                    intervention = objectSpace.CreateObject<Intervention>();
                    intervention.Equipment = objectSpace.GetObjectByKey<Equipment>(equipmentInLocation.Key.EquipmentOid);
                    intervention.MaintenancePlan = objectSpace.GetObjectByKey<MaintenancePlan>(equipmentInLocation.Key.MaintenancePlanOid);
                    intervention.Visit = _visit;
                    intervention.ExecutionDate = DateTime.Today;
                    if (CreateCheckList(intervention))
                    {
                        objectSpace.CommitChanges();
                        var form = new ReportForm(this.Application, intervention, this.View);
                        form.ShowDialog();
                    }
                    else
                    {
                        throw new UserFriendlyException("Some issues in generating checklist.");
                    }

                    View.ObjectSpace.Refresh();
                }
                else
                {
                    throw new UserFriendlyException("Intervention is already created.");
                }
            }
        }

        private bool CreateCheckList(Intervention intervention)
        {
            if (intervention.MaintenancePlan != null && intervention.MaintenancePlan.MaintenanceSteps.Any())
            {
                foreach (var maintenanceStep in intervention.MaintenancePlan.MaintenanceSteps)
                {
                    var check = ObjectSpace.CreateObject<Check>();

                    check.Block = maintenanceStep.Block;
                    check.StepType = maintenanceStep.StepType;
                    check.Column1 = maintenanceStep.Column1;
                    check.Column2 = maintenanceStep.Column2;
                    check.Column3 = maintenanceStep.Column3;
                    check.Column4 = maintenanceStep.Column4;
                    check.Column5 = maintenanceStep.Column5;
                    check.Column6 = maintenanceStep.Column6;
                    check.HiTolerance = maintenanceStep.HiTolerance;
                    check.HideIfSequence = maintenanceStep.HideIfSequence;
                    check.HiPercentage = maintenanceStep.HiPercentage;
                    check.HideIfValue = maintenanceStep.HideIfValue;
                    check.InternalText = maintenanceStep.InternalText;
                    check.LimitPrintStyle = maintenanceStep.LimitPrintStyle;
                    check.LoPercentage = maintenanceStep.LoPercentage;
                    check.LoTolerance = maintenanceStep.LoTolerance;
                    check.OkPrintStyle = maintenanceStep.OkPrintStyle;
                    check.PrintSequence = maintenanceStep.PrintSequence;
                    check.Sequence = maintenanceStep.Sequence;
                    check.TargetValue = maintenanceStep.TargetValue;
                    check.TargetSequence = maintenanceStep.TargetSequence;
                    check.ValueInputs = maintenanceStep.ValueInputs;
                    check.TargetType = maintenanceStep.TargetType;
                    check.VarianceSeq = maintenanceStep.VarianceSeq;
                    check.VarianceType = maintenanceStep.VarianceType;
                    check.StepDescription = maintenanceStep.StepDescription;
                    check.VarianceType = maintenanceStep.VarianceType;

                    intervention.Checks.Add(check);
                }

                return true;
            }

            return false;
        }

        protected override void OnDeactivated()
        {
            if (_processCurrentObjectController != null)
            {
                _processCurrentObjectController.CustomProcessSelectedItem -=
                    processCurrentObjectController_CustomProcessSelectedItem;
            }

            if ((View as ListView)?.CollectionSource is PropertyCollectionSource collectionSource)
            {
                collectionSource.MasterObjectChanged -= OnMasterObjectChanged;
            }

            base.OnDeactivated();
        }
    }
}
