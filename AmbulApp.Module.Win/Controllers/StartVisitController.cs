﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;

namespace AmbulApp.Module.Win.Controllers
{
    public class StartVisitController : ObjectViewController<DetailView, ScheduledVisit>
    {
        public StartVisitController()
        {
            var startVisitAction = new SimpleAction(
                this, "StartTheVisit", PredefinedCategory.Unspecified);
            startVisitAction.Execute += StartVisitActionOnExecute;
        }

        private void StartVisitActionOnExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            var scheduledVisit = e.CurrentObject as ScheduledVisit;
            var objectSpace = Application.CreateObjectSpace(typeof(Visit));
            DeleteVisitAgreedDateRecord(objectSpace, scheduledVisit);
            var objectToShow = objectSpace.CreateObject<Visit>();
            if (objectToShow != null && scheduledVisit != null)
            {
                objectToShow.VisitDate = DateTime.Today;
                objectToShow.Location = objectSpace.GetObjectByKey<Location>(scheduledVisit.Key.LocationOid);
                objectToShow.Company = objectSpace.GetObjectByKey<Company>(scheduledVisit.Key.CompanyOid);
                var createdView = Application.CreateDetailView(objectSpace, objectToShow);
                createdView.ViewEditMode = ViewEditMode.Edit;
                e.ShowViewParameters.CreatedView = createdView;
            }
        }

        private void DeleteVisitAgreedDateRecord(IObjectSpace objectSpace, ScheduledVisit scheduledVisit)
        {
            var visitAgreedDate = objectSpace.FindObject<VisitAgreedDate>(CriteriaOperator.Parse("Location == ?", scheduledVisit.Key.LocationOid));
            visitAgreedDate?.Delete();
        }
    }
}
