﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.Persistent.Base;

namespace AmbulApp.Module.Win.Controllers
{
    public class EditInterventionController : ViewController<ListView>
    {
        public EditInterventionController()
        {
            TargetObjectType = typeof(Intervention);
            SimpleAction editInterventionAction =
                new SimpleAction(this, "EditInterventionRecord", PredefinedCategory.Reports)
                {
                    ImageName = "Action_Edit",
                    SelectionDependencyType = SelectionDependencyType.RequireSingleObject,
                    Caption = "Details"
                };

            editInterventionAction.Execute += editInterventionAction_Execute;
        }

        void editInterventionAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            ListViewProcessCurrentObjectController.ShowObject(
                e.CurrentObject, e.ShowViewParameters, Application, Frame, View);
        }
    }
}
