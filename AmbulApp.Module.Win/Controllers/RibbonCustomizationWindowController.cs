﻿using System;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.Utils;
using DevExpress.XtraBars.Ribbon;

namespace AmbulApp.Module.Win.Controllers
{
    public class RibbonCustomizationWindowController : WindowController
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            Window.TemplateChanged += Window_TemplateChanged;
        }

        private void Window_TemplateChanged(object sender, EventArgs e)
        {
            if (Frame.Template is RibbonForm ribbonForm && ribbonForm.Ribbon != null)
            {
                RibbonControl ribbon = ribbonForm.Ribbon;
                if (SecuritySystem.CurrentUser is AmbulUser user && user.Technician)
                {
                    ribbon.ShowPageHeadersMode = ShowPageHeadersMode.Hide;
                    ribbon.ShowItemCaptionsInQAT = false;
                    ribbon.ShowToolbarCustomizeItem = false;
                    ribbon.ShowApplicationButton = DefaultBoolean.False;
                    ribbon.ShowDisplayOptionsMenuButton = DefaultBoolean.False;
                    ribbon.ShowExpandCollapseButton = DefaultBoolean.False;
                    ribbon.ShowQatLocationSelector = false;
                }
            }
        }

        protected override void OnDeactivated()
        {
            Window.TemplateChanged -= Window_TemplateChanged;
            base.OnDeactivated();
        }
    }
}
