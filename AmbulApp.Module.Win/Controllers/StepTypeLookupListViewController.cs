﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.ExpressApp.Win.SystemModule;

namespace AmbulApp.Module.Win.Controllers
{
    public class StepTypeLookupListViewController : ViewController<ListView>
    {
        public StepTypeLookupListViewController()
        {
            TargetObjectType = typeof(StepType);
        }


        protected override void OnActivated()
        {
            base.OnActivated();
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if (Frame.Context == TemplateContext.LookupControl || Frame.Context == TemplateContext.LookupWindow)
            {
                var listEditor = View?.Editor as GridListEditor;
                var gridView = listEditor?.GridView;
                if (gridView != null)
                {
                    gridView.OptionsBehavior.Editable = false;
                }
            }
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }
    }
}
