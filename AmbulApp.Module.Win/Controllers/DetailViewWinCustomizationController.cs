﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Win.Editors;

namespace AmbulApp.Module.Win.Controllers
{
    public class DetailViewWinCustomizationController : ViewController<DetailView>
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            Frame.GetController<DevExpress.ExpressApp.Validation.AllContextsView.ShowAllContextsController>().Active.SetItemValue("validation", false);
            foreach (IntegerPropertyEditor propertyEditor in View.GetItems<IntegerPropertyEditor>())
            {
                propertyEditor.ControlCreated += propertyEditor_ControlCreated;
            }
        }

        void propertyEditor_ControlCreated(object sender, EventArgs e)
        {
            IntegerPropertyEditor propertyEditor = (IntegerPropertyEditor)sender;
            propertyEditor.Control.Properties.Buttons[propertyEditor.Control.Properties.SpinButtonIndex].Visible = false;
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            foreach (IntegerPropertyEditor propertyEditor in View.GetItems<IntegerPropertyEditor>())
            {
                propertyEditor.ControlCreated -= new EventHandler<EventArgs>(propertyEditor_ControlCreated);
            }
        }
    }
}
