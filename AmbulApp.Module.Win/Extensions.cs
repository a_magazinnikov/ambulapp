﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmbulApp.Module.Win
{
    public static class Extensions
    {
        public static bool IsWithin(this double value, double minimum, double maximum)
        {
            if (value.CompareTo(minimum) < 0)
                return false;
            if (value.CompareTo(maximum) > 0)
                return false;
            return true;
        }
    }
}
