﻿using AmbulApp.Module.BusinessObjects;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;
using System;
using System.Linq;

namespace AmbulApp.OData
{
    public static class XpoHelper
    {
        static readonly Type[] EntityTypes = new Type[] {
            typeof(OperatorManual)
        };
        public static void InitXpo(string connectionString)
        {
            var dictionary = PrepareDictionary();
            if (XpoDefault.DataLayer == null)
            {
                using (var updateDataLayer = XpoDefault.GetDataLayer(connectionString, dictionary, AutoCreateOption.DatabaseAndSchema))
                {
                    updateDataLayer.UpdateSchema(false, dictionary.CollectClassInfos(EntityTypes));
                    new UnitOfWork(updateDataLayer).CreateObjectTypeRecords();
                }
            }
            var dataStore = XpoDefault.GetConnectionProvider(connectionString, AutoCreateOption.SchemaAlreadyExists);
            XpoDefault.DataLayer = new ThreadSafeDataLayer(dictionary, dataStore);
            XpoDefault.Session = null;

        }
        public static UnitOfWork CreateUnitOfWork()
        {
            return new UnitOfWork();
        }
        static XPDictionary PrepareDictionary()
        {
            var dict = new ReflectionDictionary();
            dict.GetDataStoreSchema(EntityTypes);
            return dict;
        }


    }
}