﻿using AmbulApp.Module.BusinessObjects;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AmbulApp.OData
{
    /// <summary>
    /// Summary description for DownloadManual
    /// </summary>
    public class DownloadManual : IHttpHandler
    {

        public bool IsReusable => true;


        static string connection;
        public DownloadManual()
        {
            connection = System.Configuration.ConfigurationManager.
             ConnectionStrings["ConnectionString"].ConnectionString;
        }
        public void ProcessRequest(HttpContext context)
        {
            Guid Oid = Guid.Parse(context.Request.QueryString["Oid"]);

            XpoHelper.InitXpo(connection);

            var UoW = XpoHelper.CreateUnitOfWork();

            var Manual = UoW.Query<OperatorManual>().FirstOrDefault(op => op.Oid == Oid);

        
          

            var file = new MemoryStream();
            Manual.File.SaveToStream(file);
            var filename = Manual.File.FileName;

            if (file != null && file.CanRead)
            {
                context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
                context.Response.ContentType = "application/octet-stream";
                context.Response.ClearContent();
                file.CopyTo(context.Response.OutputStream);
            }
        }
    }
}