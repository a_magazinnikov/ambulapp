﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DevExpress.Xpo;
using DevExpress.Xpo.Helpers;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq.Expressions;
using System.Reflection;
using System.Diagnostics;
using System.Configuration;
using DevExpress.Data.Filtering;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Persistent.Base;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace AmbulApp.OData
{
    public class AmbulAppContext : XpoContext
    {
        public AmbulAppContext(string s1, string s2, IDataLayer dataLayer)
            : base(s1, s2, dataLayer)
        {
        }
        // Place Actions here.
        // [Action]
        // public bool ActionName(EntityType entity) {
        //     return true;
        // }
    }

#if DEBUG
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, IncludeExceptionDetailInFaults = true)]
    [JSONPSupportBehavior]
#else
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [JSONPSupportBehavior]
#endif
    public class AmbulAppService : XpoDataServiceV3
    {
        static readonly AmbulAppContext ServiceContext = new AmbulAppContext("AmbulApp.ODataService", "AmbulApp.Module.BusinessObjects", CreateDataLayer());

        List<string> whiteListMethod;
        bool useAuthentication = Convert.ToBoolean(ConfigurationManager.AppSettings["UseAuthentication"]);
        public AmbulAppService() : base(ServiceContext, ServiceContext)
        {
            var Methods = ConfigurationManager.AppSettings["WhiteListMethod"];
            whiteListMethod = Methods.Split(';').ToList();

        }

        static IDataLayer CreateDataLayer()
        {
            try
            {
                //TODO if you use an app made on template XAF 16.2.4 or greater you have to uncomment the following lines

                PasswordCryptographer.EnableRfc2898 = true;
                PasswordCryptographer.SupportLegacySha512 = false;

                var connectionString =
                System.Configuration.ConfigurationManager.
                ConnectionStrings["ConnectionString"].ConnectionString;
                DevExpress.Xpo.Metadata.XPDictionary dict = new DevExpress.Xpo.Metadata.ReflectionDictionary();
                dict.GetDataStoreSchema(typeof(AmbulApp.Module.AmbulAppModule).Assembly);
                //dict.GetDataStoreSchema(Assembly.GetCallingAssembly());
                IDataLayer dataLayer = new ThreadSafeDataLayer(dict, DevExpress.Xpo.XpoDefault.GetConnectionProvider(connectionString, DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema));
                //IDataLayer dataLayer = new SimpleDataLayer(dict, DevExpress.Xpo.XpoDefault.GetConnectionProvider(ConnectionString, DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema)); 
                //UnitOfWork UoW = new UnitOfWork(dataLayer);
                //UoW.CreateObjectTypeRecords();
                //UoW.UpdateSchema();
                XpoDefault.DataLayer = dataLayer;
                XpoDefault.Session = null;
                return dataLayer;
            }
            catch (Exception exception)
            {

                Debug.WriteLine($"exception.Message:{exception.Message}");
                if (exception.InnerException != null)
                {
                    Debug.WriteLine($"exception.InnerException.Message:{exception.InnerException.Message}");

                }
                Debug.WriteLine($" exception.StackTrace:{exception.StackTrace}");
            }

            return null;
        }

        protected override void OnStartProcessingRequest(ProcessRequestArgs args)
        {
            base.OnStartProcessingRequest(args);
        }

        // This method is called just once to initialize global service policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            //config.SetEntitySetAccessRule("*", EntitySetRights.All
            //TODO only add permissions to the tables that we want to expose

            config.SetEntitySetAccessRule("AccessoryType", EntitySetRights.All);
            config.SetEntitySetAccessRule("AmbulUser", EntitySetRights.All);
            config.SetEntitySetAccessRule("AttachmentCategory", EntitySetRights.All);
            config.SetEntitySetAccessRule("Block", EntitySetRights.All);
            config.SetEntitySetAccessRule("Brand", EntitySetRights.All);
            config.SetEntitySetAccessRule("ChangeMyCompany", EntitySetRights.All);
            config.SetEntitySetAccessRule("Check", EntitySetRights.All);
            config.SetEntitySetAccessRule("Company", EntitySetRights.All);
            config.SetEntitySetAccessRule("Equipment", EntitySetRights.All);
            config.SetEntitySetAccessRule("ExternalAccessory", EntitySetRights.All);
            config.SetEntitySetAccessRule("Check", EntitySetRights.All);
            config.SetEntitySetAccessRule("Company", EntitySetRights.All);
            config.SetEntitySetAccessRule("Equipment", EntitySetRights.All);
            config.SetEntitySetAccessRule("Family", EntitySetRights.All);
            config.SetEntitySetAccessRule("FamilyType", EntitySetRights.All);
            config.SetEntitySetAccessRule("InternalModule", EntitySetRights.All);
            config.SetEntitySetAccessRule("Intervention", EntitySetRights.All);
            config.SetEntitySetAccessRule("Location", EntitySetRights.All);
            config.SetEntitySetAccessRule("MaintenancePlan", EntitySetRights.All);
            config.SetEntitySetAccessRule("MaintenanceStep", EntitySetRights.All);
            config.SetEntitySetAccessRule("Model", EntitySetRights.All);
            config.SetEntitySetAccessRule("ModelAttachment", EntitySetRights.All);
            config.SetEntitySetAccessRule("OperatorManual", EntitySetRights.All);
            config.SetEntitySetAccessRule("Parameter", EntitySetRights.All);
            config.SetEntitySetAccessRule("ReadedManual", EntitySetRights.All);
            config.SetEntitySetAccessRule("SparePart", EntitySetRights.All);
            config.SetEntitySetAccessRule("SparePartCategory", EntitySetRights.All);
            config.SetEntitySetAccessRule("StepType", EntitySetRights.All);
            config.SetEntitySetAccessRule("Visit", EntitySetRights.All);

            config.DataServiceBehavior.AcceptAnyAllRequests = true;
            config.SetServiceOperationAccessRule("*", ServiceOperationRights.All);
            config.SetServiceActionAccessRule("*", ServiceActionRights.Invoke);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
            config.DataServiceBehavior.AcceptProjectionRequests = true;
            config.DataServiceBehavior.AcceptCountRequests = true;
            config.AnnotationsBuilder = CreateAnnotationsBuilder(() => ServiceContext);
            config.DataServiceBehavior.AcceptReplaceFunctionInQuery = true;
            config.DataServiceBehavior.AcceptSpatialLiteralsInQuery = true;
            config.DisableValidationOnMetadataWrite = true;
#if DEBUG
            config.UseVerboseErrors = true;
#endif
        }

        // Place OData Interceptors here, one for each Entity type.
        // [QueryInterceptor("EntityType")]
        // public Expression<Func<EntityType, bool>> OnQuery() {
        //    return o => true;
        // }
        [WebGet]
        public string Login(string UserName, string Password)
        {


            var Dal = CreateDataLayer();
            UnitOfWork UoW = new UnitOfWork(Dal);
            var User = UoW.FindObject<AmbulUser>(new BinaryOperator("UserName", UserName));
            if (User == null)
            {
                throw new DataServiceException(401, "Invalid login or password");
            }
            if (!User.ComparePassword(Password))
            {
                throw new DataServiceException(401, "Invalid login or password");
            }

            // Define const Key this should be private secret key  stored in some safe place
            string key = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";

            // Create Security key  using private key above:
            // not that latest version of JWT using Microsoft namespace instead of System
            var securityKey = new Microsoft
               .IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            // Also note that securityKey length should be >256b
            // so you have to make sure that your private key has a proper length
            //
            var credentials = new Microsoft.IdentityModel.Tokens.SigningCredentials
                              (securityKey, SecurityAlgorithms.HmacSha256Signature);

            //  Finally create a Token
            var header = new JwtHeader(credentials);

            //Some PayLoad that contain information needed
            var payload = new JwtPayload
            {
               { "UserOid ", User.Oid},
            };

       
            JwtSecurityToken secToken = new JwtSecurityToken(header, payload);

            var handler = new JwtSecurityTokenHandler();

            // Token to String so you can use it in your client
            var tokenString = handler.WriteToken(secToken);

            return tokenString;

          
        }
        JwtPayload ValidateJwtToken(string Token)
        {
            var handler = new JwtSecurityTokenHandler();
            // And finally when  you received token from client
            // you can  either validate it or try to  read
            var token = handler.ReadJwtToken(Token);

            JwtPayload Payload = token.Payload;
            return Payload;
        }
        public override object Authenticate(ProcessRequestArgs args)
        {
            try
            {
                if (!useAuthentication)
                {
                    return base.Authenticate(args);
                }

                var Method = args.RequestUri.Segments.Last();
                if (whiteListMethod.Contains(Method))
                {
                    return base.Authenticate(args);
                }

                string Auth = args.OperationContext.RequestHeaders["Auth"];

                if (Auth == null)
                {
                    throw new DataServiceException(401, "Invalid Token");
                }
              
                return ValidateJwtToken(Auth);
            }
            catch (Exception exception)
            {

                Debug.WriteLine(string.Format("{0}:{1}", "exception.Message", exception.Message));
                if (exception.InnerException != null)
                {
                    Debug.WriteLine(string.Format("{0}:{1}", "exception.InnerException.Message", exception.InnerException.Message));

                }
                Debug.WriteLine(string.Format("{0}:{1}", " exception.StackTrace", exception.StackTrace));
            }
            return base.Authenticate(args);
           
        }

        [WebGet]
        public IQueryable<Equipment> GetEquipment()
        {
            const string V = "E";

            return FilteredEquipment(V);
        }
        [WebGet]
        public IQueryable<Equipment> GetVehicles()
        {
            const string V = "V";

            return FilteredEquipment(V);
        }
        private IQueryable<Equipment> FilteredEquipment(string V)
        {
            UnitOfWork uow = new UnitOfWork(Context.ObjectLayer);
            XPQueryCreatorBase xPQueryCreatorBase = Context.GetXPQueryCreator(typeof(Equipment));
            IQueryable<Equipment> queryable = xPQueryCreatorBase.CreateXPQuery(uow).Cast<Equipment>();
            return queryable.Where(e => e.Family.Type.Key == V);
        }

        // Place a single XPO Interceptor that here, one for all Entity types.
        public override LambdaExpression GetQueryInterceptor(Type entityType, object token)
        {
            //Example interceptor for the Employee class
            //if (token != null && entityType == typeof(Employee)){
            //    return (Expression<Func<Employee, bool>>)(o => o.UserName != "Admin");
            //}
            return base.GetQueryInterceptor(entityType, token);
        }

        // Place Service Operations here.
        // [WebGet]
        // public void OperationName(Type arg1, ...) {
        // }
    }
}
