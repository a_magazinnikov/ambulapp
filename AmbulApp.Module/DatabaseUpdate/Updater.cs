﻿using System;
using System.Linq;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;

namespace AmbulApp.Module.DatabaseUpdate
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
    public class Updater : DevExpress.ExpressApp.Updating.ModuleUpdater
    {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) : base(objectSpace, currentDBVersion)
        {
        }

        public override void UpdateDatabaseAfterUpdateSchema()
        {
            base.UpdateDatabaseAfterUpdateSchema();
            CreateStepTypes();
            //            SeedData();
        }

        private void SeedData()
        {
            #region Temp Data

            //Create Companies.
            Company company1 = ObjectSpace.FindObject<Company>(CriteriaOperator.Parse("Title == 'Company 1'"));
            if (company1 == null)
            {
                company1 = ObjectSpace.CreateObject<Company>();
                company1.Title = "Company 1";
                company1.Save();
            }

            Company company2 =
                ObjectSpace.FindObject<Company>(CriteriaOperator.Parse("Title == 'Company 2'"));
            if (company2 == null)
            {
                company2 = ObjectSpace.CreateObject<Company>();
                company2.Title = "Company 2";
                company2.Save();
            }

            Company mainCompany =
                ObjectSpace.FindObject<Company>(CriteriaOperator.Parse("Title == 'Main Company'"));
            if (mainCompany == null)
            {
                mainCompany = ObjectSpace.CreateObject<Company>();
                mainCompany.Title = "Main Company";
                mainCompany.Save();
            }

            //Create users.
            //Admin is a god that can do everything.
            AmbulUser administrator = ObjectSpace.FindObject<AmbulUser>(CriteriaOperator.Parse("UserName == 'Admin'"));
            if (administrator == null)
            {
                administrator = ObjectSpace.CreateObject<AmbulUser>();
                administrator.UserName = "admin@test.com";
                administrator.FirstName = "Admin";
                administrator.LastName = "Admin";
                administrator.EmailAddress = "admin@test.com";
                administrator.SuperAdmin = true;
                administrator.Companies.Add(mainCompany);
                administrator.IsActive = true;
                administrator.SetPassword("");
                administrator.Roles.Add(GetSuperAdminRole());
                administrator.Save();
            }

            //Sam is an admin in company 1 and he can do everything with his own Company
            AmbulUser managerSam = ObjectSpace.FindObject<AmbulUser>(CriteriaOperator.Parse("UserName == 'Sam'"));
            if (managerSam == null)
            {
                managerSam = ObjectSpace.CreateObject<AmbulUser>();
                managerSam.UserName = "sam@test.com";
                managerSam.FirstName = "Sam";
                managerSam.LastName = "Jackson";
                managerSam.IsActive = true;
                managerSam.SetPassword("");
                managerSam.EmailAddress = "sam@test.com";
                managerSam.Companies.Add(company1);
                managerSam.Roles.Add(GetCompanyUserAdminRole());
                managerSam.Save();
            }

            //John is an equipment admin in the company1.
            AmbulUser userJohn = ObjectSpace.FindObject<AmbulUser>(CriteriaOperator.Parse("UserName == 'John'"));
            if (userJohn == null)
            {
                userJohn = ObjectSpace.CreateObject<AmbulUser>();
                userJohn.UserName = "john@test.com";
                userJohn.FirstName = "John";
                userJohn.LastName = "Doe";
                userJohn.IsActive = true;
                userJohn.SetPassword("");
                userJohn.Companies.Add(company1);
                userJohn.EmailAddress = "john@test.com";
                userJohn.Roles.Add(GetCompanyEquipmentAdminRole());
                userJohn.Save();
            }

            //Mary is an admin in company 2  
            AmbulUser managerMary = ObjectSpace.FindObject<AmbulUser>(CriteriaOperator.Parse("UserName == 'Mary'"));
            if (managerMary == null)
            {
                managerMary = ObjectSpace.CreateObject<AmbulUser>();
                managerMary.UserName = "mary@test.com";
                managerMary.FirstName = "Mary";
                managerMary.LastName = "Tellinson";
                managerMary.IsActive = true;
                managerMary.SetPassword("");
                managerMary.Companies.Add(company2);
                managerMary.EmailAddress = "mary@test.com";
                managerMary.Roles.Add(GetCompanyUserAdminRole());
                managerMary.Save();
            }

            //Joe is an equipment admin in the company2.
            AmbulUser userJoe = ObjectSpace.FindObject<AmbulUser>(CriteriaOperator.Parse("UserName == 'Joe'"));
            if (userJoe == null)
            {
                userJoe = ObjectSpace.CreateObject<AmbulUser>();
                userJoe.UserName = "joe@test.com";
                userJoe.FirstName = "Joe";
                userJoe.LastName = "Pitt";
                userJoe.IsActive = true;
                userJoe.SetPassword("");
                userJoe.Companies.Add(company2);
                userJoe.EmailAddress = "joe@test.com";
                userJoe.Roles.Add(GetCompanyEquipmentAdminRole());
                userJoe.Save();
            }

            //Create equipments for companies.
//            var equipmentInCompany1 = ObjectSpace.FindObject<Equipment>(CriteriaOperator.Parse("SerialNumber == '111'"));
//            if (equipmentInCompany1 == null)
//            {
//                equipmentInCompany1 = ObjectSpace.CreateObject<Equipment>();
//                equipmentInCompany1.SerialNumber = "111";
//                equipmentInCompany1.Company = company1;
//                equipmentInCompany1.Save();
//            }
//
//            var equipmentInCompany2 = ObjectSpace.FindObject<Equipment>(CriteriaOperator.Parse("SerialNumber == '222'"));
//            if (equipmentInCompany2 == null)
//            {
//                equipmentInCompany2 = ObjectSpace.CreateObject<Equipment>();
//                equipmentInCompany2.SerialNumber = "222";
//                equipmentInCompany2.Company = company2;
//                equipmentInCompany2.Save();
//            }

            var familyTypeA = ObjectSpace.FindObject<FamilyType>(CriteriaOperator.Parse("Key == 'A'"));
            if (familyTypeA == null)
            {
                familyTypeA = ObjectSpace.CreateObject<FamilyType>();
                familyTypeA.Key = "A";
                familyTypeA.Value = "Analisys Instruments";
                familyTypeA.Save();
            }

            var familyTypeV = ObjectSpace.FindObject<FamilyType>(CriteriaOperator.Parse("Key == 'V'"));
            if (familyTypeV == null)
            {
                familyTypeV = ObjectSpace.CreateObject<FamilyType>();
                familyTypeV.Key = "V";
                familyTypeV.Value = "Vehicles";
                familyTypeV.Save();
            }

            var familyTypeE = ObjectSpace.FindObject<FamilyType>(CriteriaOperator.Parse("Key == 'E'"));
            if (familyTypeE == null)
            {
                familyTypeE = ObjectSpace.CreateObject<FamilyType>();
                familyTypeE.Key = "E";
                familyTypeE.Value = "Equipments";
                familyTypeE.Save();
            }

            var familyV1 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Ambulanze'"));
            if (familyV1 == null)
            {
                familyV1 = ObjectSpace.CreateObject<Family>();
                familyV1.Name = "Ambulanze";
                familyV1.Type = familyTypeV;
                familyV1.Save();
            }

            var familyV2 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Auto mediche'"));
            if (familyV2 == null)
            {
                familyV2 = ObjectSpace.CreateObject<Family>();
                familyV2.Name = "Auto mediche";
                familyV2.Type = familyTypeV;
                familyV2.Save();
            }

            var familyV3 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Elicotteri'"));
            if (familyV3 == null)
            {
                familyV3 = ObjectSpace.CreateObject<Family>();
                familyV3.Name = "Elicotteri";
                familyV3.Type = familyTypeV;
                familyV3.Save();
            }

            var familyE1 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Aspiratori portatili'"));
            if (familyE1 == null)
            {
                familyE1 = ObjectSpace.CreateObject<Family>();
                familyE1.Name = "Aspiratori portatili";
                familyE1.Type = familyTypeE;
                familyE1.Save();
            }

            var familyE2 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Defibrillatori'"));
            if (familyE2 == null)
            {
                familyE2 = ObjectSpace.CreateObject<Family>();
                familyE2.Name = "Defibrillatori";
                familyE2.Type = familyTypeE;
                familyE2.Save();
            }

            var familyE3 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Frigoemoteche'"));
            if (familyE3 == null)
            {
                familyE3 = ObjectSpace.CreateObject<Family>();
                familyE3.Name = "Frigoemoteche";
                familyE3.Type = familyTypeE;
                familyE3.Save();
            }

            var familyE4 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Monitor defibrillatori'"));
            if (familyE4 == null)
            {
                familyE4 = ObjectSpace.CreateObject<Family>();
                familyE4.Name = "Monitor defibrillatori";
                familyE4.Type = familyTypeE;
                familyE4.Save();
            }

            var familyE5 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Pulsossimetri'"));
            if (familyE5 == null)
            {
                familyE5 = ObjectSpace.CreateObject<Family>();
                familyE5.Name = "Pulsossimetri";
                familyE5.Type = familyTypeE;
                familyE5.Save();
            }

            var familyE6 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Riduttori ossigeno'"));
            if (familyE6 == null)
            {
                familyE6 = ObjectSpace.CreateObject<Family>();
                familyE6.Name = "Riduttori ossigeno";
                familyE6.Type = familyTypeE;
                familyE6.Save();
            }

            var familyA1 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Computer'"));
            if (familyA1 == null)
            {
                familyA1 = ObjectSpace.CreateObject<Family>();
                familyA1.Name = "Computer";
                familyA1.Type = familyTypeA;
                familyA1.Save();
            }

            var familyA2 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Misuratore di pressione'"));
            if (familyA2 == null)
            {
                familyA2 = ObjectSpace.CreateObject<Family>();
                familyA2.Name = "Misuratore di pressione";
                familyA2.Type = familyTypeA;
                familyA2.Save();
            }

            var familyA3 = ObjectSpace.FindObject<Family>(CriteriaOperator.Parse("Name == 'Tester'"));
            if (familyA3 == null)
            {
                familyA3 = ObjectSpace.CreateObject<Family>();
                familyA3.Name = "Tester";
                familyA3.Type = familyTypeA;
                familyA3.Save();
            }

            var attachmentCategory = ObjectSpace.FindObject<AttachmentCategory>(CriteriaOperator.Parse("Name == 'Operator Manual'"));
            if (attachmentCategory == null)
            {
                attachmentCategory = ObjectSpace.CreateObject<AttachmentCategory>();
                attachmentCategory.Name = "Operator Manual";
                attachmentCategory.Save();
            }

            var brand1 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Alfa Romeo'"));
            if (brand1 == null)
            {
                brand1 = ObjectSpace.CreateObject<Brand>();
                brand1.Name = "Alfa Romeo";
                brand1.Save();
            }

            var brand2 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Boscarol'"));
            if (brand2 == null)
            {
                brand2 = ObjectSpace.CreateObject<Brand>();
                brand2.Name = "Boscarol";
                brand2.Save();
            }

            var brand3 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Fiat'"));
            if (brand3 == null)
            {
                brand3 = ObjectSpace.CreateObject<Brand>();
                brand3.Name = "Fiat";
                brand3.Save();
            }

            var brand4 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Fluke'"));
            if (brand4 == null)
            {
                brand4 = ObjectSpace.CreateObject<Brand>();
                brand4.Name = "Fluke";
                brand4.Save();
            }

            var brand5 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Ford'"));
            if (brand5 == null)
            {
                brand5 = ObjectSpace.CreateObject<Brand>();
                brand5.Name = "Ford";
                brand5.Save();
            }

            var brand6 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Laerdal'"));
            if (brand6 == null)
            {
                brand6 = ObjectSpace.CreateObject<Brand>();
                brand6.Name = "Laerdal";
                brand6.Save();
            }

            var brand7 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'LG'"));
            if (brand7 == null)
            {
                brand7 = ObjectSpace.CreateObject<Brand>();
                brand7.Name = "LG";
                brand7.Save();
            }

            var brand8 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Microsoft'"));
            if (brand8 == null)
            {
                brand8 = ObjectSpace.CreateObject<Brand>();
                brand8.Name = "Microsoft";
                brand8.Save();
            }

            var brand9 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Philips'"));
            if (brand9 == null)
            {
                brand9 = ObjectSpace.CreateObject<Brand>();
                brand9.Name = "Philips";
                brand9.Save();
            }

            var brand10 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Samsung'"));
            if (brand10 == null)
            {
                brand10 = ObjectSpace.CreateObject<Brand>();
                brand10.Name = "Samsung";
                brand10.Save();
            }

            var brand11 = ObjectSpace.FindObject<Brand>(CriteriaOperator.Parse("Name == 'Weinmann'"));
            if (brand11 == null)
            {
                brand11 = ObjectSpace.CreateObject<Brand>();
                brand11.Name = "Weinmann";
                brand11.Save();
            }

            #endregion

            ObjectSpace.CommitChanges();
        }

        private void CreateStepTypes()
        {
            #region Necessary  Data

            var stepTypeM01 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'M01'"));
            if (stepTypeM01 == null)
            {
                stepTypeM01 = ObjectSpace.CreateObject<StepType>();
                stepTypeM01.Code = "M01";
                stepTypeM01.Description = "OK/KO"; //Ok/not ok
                stepTypeM01.Type = StepTypeGroup.ManualOk;
                stepTypeM01.Save();
            }

            var stepTypeM02 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'M02'"));
            if (stepTypeM02 == null)
            {
                stepTypeM02 = ObjectSpace.CreateObject<StepType>();
                stepTypeM02.Code = "M02";
                stepTypeM02.Description = "OK/KO/NA"; //Ok/not ok/not applicable
                stepTypeM02.Type = StepTypeGroup.ManualOk;
                stepTypeM02.Save();
            }

            var stepTypeM03 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'M03'"));
            if (stepTypeM03 == null)
            {
                stepTypeM03 = ObjectSpace.CreateObject<StepType>();
                stepTypeM03.Code = "M03";
                stepTypeM03.Description = "Testo e OK/KO"; //Ok/not ok, plus a string
                stepTypeM03.Type = StepTypeGroup.ManualOk;
                stepTypeM03.Save();
            }

            var stepTypeM04 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'M04'"));
            if (stepTypeM04 == null)
            {
                stepTypeM04 = ObjectSpace.CreateObject<StepType>();
                stepTypeM04.Code = "M04";
                stepTypeM04.Description = "Testo e OK/KO/NA"; //Ok/not ok/not applicable, plus a string
                stepTypeM04.Type = StepTypeGroup.ManualOk;
                stepTypeM04.Save();
            }

            var stepTypeA01 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'A01'"));
            if (stepTypeA01 == null)
            {
                stepTypeA01 = ObjectSpace.CreateObject<StepType>();
                stepTypeA01.Code = "A01";
                stepTypeA01.Description = "Valore (senza stampa limiti)"; //Ok depends by a value
                stepTypeA01.Type = StepTypeGroup.AutomaticOk;
                stepTypeA01.Save();
            }

            var stepTypeA02 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'A02'"));
            if (stepTypeA02 == null)
            {
                stepTypeA02 = ObjectSpace.CreateObject<StepType>();
                stepTypeA02.Code = "A02";
                stepTypeA02.Description = "Testo e valore (no limiti)";
                stepTypeA02.Type = StepTypeGroup.AutomaticOk;
                stepTypeA02.Save();
            }

            var stepTypeA03 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'A03'"));
            if (stepTypeA03 == null)
            {
                stepTypeA03 = ObjectSpace.CreateObject<StepType>();
                stepTypeA03.Code = "A03";
                stepTypeA03.Description = "Valore (con stampa limiti)"; //Same as A01 plus printing Hi-Lo limits
                stepTypeA03.Type = StepTypeGroup.AutomaticOk;
                stepTypeA03.Save();
            }

            var stepTypeA04 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'A04'"));
            if (stepTypeA04 == null)
            {
                stepTypeA04 = ObjectSpace.CreateObject<StepType>();
                stepTypeA04.Code = "A04";
                stepTypeA04.Description = "Testo e valore (si limiti)"; //Same as A02 plus printing Hi-Lo limits
                stepTypeA04.Type = StepTypeGroup.AutomaticOk;
                stepTypeA04.Save();
            }

            var stepTypeA05 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'A05'"));
            if (stepTypeA05 == null)
            {
                stepTypeA05 = ObjectSpace.CreateObject<StepType>();
                stepTypeA05.Code = "A05";
                stepTypeA05.Description = "Media letture"; //Read from a previous N01 step with values separated by coma.
                stepTypeA05.Type = StepTypeGroup.AutomaticOk;
                stepTypeA05.Save();
            }

            var stepTypeN01 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'N01'"));
            if (stepTypeN01 == null)
            {
                stepTypeN01 = ObjectSpace.CreateObject<StepType>();
                stepTypeN01.Code = "N01";
                stepTypeN01.Description = "Testo breve"; //String one line
                stepTypeN01.Type = StepTypeGroup.NoNeedForOk;
                stepTypeN01.Save();
            }

            var stepTypeN02 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'N02'"));
            if (stepTypeN02 == null)
            {
                stepTypeN02 = ObjectSpace.CreateObject<StepType>();
                stepTypeN02.Code = "N02";
                stepTypeN02.Description = "Testo lungo"; //String multi line
                stepTypeN02.Type = StepTypeGroup.NoNeedForOk;
                stepTypeN02.Save();
            }

            var stepTypeP01 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'P01'"));
            if (stepTypeP01 == null)
            {
                stepTypeP01 = ObjectSpace.CreateObject<StepType>();
                stepTypeP01.Code = "P01";
                stepTypeP01.Description = "Spazio bianco"; //White space
                stepTypeP01.Type = StepTypeGroup.PrintOnly;
                stepTypeP01.Save();
            }

            var stepTypeP02 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'P02'"));
            if (stepTypeP02 == null)
            {
                stepTypeP02 = ObjectSpace.CreateObject<StepType>();
                stepTypeP02.Code = "P02";
                stepTypeP02.Description = "Testo stampa"; //Fixed text for a description
                stepTypeP02.Type = StepTypeGroup.PrintOnly;
                stepTypeP02.Save();
            }

            var stepTypeP03 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'P03'"));
            if (stepTypeP03 == null)
            {
                stepTypeP03 = ObjectSpace.CreateObject<StepType>();
                stepTypeP03.Code = "P03";
                stepTypeP03.Description = "Titolo stampa"; //Fixed text for a section header
                stepTypeP03.Type = StepTypeGroup.PrintOnly;
                stepTypeP03.Save();
            }

            var stepTypeP04 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'P04'"));
            if (stepTypeP04 == null)
            {
                stepTypeP04 = ObjectSpace.CreateObject<StepType>();
                stepTypeP04.Code = "P04";
                stepTypeP04.Description = "Page Break";
                stepTypeP04.Type = StepTypeGroup.PrintOnly;
                stepTypeP04.Save();
            }

            var stepTypeT10 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T10'"));
            if (stepTypeT10 == null)
            {
                stepTypeT10 = ObjectSpace.CreateObject<StepType>();
                stepTypeT10.Code = "T10";
                stepTypeT10.Description = "Tab 1 (12) N02"; //Table header with 1 columns
                stepTypeT10.Type = StepTypeGroup.PrintOnly;
                stepTypeT10.Save();
            }

            var stepTypeT20 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T20'"));
            if (stepTypeT20 == null)
            {
                stepTypeT20 = ObjectSpace.CreateObject<StepType>();
                stepTypeT20.Code = "T20";
                stepTypeT20.Description = "Tab 2 (6-6) N01";
                stepTypeT20.Type = StepTypeGroup.PrintOnly;
                stepTypeT20.Save();
            }

            var stepTypeT21 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T21'"));
            if (stepTypeT21 == null)
            {
                stepTypeT21 = ObjectSpace.CreateObject<StepType>();
                stepTypeT21.Code = "T21";
                stepTypeT21.Description = "Tab 2 (10-2) M01-M02"; //Tab head 2 cols different spacing
                stepTypeT21.Type = StepTypeGroup.PrintOnly;
                stepTypeT21.Save();
            }

            var stepTypeT30 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T30'"));
            if (stepTypeT30 == null)
            {
                stepTypeT30 = ObjectSpace.CreateObject<StepType>();
                stepTypeT30.Code = "T30";
                stepTypeT30.Description = "Tab 3 (4-4-4)"; //Table header with 3 columns
                stepTypeT30.Type = StepTypeGroup.PrintOnly;
                stepTypeT30.Save();
            }

            var stepTypeT31 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T31'"));
            if (stepTypeT31 == null)
            {
                stepTypeT31 = ObjectSpace.CreateObject<StepType>();
                stepTypeT31.Code = "T31";
                stepTypeT31.Description = "Tab 3 (6-4-2) M03-M04"; //Table header with 3 columns
                stepTypeT31.Type = StepTypeGroup.PrintOnly;
                stepTypeT31.Save();
            }

            var stepTypeT32 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T32'"));
            if (stepTypeT32 == null)
            {
                stepTypeT32 = ObjectSpace.CreateObject<StepType>();
                stepTypeT32.Code = "T32";
                stepTypeT32.Description = "Tab 3 (9-1-2) A01"; //Table header with 3 columns
                stepTypeT32.Type = StepTypeGroup.PrintOnly;
                stepTypeT32.Save();
            }

            var stepTypeT40 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T40'"));
            if (stepTypeT40 == null)
            {
                stepTypeT40 = ObjectSpace.CreateObject<StepType>();
                stepTypeT40.Code = "T40";
                stepTypeT40.Description = "Tab 4 (3-3-3-3)"; //Table header with 4 columns
                stepTypeT40.Type = StepTypeGroup.PrintOnly;
                stepTypeT40.Save();
            }

            var stepTypeT41 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T41'"));
            if (stepTypeT41 == null)
            {
                stepTypeT41 = ObjectSpace.CreateObject<StepType>();
                stepTypeT41.Code = "T41";
                stepTypeT41.Description = "Tab 4 (6-3-1-2) A02-A03"; //Table header with 4 columns
                stepTypeT41.Type = StepTypeGroup.PrintOnly;
                stepTypeT41.Save();
            }

            var stepTypeT42 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T42'"));
            if (stepTypeT42 == null)
            {
                stepTypeT42 = ObjectSpace.CreateObject<StepType>();
                stepTypeT42.Code = "T42";
                stepTypeT42.Description = "Tab 4 (3-6-1-2) A05"; //Table header with 4 columns
                stepTypeT42.Type = StepTypeGroup.PrintOnly;
                stepTypeT42.Save();
            }

            var stepTypeT51 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T51'"));
            if (stepTypeT51 == null)
            {
                stepTypeT51 = ObjectSpace.CreateObject<StepType>();
                stepTypeT51.Code = "T51";
                stepTypeT51.Description = "Tab 5 (3-3-3-1-2) A04"; //Table header with 5 columns
                stepTypeT51.Type = StepTypeGroup.PrintOnly;
                stepTypeT51.Save();
            }

            var stepTypeT60 = ObjectSpace.FindObject<StepType>(CriteriaOperator.Parse("Code == 'T60'"));
            if (stepTypeT60 == null)
            {
                stepTypeT60 = ObjectSpace.CreateObject<StepType>();
                stepTypeT60.Code = "T60";
                stepTypeT60.Description = "Tab 6 (2-2-2-2-2-2)"; //Table header with 6 columns
                stepTypeT60.Type = StepTypeGroup.PrintOnly;
                stepTypeT60.Save();
            }

            var settings = ObjectSpace.FindObject<ChangeMyCompany>(null);
            if (settings == null)
            {
                settings = ObjectSpace.CreateObject<ChangeMyCompany>();
                settings.Save();
            }

            #endregion

            ObjectSpace.CommitChanges();

        }

        //Administrators can do everything within the application.
        private PermissionPolicyRole GetSuperAdminRole()
        {
            PermissionPolicyRole superAdminRole =
                ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Super Admin Role"));
            if (superAdminRole == null)
            {
                superAdminRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                superAdminRole.PermissionPolicy = SecurityPermissionPolicy.AllowAllByDefault;
                superAdminRole.Name = "Super Admin Role";
                //Can access everything.
                superAdminRole.IsAdministrative = true;
            }

            return superAdminRole;
        }

        //Users can access and partially edit data (no create and delete capabilities) from their own Company.
        private PermissionPolicyRole GetCompanyEquipmentAdminRole()
        {
            PermissionPolicyRole companyEquipmentAdminRole =
                ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Company Equipment Admin Role"));
            if (companyEquipmentAdminRole == null)
            {
                companyEquipmentAdminRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                companyEquipmentAdminRole.Name = "Company Equipment Admin Role";

                companyEquipmentAdminRole.PermissionPolicy = SecurityPermissionPolicy.DenyAllByDefault;

                companyEquipmentAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Interventions/Items/Visit_ListView_Done", SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Interventions/Items/Intervention_ListView", SecurityPermissionState.Allow);

                companyEquipmentAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment list/Items/Equipment_ListView", SecurityPermissionState.Allow);

                companyEquipmentAdminRole.AddNavigationPermission("Application/NavigationItems/Items/User settings/Items/AmbulUser_ListView", SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddNavigationPermission("Application/NavigationItems/Items/User settings/Items/ChangeMyCompanyId", SecurityPermissionState.Allow);
                
                companyEquipmentAdminRole.AddTypePermission<Equipment>(SecurityOperations.Read, SecurityPermissionState.Deny);
                companyEquipmentAdminRole.AddObjectPermission<Equipment>(SecurityOperations.ReadWriteAccess, "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddMemberPermission<Equipment>(SecurityOperations.Read, "Company", "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddMemberPermission<Equipment>(SecurityOperations.Write, "Company", "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Deny);

                companyEquipmentAdminRole.AddTypePermission<Visit>(SecurityOperations.Read, SecurityPermissionState.Deny);
                companyEquipmentAdminRole.AddObjectPermission<Visit>(SecurityOperations.ReadWriteAccess, "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddMemberPermission<Visit>(SecurityOperations.Read, "Company", "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddMemberPermission<Visit>(SecurityOperations.Write, "Company", "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Deny);

                companyEquipmentAdminRole.AddTypePermission<AmbulUser>(SecurityOperations.Read, SecurityPermissionState.Deny);
                companyEquipmentAdminRole.AddObjectPermission<AmbulUser>(SecurityOperations.Read, "Oid=CurrentUserId()", SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddMemberPermission<AmbulUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", null, SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddMemberPermission<AmbulUser>(SecurityOperations.Write, "StoredPassword", null, SecurityPermissionState.Allow);

                companyEquipmentAdminRole.AddTypePermission<Company>(SecurityOperations.Read, SecurityPermissionState.Deny);
                companyEquipmentAdminRole.AddObjectPermission<Company>(SecurityOperations.ReadWriteAccess, "Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);

                companyEquipmentAdminRole.AddTypePermission<ChangeMyCompany>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);

                companyEquipmentAdminRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
                companyEquipmentAdminRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            }

            return companyEquipmentAdminRole;
        }

        //Users can access and fully edit (including create and delete capabilities) data from their own Company. However, they cannot access data from other Companys.
        private PermissionPolicyRole GetCompanyUserAdminRole()
        {
            PermissionPolicyRole companyUserAdminRole =
                ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Company User Admin Role"));
            if (companyUserAdminRole == null)
            {
                companyUserAdminRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                companyUserAdminRole.Name = "Company User Admin Role";

                companyUserAdminRole.PermissionPolicy = SecurityPermissionPolicy.DenyAllByDefault;

                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Interventions/Items/ScheduledVisits", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Interventions/Items/Visit_ListView_Done", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Interventions/Items/Intervention_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Interventions/Items/Check_ListView", SecurityPermissionState.Allow);

                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment list/Items/Equipment_ListView", SecurityPermissionState.Allow);

                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment settings/Items/Family_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment settings/Items/MaintenancePlan_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment settings/Items/Brand_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment settings/Items/Model_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment settings/Items/AccessoryType_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment settings/Items/AccessoryType_ListView", SecurityPermissionState.Allow);

                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/User settings/Items/AmbulUser_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/User settings/Items/PermissionPolicyRole_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/User settings/Items/Company_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/User settings/Items/ChangeMyCompanyId", SecurityPermissionState.Allow);

                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Other settings/Items/Part_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Other settings/Items/SparePartCategory_ListView", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Other settings/Items/Equipment_ListView_TypeA", SecurityPermissionState.Allow);
                companyUserAdminRole.AddNavigationPermission("Application/NavigationItems/Items/Other settings/Items/Parameter_ListView", SecurityPermissionState.Allow);

                companyUserAdminRole.AddTypePermission<Equipment>(SecurityOperations.Read, SecurityPermissionState.Deny);
                companyUserAdminRole.AddObjectPermission<Equipment>(SecurityOperations.ReadWriteAccess, "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyUserAdminRole.AddMemberPermission<Equipment>(SecurityOperations.Read, "Company", "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyUserAdminRole.AddMemberPermission<Equipment>(SecurityOperations.Write, "Company", "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Deny);

                companyUserAdminRole.AddTypePermission<Visit>(SecurityOperations.Read, SecurityPermissionState.Deny);
                companyUserAdminRole.AddObjectPermission<Visit>(SecurityOperations.ReadWriteAccess, "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyUserAdminRole.AddMemberPermission<Visit>(SecurityOperations.Read, "Company", "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyUserAdminRole.AddMemberPermission<Visit>(SecurityOperations.Write, "Company", "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Deny);

                companyUserAdminRole.AddTypePermission<AmbulUser>(SecurityOperations.Read, SecurityPermissionState.Deny);
                //                companyUserAdminRole.AddObjectPermission<AmbulUser>(SecurityOperations.ReadOnlyAccess, "Company.Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);
                companyUserAdminRole.AddObjectPermission<AmbulUser>(SecurityOperations.ReadOnlyAccess, "[Companies][Users[Oid = CurrentUserId()]]", SecurityPermissionState.Allow);
                companyUserAdminRole.AddObjectPermission<AmbulUser>(SecurityOperations.Write, "Oid=CurrentUserId()", SecurityPermissionState.Allow);
                companyUserAdminRole.AddMemberPermission<AmbulUser>(SecurityOperations.Write, "ChangePasswordOnFirstLogon", null, SecurityPermissionState.Allow);
                companyUserAdminRole.AddMemberPermission<AmbulUser>(SecurityOperations.Write, "StoredPassword", null, SecurityPermissionState.Allow);

                companyUserAdminRole.AddTypePermission<Company>(SecurityOperations.Read, SecurityPermissionState.Deny);
                companyUserAdminRole.AddObjectPermission<Company>(SecurityOperations.ReadWriteAccess, "Users[Oid = CurrentUserId()]", SecurityPermissionState.Allow);

                companyUserAdminRole.AddTypePermission<ChangeMyCompany>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);

                companyUserAdminRole.AddTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
                companyUserAdminRole.AddTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecurityPermissionState.Allow);
            }

            return companyUserAdminRole;
        }
        
        //Users can access and fully edit (including create and delete capabilities) data from their own Company. However, they cannot access data from other Companys.
        private PermissionPolicyRole CreateTechnicianRole()
        {
            PermissionPolicyRole technicianRole =
                ObjectSpace.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Technician Role"));
            if (technicianRole == null)
            {
                technicianRole = ObjectSpace.CreateObject<PermissionPolicyRole>();
                technicianRole.Name = "Technician Role";

                technicianRole.PermissionPolicy = SecurityPermissionPolicy.AllowAllByDefault;

                technicianRole.AddNavigationPermission("Application/NavigationItems/Items/User settings/Items/AmbulUser_ListView", SecurityPermissionState.Allow);
                technicianRole.AddNavigationPermission("Application/NavigationItems/Items/Interventions/Items/ScheduledVisits", SecurityPermissionState.Allow);
                technicianRole.AddNavigationPermission("Application/NavigationItems/Items/Interventions/Items/Visit_ListView_Done", SecurityPermissionState.Allow);
                technicianRole.AddNavigationPermission("Application/NavigationItems/Items/Equipment list", SecurityPermissionState.Allow);

                technicianRole.AddTypePermission<AmbulUser>(SecurityOperations.ReadOnlyAccess, SecurityPermissionState.Allow);

                technicianRole.AddMemberPermission<AmbulUser>(SecurityOperations.Write, "EmailAddress; FirstName; LastName; Location; Signature; TechnicianSignature; TelephoneNumber; ReadedManuals; Companies; Company", null, SecurityPermissionState.Allow);
            }

            return technicianRole;
        }
    }
}
