-- View: public."AllEquipmentsAndPlans"

-- DROP VIEW public."AllEquipmentsAndPlans";

CREATE OR REPLACE VIEW public."AllEquipmentsAndPlans" AS
 SELECT "Equipment"."Oid" AS "EquipmentOid",
    "Equipment"."PurchaseInvoiceDate",
    "MaintenancePlan"."Oid" AS "MaintenancePlanOid",
    "MaintenancePlan"."DaysRecurrency",
    "MaintenancePlan"."EstimatedEffort",
    "Family"."Type" AS "FamilyType"
   FROM "Family"
     JOIN ("Equipment"
     JOIN "MaintenancePlan" ON "Equipment"."Family" = "MaintenancePlan"."Family") ON "Family"."Oid" = "MaintenancePlan"."Family"
  WHERE "Family"."Type"::text <> 'A'::text AND "Equipment"."Dismissed" = false;

ALTER TABLE public."AllEquipmentsAndPlans"
    OWNER TO postgres;


-- View: public."BaseForScheduledVisitsScreen"

-- DROP VIEW public."BaseForScheduledVisitsScreen";

CREATE OR REPLACE VIEW public."BaseForScheduledVisitsScreen" AS
 SELECT "Company"."Title" AS "CompanyTitle",
    "Company"."Oid" AS "CompanyOid",
    "Location"."City" AS "LocationCity",
    "Location"."Oid" AS "LocationOid",
        CASE
            WHEN "GroupByLocationAndDueDate"."DueDate" IS NULL THEN '1900-01-01 00:00:00'::timestamp without time zone
            ELSE "GroupByLocationAndDueDate"."DueDate"
        END AS "DueDate",
    "GroupByLocationAndDueDate"."VehiclesCount",
    "GroupByLocationAndDueDate"."EquipmentsCount",
    "GroupByLocationAndDueDate"."TotalEstimatedEffort",
    "VisitAgreedDate"."AgreedDate"
   FROM "Company"
     JOIN ("GroupByLocationAndDueDate"
     RIGHT JOIN "Location" ON "GroupByLocationAndDueDate"."EquipmentLocationOid" = "Location"."Oid") ON "Company"."Oid" = "Location"."Company"
     LEFT JOIN "VisitAgreedDate" ON "Location"."Oid" = "VisitAgreedDate"."Location";

ALTER TABLE public."BaseForScheduledVisitsScreen"
    OWNER TO postgres;

-- View: public."DueDateCalculation"

-- DROP VIEW public."DueDateCalculation";

CREATE OR REPLACE VIEW public."DueDateCalculation" AS
 SELECT "AllEquipmentsAndPlans"."EquipmentOid",
    "AllEquipmentsAndPlans"."MaintenancePlanOid",
        CASE
            WHEN "LastExecution"."LastExecution" IS NULL THEN "AllEquipmentsAndPlans"."PurchaseInvoiceDate" + 365::double precision * '1 day'::interval
            ELSE "LastExecution"."LastExecution" + "AllEquipmentsAndPlans"."DaysRecurrency"::double precision * '1 day'::interval
        END AS duedate,
    "AllEquipmentsAndPlans"."EstimatedEffort",
    "AllEquipmentsAndPlans"."FamilyType"
   FROM "AllEquipmentsAndPlans"
     LEFT JOIN "LastExecution" ON "AllEquipmentsAndPlans"."MaintenancePlanOid" = "LastExecution"."InterventionMaintenancePlan" AND "AllEquipmentsAndPlans"."EquipmentOid" = "LastExecution"."InterventionEquipment";

ALTER TABLE public."DueDateCalculation"
    OWNER TO postgres;

-- View: public."EquipmentInLocation"

-- DROP VIEW public."EquipmentInLocation";

CREATE OR REPLACE VIEW public."EquipmentInLocation" AS
 SELECT e."Location" AS "LocationOid",
    e."Oid" AS "EquipmentOid",
    p."Oid" AS "MaintenancePlanOid",
    f."Name" AS "Family",
    b."Name" AS "Brand",
    m."Name" AS "Model",
    e."QrCode",
    e."Plate",
    e."SerialNumber",
    p."OptionalDescription" AS "Plan",
    d.duedate AS "DueDate",
    d."EstimatedEffort"
   FROM "DueDateCalculation" d
     JOIN "MaintenancePlan" p ON d."MaintenancePlanOid" = p."Oid"
     JOIN "Equipment" e ON d."EquipmentOid" = e."Oid"
     JOIN "Family" f ON e."Family" = f."Oid"
     JOIN "Brand" b ON e."Brand" = b."Oid"
     JOIN "Model" m ON e."Model" = m."Oid";

ALTER TABLE public."EquipmentInLocation"
    OWNER TO postgres;

-- View: public."GroupByLocationAndDueDate"

-- DROP VIEW public."GroupByLocationAndDueDate";

CREATE OR REPLACE VIEW public."GroupByLocationAndDueDate" AS
 SELECT "Equipment"."Location" AS "EquipmentLocationOid",
    "DueDateCalculation".duedate AS "DueDate",
    sum("DueDateCalculation"."EstimatedEffort") AS "TotalEstimatedEffort",
    sum(
        CASE
            WHEN "DueDateCalculation"."FamilyType"::text = 'V'::text THEN 1
            ELSE 0
        END) AS "VehiclesCount",
    sum(
        CASE
            WHEN "DueDateCalculation"."FamilyType"::text = 'E'::text THEN 1
            ELSE 0
        END) AS "EquipmentsCount"
   FROM "DueDateCalculation"
     JOIN "Equipment" ON "DueDateCalculation"."EquipmentOid" = "Equipment"."Oid"
  GROUP BY "Equipment"."Location", "DueDateCalculation".duedate;

ALTER TABLE public."GroupByLocationAndDueDate"
    OWNER TO postgres;

-- View: public."LastExecution"

-- DROP VIEW public."LastExecution";

CREATE OR REPLACE VIEW public."LastExecution" AS
 SELECT "Intervention"."Equipment" AS "InterventionEquipment",
    "Intervention"."MaintenancePlan" AS "InterventionMaintenancePlan",
    max("Intervention"."ExecutionDate") AS "LastExecution"
   FROM "Intervention"
  GROUP BY "Intervention"."Equipment", "Intervention"."MaintenancePlan";

ALTER TABLE public."LastExecution"
    OWNER TO postgres;

