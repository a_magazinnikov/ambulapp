﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace AmbulApp.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InterventionAutoCommitMaster : ObjectViewController<DetailView, Intervention>
    {
        private INestedObjectSpace nestedObjectSpace;

        protected override void OnActivated()
        {
            base.OnActivated();
            nestedObjectSpace = ObjectSpace as INestedObjectSpace;
            if (nestedObjectSpace != null)
            {
                nestedObjectSpace.Committed += nestedObjectSpace_Committed;
            }
        }

        private void nestedObjectSpace_Committed(object sender, EventArgs e)
        {
            nestedObjectSpace.ParentObjectSpace.CommitChanges();
            nestedObjectSpace.Refresh();
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            if (nestedObjectSpace != null)
            {
                nestedObjectSpace.Committed -= nestedObjectSpace_Committed;
                nestedObjectSpace = null;
            }
        }
    }
}