﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace AmbulApp.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InterventionViewController : ViewController
    {
        public InterventionViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void saCreateCheckList_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (e.CurrentObject is Intervention intervention)
            {
                if (intervention.MaintenancePlan != null && intervention.MaintenancePlan.MaintenanceSteps.Any())
                {
                    foreach (var maintenanceStep in intervention.MaintenancePlan.MaintenanceSteps)
                    {
                        var check = ObjectSpace.CreateObject<Check>();

                        check.Block = maintenanceStep.Block;
                        check.StepType = maintenanceStep.StepType;
                        check.Column1 = maintenanceStep.Column1;
                        check.Column2 = maintenanceStep.Column2;
                        check.Column3 = maintenanceStep.Column3;
                        check.Column4 = maintenanceStep.Column4;
                        check.Column5 = maintenanceStep.Column5;
                        check.Column6 = maintenanceStep.Column6;
                        check.HiTolerance = maintenanceStep.HiTolerance;
                        check.HideIfSequence = maintenanceStep.HideIfSequence;
                        check.HiPercentage = maintenanceStep.HiPercentage;
                        check.HideIfValue = maintenanceStep.HideIfValue;
                        check.InternalText = maintenanceStep.InternalText;
                        check.LimitPrintStyle = maintenanceStep.LimitPrintStyle;
                        check.LoPercentage = maintenanceStep.LoPercentage;
                        check.LoTolerance = maintenanceStep.LoTolerance;
                        check.OkPrintStyle = maintenanceStep.OkPrintStyle;
                        check.PrintSequence = maintenanceStep.PrintSequence;
                        check.Sequence = maintenanceStep.Sequence;
                        check.TargetValue = maintenanceStep.TargetValue;
                        check.TargetSequence = maintenanceStep.TargetSequence;
                        check.ValueInputs = maintenanceStep.ValueInputs;
                        check.TargetType = maintenanceStep.TargetType;
                        check.VarianceSeq = maintenanceStep.VarianceSeq;
                        check.VarianceType = maintenanceStep.VarianceType;
                        check.StepDescription = maintenanceStep.StepDescription;
                        check.VarianceType = maintenanceStep.VarianceType;

                        intervention.Checks.Add(check);
                    }

                    ObjectSpace.CommitChanges();

                    MessageOptions options = new MessageOptions
                    {
                        Duration = 2000,
                        Message = "Check list have been successfully created!",
                        Type = InformationType.Success,
                        Web = { Position = InformationPosition.Top },
                        Win =
                        {
                            Caption = "Success",
                            Type = WinMessageType.Flyout
                        }
                    };

                    Application.ShowViewStrategy.ShowMessage(options);
                }
            }
        }
    }
}
