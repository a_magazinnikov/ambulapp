﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;

namespace AmbulApp.Module.Controllers
{
    public class ScheduledVisitViewController : ObjectViewController<DetailView, ScheduledVisit>
    {
        ScheduledVisit _scheduledVisitsCache;

        protected override void OnActivated()
        {
            base.OnActivated();
            View.ObjectSpace.CustomCommitChanges += ObjectSpace_CustomCommitChanges;
            View.ObjectSpace.Committing += ObjectSpace_Committing;
            View.ObjectSpace.Committed += ObjectSpace_Committed;
            View.ObjectSpace.Reloaded += ObjectSpace_Reloaded;
        }

        protected override void OnDeactivated()
        {
            View.ObjectSpace.CustomCommitChanges -= ObjectSpace_CustomCommitChanges;
            View.ObjectSpace.Committing -= ObjectSpace_Committing;
            View.ObjectSpace.Committed -= ObjectSpace_Committed;
            View.ObjectSpace.Reloaded -= ObjectSpace_Reloaded;
            base.OnDeactivated();
        }

        void ObjectSpace_Reloaded(object sender, EventArgs e)
        {
//            _scheduledVisitsCache.Clear();
        }

        void ObjectSpace_Committed(object sender, EventArgs e)
        {
//            _scheduledVisitsCache.Clear();
        }

        void ObjectSpace_Committing(object sender, CancelEventArgs e)
        {
            IObjectSpace os = (IObjectSpace)sender;
            var count = os.ModifiedObjects.Count;
            if (count > 0)
            {
                _scheduledVisitsCache = os.ModifiedObjects[0] as ScheduledVisit;
                os.RemoveFromModifiedObjects(os.ModifiedObjects[0]);
            }
        }

        void ObjectSpace_CustomCommitChanges(object sender, HandledEventArgs e)
        {
            IObjectSpace os = (IObjectSpace) sender;
            var visit = ObjectSpace.GetObjectByKey<VisitAgreedDate>(_scheduledVisitsCache.Key.LocationOid);
            if (visit == null)
            {
                visit = os.CreateObject<VisitAgreedDate>();
                visit.Location = _scheduledVisitsCache.Key.LocationOid;
            }

            visit.AgreedDate = _scheduledVisitsCache.AgreedDate;
            visit.Save();

        }
    }
}
