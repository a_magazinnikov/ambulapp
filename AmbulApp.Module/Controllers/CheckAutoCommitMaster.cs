﻿using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using System;
using System.Linq;

namespace AmbulApp.Module.Controllers
{
    public class CheckAutoCommitMaster : ObjectViewController<ListView, Check>
    {
        private INestedObjectSpace nestedObjectSpace;

        protected override void OnActivated()
        {
            base.OnActivated();
            nestedObjectSpace = ObjectSpace as INestedObjectSpace;
            if (nestedObjectSpace != null)
            {
                nestedObjectSpace.Committed += nestedObjectSpace_Committed;
            }
        }

        private void nestedObjectSpace_Committed(object sender, EventArgs e)
        {
            nestedObjectSpace.ParentObjectSpace.CommitChanges();
            nestedObjectSpace.Refresh();
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            if (nestedObjectSpace != null)
            {
                nestedObjectSpace.Committed -= nestedObjectSpace_Committed;
                nestedObjectSpace = null;
            }
        }
    }
}