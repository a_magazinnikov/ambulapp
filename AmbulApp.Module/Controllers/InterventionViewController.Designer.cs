﻿namespace AmbulApp.Module.Controllers
{
    partial class InterventionViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.saCreateCheckList = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // saCreateCheckList
            // 
            this.saCreateCheckList.Caption = "Create Check List";
            this.saCreateCheckList.ConfirmationMessage = "This action will create a new Check list. Are you sure?";
            this.saCreateCheckList.Id = "CreateCheckListId";
            this.saCreateCheckList.TargetObjectsCriteria = "MaintenancePlan is not NULL";
            this.saCreateCheckList.TargetObjectType = typeof(AmbulApp.Module.BusinessObjects.Intervention);
            this.saCreateCheckList.ToolTip = null;
            this.saCreateCheckList.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.saCreateCheckList_Execute);
            // 
            // InterventionViewController
            // 
            this.Actions.Add(this.saCreateCheckList);
            this.TargetObjectType = typeof(AmbulApp.Module.BusinessObjects.Intervention);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction saCreateCheckList;
    }
}
