﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.SystemModule;

namespace AmbulApp.Module.Controllers
{
    public class FilterControllerCaseInsensitive : ViewController<ListView>
    {
        public FilterControllerCaseInsensitive()
        {
        }

        FilterController standardFilterController;

        protected override void OnActivated()
        {
            standardFilterController = Frame.GetController<FilterController>();
            if (standardFilterController == null)
                return;

            standardFilterController.FullTextFilterAction.Execute += FullTextFilterAction_Execute;
        }

        private void FullTextFilterAction_Execute(object sender, ParametrizedActionExecuteEventArgs e)
        {
            //we locate filter with the key FilterController.FullTextSearchCriteriaName then we convert it to case insensitive
            if (!string.IsNullOrEmpty(e.ParameterCurrentValue as string) &&
                View.CollectionSource.Criteria.ContainsKey(FilterController.FullTextSearchCriteriaName))
                View.CollectionSource.Criteria[FilterController.FullTextSearchCriteriaName] =
                    GetCaseInsensitiveCriteria(e.ParameterCurrentValue);
        }

        private CriteriaOperator GetCaseInsensitiveCriteria(object searchValue)
        {
            //we get a list of all the properties that can be involved in the filter
            var searchProperties = standardFilterController.GetFullTextSearchProperties();
            //we declare a model class and a property name,the values on this variables will change if we property involve is a navigation property (nother persistent object)
            IModelClass modelClass = null;
            string propertyName = string.Empty;

            //we declare a list of operators to contains new operators we are going to create
            List<CriteriaOperator> Operator = new List<CriteriaOperator>();
            //we iterate all the properties
            foreach (var currentProperty in searchProperties)
            {
                //here we split the name with a dot, if lengt is greater than 1 it means its a navigation propertie, beware that this may fail with a deep tree of properties like category.subcategory.categoryname
                var split = currentProperty.Split('.');
                if (split.Length > 1)
                {
                    Debug.WriteLine($"{"its a complex property"}");
                    var currentClass = this.View.Model.ModelClass;
                    for (int i = 0; i < split.Length; i++)
                    {
                        //if its a navigation property we locate the type in the BOModel
                        //IModelMember member = CurrentClass.OwnMembers.Where(m => m.Name == Split[i]).FirstOrDefault();
                        //var member = CurrentClass.AllMembers.Where(m => m.Name == Split[i]).FirstOrDefault();
                        if (currentClass != null)
                        {
                            IModelMember member = currentClass.AllMembers.FirstOrDefault(m => m.Name == split[i]);

                            //then we set the model class and property name to the values of the navigation property like category.name where category is the model class and name is the property
                            currentClass = this.Application.Model.BOModel.GetClass(member?.Type);
                        }

                        if (currentClass == null || i + 1 == split.Length)
                            continue;

                        modelClass = currentClass;
                        propertyName = split[i + 1];
                    }

                    Debug.WriteLine($"{"ModelClass"}:{modelClass?.Name}");
                    Debug.WriteLine($"{"PropertyName"}:{propertyName}");

                    //OLD VERSION WITHOUT RECURSIVE COMPLEX PROPERTIES
                    ////if its a navigation property we locate the type in the BOModel
                    //IModelMember member = this.View.Model.ModelClass.OwnMembers.Where(m => m.Name == Split[0]).FirstOrDefault();
                    ////then we set the model class and property name to the values of the navigation property like category.name where category is the model class and name is the property
                    //ModelClass = this.Application.Model.BOModel.GetClass(member.Type);
                    //PropertyName = Split[1];
                }
                else
                {
                    //else the model class will be the current class where the filter is executing, and the property will be the current property we are evaluating
                    modelClass = this.View.Model.ModelClass;
                    propertyName = currentProperty;
                }

                //we look for the property on the class model own member
                var property = modelClass?.OwnMembers.FirstOrDefault(m => m.Name == propertyName);
                if (property != null)
                {
                    //if the property is a string it means that we can set it to upper case
                    if (property.Type == typeof(string))
                    {
                        searchValue = searchValue.ToString().ToUpper();
                        //we create an operator where we set the value of the property to upper before we compare it, also we change the comparison value to upper
                        CriteriaOperator operand = CriteriaOperator.Parse("Contains(Upper(" + currentProperty + "), ?)",
                            searchValue);
                        //we added to the list of operators that will will concatenate with OR
                        Operator.Add(operand);
                    }
                    else
                    {
                        //if the property is not a string we need to try to cast the value to the correct type so we do a catch try, if we manage to cast the value it will be added to the operators list
                        try
                        {
                            var convertedType = Convert.ChangeType(searchValue, property.Type);
                            CriteriaOperator operand = new BinaryOperator(currentProperty, convertedType,
                                BinaryOperatorType.Equal);
                            Operator.Add(operand);
                        }
                        catch (Exception)
                        {
                            //silent exception, this will happend if the casting was not succesfull so we won't add the operand on this case
                        }
                    }
                }
            }

            //we concatenate everything with an OR 
            var operators = CriteriaOperator.Or(Operator.ToArray());
            Debug.WriteLine($"{"operators"}:{operators}");
            return operators;
        }
    }
}