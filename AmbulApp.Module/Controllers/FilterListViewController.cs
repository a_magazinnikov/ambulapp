﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
//using AmbulApp.Module.CustomAuthentication;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;

namespace AmbulApp.Module.Controllers
{
    public class FilterListViewController : ViewController
    {
        public FilterListViewController()
        {
            TargetViewId =
                "Company_ListView;Equipment_ListView;AmbulUser_ListView;Visit_ListView_Scheduled;Visit_ListView_Done";
        }

        protected override void OnActivated()
        {
            base.OnActivated();

            if (SecuritySystem.CurrentUser is AmbulUser user && !(user.SuperAdmin || user.Technician))
            {
                var settings = ObjectSpace.FindObject<ChangeMyCompany>(null);
                var company = settings?.Company;
                if (company != null && View is ListView listView)
                {
                    if (listView.Id == "Company_ListView")
                    {
                        listView.CollectionSource.Criteria["Filter1"] =
                            CriteriaOperator.Parse("Oid = ?", company.Oid);
                    }

                    if (listView.Id == "Equipment_ListView")
                    {
                        listView.CollectionSource.Criteria["Filter2"] =
                            CriteriaOperator.Parse("Company = ?", company.Oid);
                    }

                    if (listView.Id == "Visit_ListView_Scheduled" || listView.Id == "Visit_ListView_Done")
                    {
                        listView.CollectionSource.Criteria["Filter3"] =
                            CriteriaOperator.Parse("Company = ?", company.Oid);
                    }

                    if (listView.Id == "AmbulUser_ListView")
                    {
                        listView.CollectionSource.Criteria["Filter4"] =
                            CriteriaOperator.Parse("[Companies][Oid = ?]", company.Oid);
                    }
                }
            }
        }
    }
}
