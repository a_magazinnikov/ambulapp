﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.SystemModule;

namespace AmbulApp.Module.Controllers
{
    public class DeactivateNewActionInLookupsController : ViewController<ListView>
    {
        protected override void OnActivated()
        {
            base.OnActivated();

            if (View.Id == "Family_LookupListView" || View.Id == "Brand_LookupListView")
            {
                if (Frame.Context == TemplateContext.LookupControl || Frame.Context == TemplateContext.LookupWindow)
                {
                    NewObjectViewController controller = Frame.GetController<NewObjectViewController>();
                    controller?.NewObjectAction.Active.SetItemValue("LookupListView", false);
                }
            }
        }
    }
}
