﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;

namespace AmbulApp.Module.Controllers
{
    public class AmbulUserDetailViewController : ObjectViewController<DetailView, AmbulUser>
    {
        public AmbulUserDetailViewController()
        {
            var sendEmailAction =
                new SimpleAction(this, "Send Invitation Email", DevExpress.Persistent.Base.PredefinedCategory.Edit)
                {
                    ConfirmationMessage = "This will send an invitation email. Are you sure?",
                    TargetViewType = ViewType.DetailView
                };

            sendEmailAction.Execute += SendEmailActionOnExecute;
        }

        private void SendEmailActionOnExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (e.CurrentObject is AmbulUser ambulUser && EmailHelper.Instance.SendRegistrationEmail(ambulUser.UserName, ambulUser.EmailAddress))
            {
                MessageOptions options = new MessageOptions
                {
                    Duration = 2000,
                    Message = "Email message have been successfully sent!",
                    Type = InformationType.Success,
                    Web = {Position = InformationPosition.Top},
                    Win =
                    {
                        Caption = "Success",
                        Type = WinMessageType.Flyout
                    }
                };

                Application.ShowViewStrategy.ShowMessage(options);
            }
        }
    }
}
