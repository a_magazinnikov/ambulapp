﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.SystemModule;

namespace AmbulApp.Module.Controllers
{
    public class MaintenanceStepDetailViewController: ViewController
    {
        private NewObjectViewController _controller;

        public MaintenanceStepDetailViewController()
        {
            TargetObjectType = typeof(MaintenanceStep);
        }

        protected override void OnActivated()
        {
            base.OnActivated();

            _controller = Frame.GetController<NewObjectViewController>();
            if (_controller != null)
            {
                _controller.ObjectCreated += controller_ObjectCreated;
            }
        }

        void controller_ObjectCreated(object sender, ObjectCreatedEventArgs e)
        {
            if (e.CreatedObject is MaintenanceStep currentStep)
            {
                var currentPlan = currentStep.Plan;
                if (currentPlan != null)
                {
                    currentStep.Sequence = currentPlan.GetNextLineSeqNo();
                    currentStep.PrintSequence = currentStep.Sequence;
                }
            }
        }

        protected override void OnDeactivated()
        {
            if (_controller != null)
            {
                _controller.ObjectCreated -= controller_ObjectCreated;
            }
            base.OnDeactivated();
        }
    }
}
