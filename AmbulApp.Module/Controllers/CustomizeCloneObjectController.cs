﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.CloneObject;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace AmbulApp.Module.Controllers
{
    public class CustomizeCloneObjectController : ObjectViewController
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            TargetObjectType = typeof(MaintenancePlan);
            var cloneObjectController = Frame.GetController<CloneObjectViewController>();
            if (cloneObjectController != null)
            {
                cloneObjectController.CustomCloneObject += cloneObjectController_CustomCloneObject;
            }
        }

        void cloneObjectController_CustomCloneObject(object sender, CustomCloneObjectEventArgs e)
        {
            var cloner = new MyCloner();
            e.TargetObjectSpace = e.CreateDefaultTargetObjectSpace();
            object objectFromTargetObjectSpace = e.TargetObjectSpace.GetObject(e.SourceObject);
            e.ClonedObject = cloner.CloneTo(objectFromTargetObjectSpace, e.TargetType);
        }
    }

    public class MyCloner : Cloner
    {
        public override void CopyMemberValue(XPMemberInfo memberInfo, IXPSimpleObject sourceObject, IXPSimpleObject targetObject)
        {
            if (memberInfo.IsAssociation || !memberInfo.IsAssociation)
            {
                base.CopyMemberValue(memberInfo, sourceObject, targetObject);
            }
        }

        public override void CopyCollection(XPMemberInfo memberInfo, IXPSimpleObject sourceObject, IXPSimpleObject targetObject,
            bool aggregated)
        {
            if (memberInfo.IsAssociation)
            {
                base.CopyCollection(memberInfo, sourceObject, targetObject, aggregated);
            }
        }
    }
}
