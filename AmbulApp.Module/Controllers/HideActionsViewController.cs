﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;

namespace AmbulApp.Module.Controllers
{
    public class HideActionsViewController : ViewController<DetailView>
    {
        protected override void OnActivated()
        {
            base.OnActivated();

            var modificationsController = Frame.GetController<DevExpress.ExpressApp.SystemModule.ModificationsController>();
            if (modificationsController != null)
            {
                if (!View.Id.Equals("MaintenanceStep_DetailView"))
                {
                    modificationsController.SaveAndCloseAction.Active.SetItemValue("ReasonHideSaveAndCloseAction", false);
                    modificationsController.SaveAndNewAction.Active.SetItemValue("ReasonHideSaveAndNewAction", false);
                    
                }
                else
                {
                    modificationsController.SaveAndCloseAction.Active.SetItemValue("ReasonHideSaveAndCloseAction", true);
                    modificationsController.SaveAndNewAction.Active.SetItemValue("ReasonHideSaveAndNewAction", true);
                }
            }
        }
    }
}
