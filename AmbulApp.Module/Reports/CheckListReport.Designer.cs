﻿namespace AmbulApp.Module.Reports
{
    partial class CheckListReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckListReport));
            DevExpress.XtraPrinting.BarCode.QRCodeGenerator qrCodeGenerator1 = new DevExpress.XtraPrinting.BarCode.QRCodeGenerator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPageBreak1 = new DevExpress.XtraReports.UI.XRPageBreak();
            this.M01Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.m01SD = new DevExpress.XtraReports.UI.XRTableCell();
            this.m01Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.M02Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.m02SD = new DevExpress.XtraReports.UI.XRTableCell();
            this.m02Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.M03Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.m03SD = new DevExpress.XtraReports.UI.XRTableCell();
            this.m03RT = new DevExpress.XtraReports.UI.XRTableCell();
            this.m03Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.M04Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.m04SD = new DevExpress.XtraReports.UI.XRTableCell();
            this.m04RT = new DevExpress.XtraReports.UI.XRTableCell();
            this.m04Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.A01Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.a01SD = new DevExpress.XtraReports.UI.XRTableCell();
            this.a01ReadV = new DevExpress.XtraReports.UI.XRTableCell();
            this.a01Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.A02Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.a02Sd = new DevExpress.XtraReports.UI.XRTableCell();
            this.a02RT = new DevExpress.XtraReports.UI.XRTableCell();
            this.a02ReadV = new DevExpress.XtraReports.UI.XRTableCell();
            this.a02Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.A03Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.a03SD = new DevExpress.XtraReports.UI.XRTableCell();
            this.a03Lim = new DevExpress.XtraReports.UI.XRTableCell();
            this.a03ReadV = new DevExpress.XtraReports.UI.XRTableCell();
            this.a03Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.A04Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.a04Sd = new DevExpress.XtraReports.UI.XRTableCell();
            this.a04RT = new DevExpress.XtraReports.UI.XRTableCell();
            this.a04Lim = new DevExpress.XtraReports.UI.XRTableCell();
            this.a04ReadV = new DevExpress.XtraReports.UI.XRTableCell();
            this.a04Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.A05Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.a05SD = new DevExpress.XtraReports.UI.XRTableCell();
            this.a05ValueInp = new DevExpress.XtraReports.UI.XRTableCell();
            this.a05ReadV = new DevExpress.XtraReports.UI.XRTableCell();
            this.a05Ok = new DevExpress.XtraReports.UI.XRTableCell();
            this.N01Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.n01SD = new DevExpress.XtraReports.UI.XRTableCell();
            this.n01RT = new DevExpress.XtraReports.UI.XRTableCell();
            this.N02Table = new DevExpress.XtraReports.UI.XRTable();
            this.n02SD = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.P01Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.p01cell = new DevExpress.XtraReports.UI.XRTableCell();
            this.P02Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.p02cell = new DevExpress.XtraReports.UI.XRTableCell();
            this.P03Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.p03cell = new DevExpress.XtraReports.UI.XRTableCell();
            this.T10Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.t10cell = new DevExpress.XtraReports.UI.XRTableCell();
            this.T20Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.t20c1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.t20c2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T21Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.t21c1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.t21c2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T30Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.t30c1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.t30c2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.t30c3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T31Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.t31c1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.t31c2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.t31c3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T32Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.t32c1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.t32c2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.t32c3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T40Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.T40C1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T40C2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T40C3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T40C4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T41Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.T41C1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T41C2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T41C3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T41C4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T42Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.T42C1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T42C2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T42C3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T42C4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T51Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.T5C1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T5C2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T5C3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T5C4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T5C5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T60Table = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.T6C1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T6C2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T6C3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T6C4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T6C5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.T6C6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.DetailReport2 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail3 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport3 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader2 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport4 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail4 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader3 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox4 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.collectionDataSource1 = new DevExpress.Persistent.Base.ReportsV2.CollectionDataSource();
            ((System.ComponentModel.ISupportInitialize)(this.M01Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.M02Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.M03Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.M04Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A01Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A02Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A03Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A04Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A05Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.N01Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.N02Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.P01Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.P02Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.P03Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T10Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T20Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T21Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T30Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T31Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T32Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T40Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T41Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T42Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T51Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T60Table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.collectionDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("Sequence", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 13F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.StylePriority.UseTextAlignment = false;
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 5.357615F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel16,
            this.xrLabel17,
            this.xrLabel18,
            this.xrLabel19,
            this.xrPageInfo1});
            this.PageFooter.HeightF = 70F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 9.75F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20F);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(128F, 25F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "ID verifica";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel16.Font = new System.Drawing.Font("Arial", 9.75F);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 45F);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(128F, 25F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UsePadding = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Documento";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel17.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Oid]")});
            this.xrLabel17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(128F, 20F);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(431F, 25F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UsePadding = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "xrTableCell21";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel18.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[MaintenancePlan].[OptionalDescription]")});
            this.xrLabel18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(128F, 45F);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(641.9131F, 25F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UsePadding = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel19.Font = new System.Drawing.Font("Arial", 9.75F);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(559F, 20F);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(128.9166F, 25F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UsePadding = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Pagina";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(687.9166F, 20F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(81.99652F, 25F);
            this.xrPageInfo1.StylePriority.UseBorders = false;
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.GroupHeader1});
            this.DetailReport.DataMember = "Checks";
            this.DetailReport.DataSource = this.collectionDataSource1;
            this.DetailReport.FilterString = "[PrintSequence] Is Not Null And [Hidden] = False";
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageBreak1,
            this.M01Table,
            this.M02Table,
            this.M03Table,
            this.M04Table,
            this.A01Table,
            this.A02Table,
            this.A03Table,
            this.A04Table,
            this.A05Table,
            this.N01Table,
            this.N02Table,
            this.P01Table,
            this.P02Table,
            this.P03Table,
            this.T10Table,
            this.T20Table,
            this.T21Table,
            this.T30Table,
            this.T31Table,
            this.T32Table,
            this.T40Table,
            this.T41Table,
            this.T42Table,
            this.T51Table,
            this.T60Table});
            this.Detail1.HeightF = 17F;
            this.Detail1.Name = "Detail1";
            this.Detail1.SortFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("PrintSequence", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.Detail1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail1_BeforePrint);
            // 
            // xrPageBreak1
            // 
            this.xrPageBreak1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPageBreak1.Name = "xrPageBreak1";
            // 
            // M01Table
            // 
            this.M01Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.M01Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M01Table.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.M01Table.Name = "M01Table";
            this.M01Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.M01Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.M01Table.StylePriority.UseBorders = false;
            this.M01Table.StylePriority.UseFont = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.m01SD,
            this.m01Ok});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // m01SD
            // 
            this.m01SD.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.m01SD.Multiline = true;
            this.m01SD.Name = "m01SD";
            this.m01SD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m01SD.Text = "m01SD";
            this.m01SD.Weight = 2.5D;
            // 
            // m01Ok
            // 
            this.m01Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.m01Ok.Multiline = true;
            this.m01Ok.Name = "m01Ok";
            this.m01Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m01Ok.StylePriority.UseTextAlignment = false;
            this.m01Ok.Text = "m01Ok";
            this.m01Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.m01Ok.Weight = 0.5D;
            this.m01Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // M02Table
            // 
            this.M02Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.M02Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M02Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.M02Table.Name = "M02Table";
            this.M02Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.M02Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.M02Table.StylePriority.UseBorders = false;
            this.M02Table.StylePriority.UseFont = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.m02SD,
            this.m02Ok});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // m02SD
            // 
            this.m02SD.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.m02SD.Multiline = true;
            this.m02SD.Name = "m02SD";
            this.m02SD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m02SD.Text = "m01SD";
            this.m02SD.Weight = 2.5D;
            // 
            // m02Ok
            // 
            this.m02Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.m02Ok.Multiline = true;
            this.m02Ok.Name = "m02Ok";
            this.m02Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m02Ok.StylePriority.UseTextAlignment = false;
            this.m02Ok.Text = "m01Ok";
            this.m02Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.m02Ok.Weight = 0.5D;
            this.m02Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // M03Table
            // 
            this.M03Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.M03Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M03Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.M03Table.Name = "M03Table";
            this.M03Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.M03Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.M03Table.StylePriority.UseBorders = false;
            this.M03Table.StylePriority.UseFont = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.m03SD,
            this.m03RT,
            this.m03Ok});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // m03SD
            // 
            this.m03SD.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.m03SD.Multiline = true;
            this.m03SD.Name = "m03SD";
            this.m03SD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m03SD.Weight = 2.25D;
            // 
            // m03RT
            // 
            this.m03RT.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[RunTimeText]")});
            this.m03RT.Multiline = true;
            this.m03RT.Name = "m03RT";
            this.m03RT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m03RT.StylePriority.UsePadding = false;
            this.m03RT.Text = "m03RT";
            this.m03RT.Weight = 1.5D;
            // 
            // m03Ok
            // 
            this.m03Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.m03Ok.Multiline = true;
            this.m03Ok.Name = "m03Ok";
            this.m03Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m03Ok.StylePriority.UseTextAlignment = false;
            this.m03Ok.Text = "m01Ok";
            this.m03Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.m03Ok.Weight = 0.75D;
            this.m03Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // M04Table
            // 
            this.M04Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.M04Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.M04Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.M04Table.Name = "M04Table";
            this.M04Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.M04Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.M04Table.StylePriority.UseBorders = false;
            this.M04Table.StylePriority.UseFont = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.m04SD,
            this.m04RT,
            this.m04Ok});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // m04SD
            // 
            this.m04SD.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.m04SD.Multiline = true;
            this.m04SD.Name = "m04SD";
            this.m04SD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m04SD.Weight = 2.25D;
            // 
            // m04RT
            // 
            this.m04RT.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[RunTimeText]")});
            this.m04RT.Multiline = true;
            this.m04RT.Name = "m04RT";
            this.m04RT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m04RT.StylePriority.UsePadding = false;
            this.m04RT.Text = "m03RT";
            this.m04RT.Weight = 1.5D;
            // 
            // m04Ok
            // 
            this.m04Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.m04Ok.Multiline = true;
            this.m04Ok.Name = "m04Ok";
            this.m04Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.m04Ok.StylePriority.UseTextAlignment = false;
            this.m04Ok.Text = "m01Ok";
            this.m04Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.m04Ok.Weight = 0.75D;
            this.m04Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // A01Table
            // 
            this.A01Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.A01Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.A01Table.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.A01Table.Name = "A01Table";
            this.A01Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.A01Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.A01Table.StylePriority.UseBorders = false;
            this.A01Table.StylePriority.UseFont = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.a01SD,
            this.a01ReadV,
            this.a01Ok});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // a01SD
            // 
            this.a01SD.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.a01SD.Multiline = true;
            this.a01SD.Name = "a01SD";
            this.a01SD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a01SD.Weight = 3.375D;
            this.a01SD.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.All_a01_05_SD_BeforePrint);
            // 
            // a01ReadV
            // 
            this.a01ReadV.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReadValue]")});
            this.a01ReadV.Multiline = true;
            this.a01ReadV.Name = "a01ReadV";
            this.a01ReadV.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a01ReadV.StylePriority.UsePadding = false;
            this.a01ReadV.StylePriority.UseTextAlignment = false;
            this.a01ReadV.Text = "m03RT";
            this.a01ReadV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.a01ReadV.Weight = 0.375D;
            // 
            // a01Ok
            // 
            this.a01Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.a01Ok.Multiline = true;
            this.a01Ok.Name = "a01Ok";
            this.a01Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a01Ok.StylePriority.UseTextAlignment = false;
            this.a01Ok.Text = "m01Ok";
            this.a01Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.a01Ok.Weight = 0.75D;
            this.a01Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // A02Table
            // 
            this.A02Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.A02Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.A02Table.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.A02Table.Name = "A02Table";
            this.A02Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.A02Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.A02Table.StylePriority.UseBorders = false;
            this.A02Table.StylePriority.UseFont = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.a02Sd,
            this.a02RT,
            this.a02ReadV,
            this.a02Ok});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // a02Sd
            // 
            this.a02Sd.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.a02Sd.Multiline = true;
            this.a02Sd.Name = "a02Sd";
            this.a02Sd.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a02Sd.Weight = 2.81250004452954D;
            this.a02Sd.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.All_a01_05_SD_BeforePrint);
            // 
            // a02RT
            // 
            this.a02RT.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[RunTimeText]")});
            this.a02RT.Multiline = true;
            this.a02RT.Name = "a02RT";
            this.a02RT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a02RT.StylePriority.UsePadding = false;
            this.a02RT.StylePriority.UseTextAlignment = false;
            this.a02RT.Text = "m03RT";
            this.a02RT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.a02RT.Weight = 1.40625002226477D;
            // 
            // a02ReadV
            // 
            this.a02ReadV.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReadValue]")});
            this.a02ReadV.Multiline = true;
            this.a02ReadV.Name = "a02ReadV";
            this.a02ReadV.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a02ReadV.StylePriority.UsePadding = false;
            this.a02ReadV.StylePriority.UseTextAlignment = false;
            this.a02ReadV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.a02ReadV.Weight = 0.46874999443380749D;
            // 
            // a02Ok
            // 
            this.a02Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.a02Ok.Multiline = true;
            this.a02Ok.Name = "a02Ok";
            this.a02Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a02Ok.StylePriority.UseTextAlignment = false;
            this.a02Ok.Text = "m01Ok";
            this.a02Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.a02Ok.Weight = 0.93749993877188242D;
            this.a02Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // A03Table
            // 
            this.A03Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.A03Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.A03Table.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.A03Table.Name = "A03Table";
            this.A03Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.A03Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.A03Table.StylePriority.UseBorders = false;
            this.A03Table.StylePriority.UseFont = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.a03SD,
            this.a03Lim,
            this.a03ReadV,
            this.a03Ok});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // a03SD
            // 
            this.a03SD.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.a03SD.Multiline = true;
            this.a03SD.Name = "a03SD";
            this.a03SD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a03SD.Weight = 2.81250004452954D;
            this.a03SD.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.All_a01_05_SD_BeforePrint);
            // 
            // a03Lim
            // 
            this.a03Lim.Multiline = true;
            this.a03Lim.Name = "a03Lim";
            this.a03Lim.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a03Lim.StylePriority.UsePadding = false;
            this.a03Lim.Text = "a03Lim";
            this.a03Lim.Weight = 1.40625002226477D;
            this.a03Lim.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.a03_a04Lim_BeforePrint);
            // 
            // a03ReadV
            // 
            this.a03ReadV.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReadValue]")});
            this.a03ReadV.Multiline = true;
            this.a03ReadV.Name = "a03ReadV";
            this.a03ReadV.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a03ReadV.StylePriority.UsePadding = false;
            this.a03ReadV.StylePriority.UseTextAlignment = false;
            this.a03ReadV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.a03ReadV.Weight = 0.46874999443380749D;
            // 
            // a03Ok
            // 
            this.a03Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.a03Ok.Multiline = true;
            this.a03Ok.Name = "a03Ok";
            this.a03Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a03Ok.StylePriority.UseTextAlignment = false;
            this.a03Ok.Text = "m01Ok";
            this.a03Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.a03Ok.Weight = 0.93749993877188242D;
            this.a03Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // A04Table
            // 
            this.A04Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.A04Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.A04Table.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.A04Table.Name = "A04Table";
            this.A04Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.A04Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.A04Table.StylePriority.UseBorders = false;
            this.A04Table.StylePriority.UseFont = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.a04Sd,
            this.a04RT,
            this.a04Lim,
            this.a04ReadV,
            this.a04Ok});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // a04Sd
            // 
            this.a04Sd.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.a04Sd.Multiline = true;
            this.a04Sd.Name = "a04Sd";
            this.a04Sd.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a04Sd.Weight = 1.7578125745869797D;
            this.a04Sd.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.All_a01_05_SD_BeforePrint);
            // 
            // a04RT
            // 
            this.a04RT.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[RunTimeText]")});
            this.a04RT.Multiline = true;
            this.a04RT.Name = "a04RT";
            this.a04RT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a04RT.StylePriority.UsePadding = false;
            this.a04RT.Weight = 1.757812505009573D;
            // 
            // a04Lim
            // 
            this.a04Lim.Multiline = true;
            this.a04Lim.Name = "a04Lim";
            this.a04Lim.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a04Lim.StylePriority.UsePadding = false;
            this.a04Lim.Weight = 1.7578125016698576D;
            this.a04Lim.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.a03_a04Lim_BeforePrint);
            // 
            // a04ReadV
            // 
            this.a04ReadV.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ReadValue]")});
            this.a04ReadV.Multiline = true;
            this.a04ReadV.Name = "a04ReadV";
            this.a04ReadV.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a04ReadV.StylePriority.UsePadding = false;
            this.a04ReadV.StylePriority.UseTextAlignment = false;
            this.a04ReadV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.a04ReadV.Weight = 0.58593748126048506D;
            // 
            // a04Ok
            // 
            this.a04Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.a04Ok.Multiline = true;
            this.a04Ok.Name = "a04Ok";
            this.a04Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a04Ok.StylePriority.UseTextAlignment = false;
            this.a04Ok.Text = "m01Ok";
            this.a04Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.a04Ok.Weight = 1.1718749820026444D;
            this.a04Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // A05Table
            // 
            this.A05Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.A05Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.A05Table.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.A05Table.Name = "A05Table";
            this.A05Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.A05Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.A05Table.StylePriority.UseBorders = false;
            this.A05Table.StylePriority.UseFont = false;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.a05SD,
            this.a05ValueInp,
            this.a05ReadV,
            this.a05Ok});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // a05SD
            // 
            this.a05SD.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.a05SD.Multiline = true;
            this.a05SD.Name = "a05SD";
            this.a05SD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a05SD.Weight = 1.7561033371403638D;
            // 
            // a05ValueInp
            // 
            this.a05ValueInp.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[RunTimeText]")});
            this.a05ValueInp.Multiline = true;
            this.a05ValueInp.Name = "a05ValueInp";
            this.a05ValueInp.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a05ValueInp.StylePriority.UsePadding = false;
            this.a05ValueInp.Weight = 3.5167678865512517D;
            // 
            // a05ReadV
            // 
            this.a05ReadV.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[TargetValue]")});
            this.a05ReadV.Multiline = true;
            this.a05ReadV.Name = "a05ReadV";
            this.a05ReadV.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a05ReadV.StylePriority.UsePadding = false;
            this.a05ReadV.StylePriority.UseTextAlignment = false;
            this.a05ReadV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.a05ReadV.Weight = 0.56788274723025822D;
            // 
            // a05Ok
            // 
            this.a05Ok.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[OK]")});
            this.a05Ok.Multiline = true;
            this.a05Ok.Name = "a05Ok";
            this.a05Ok.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.a05Ok.StylePriority.UseTextAlignment = false;
            this.a05Ok.Text = "m01Ok";
            this.a05Ok.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.a05Ok.Weight = 1.1737432555094247D;
            this.a05Ok.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.AllOk_BeforePrint);
            // 
            // N01Table
            // 
            this.N01Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.N01Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.N01Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.N01Table.Name = "N01Table";
            this.N01Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.N01Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.N01Table.StylePriority.UseBorders = false;
            this.N01Table.StylePriority.UseFont = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.n01SD,
            this.n01RT});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // n01SD
            // 
            this.n01SD.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.n01SD.Multiline = true;
            this.n01SD.Name = "n01SD";
            this.n01SD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.n01SD.Text = "m01SD";
            this.n01SD.Weight = 1.5D;
            // 
            // n01RT
            // 
            this.n01RT.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[RunTimeText]")});
            this.n01RT.Multiline = true;
            this.n01RT.Name = "n01RT";
            this.n01RT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.n01RT.StylePriority.UseTextAlignment = false;
            this.n01RT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.n01RT.Weight = 1.5D;
            // 
            // N02Table
            // 
            this.N02Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.N02Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.N02Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.N02Table.Name = "N02Table";
            this.N02Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.n02SD});
            this.N02Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.N02Table.StylePriority.UseBorders = false;
            this.N02Table.StylePriority.UseFont = false;
            // 
            // n02SD
            // 
            this.n02SD.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14});
            this.n02SD.Name = "n02SD";
            this.n02SD.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Multiline = true;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell14.Weight = 3D;
            this.xrTableCell14.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrTableCell14_BeforePrint);
            // 
            // P01Table
            // 
            this.P01Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.P01Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P01Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.P01Table.Name = "P01Table";
            this.P01Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.P01Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.P01Table.StylePriority.UseBorders = false;
            this.P01Table.StylePriority.UseFont = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.p01cell});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // p01cell
            // 
            this.p01cell.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.p01cell.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "NewLine()")});
            this.p01cell.Multiline = true;
            this.p01cell.Name = "p01cell";
            this.p01cell.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.p01cell.StylePriority.UseBorders = false;
            this.p01cell.Weight = 3D;
            // 
            // P02Table
            // 
            this.P02Table.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.P02Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P02Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.P02Table.Name = "P02Table";
            this.P02Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.P02Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.P02Table.StylePriority.UseBorders = false;
            this.P02Table.StylePriority.UseFont = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.p02cell});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // p02cell
            // 
            this.p02cell.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.p02cell.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.p02cell.Multiline = true;
            this.p02cell.Name = "p02cell";
            this.p02cell.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.p02cell.StylePriority.UseBorders = false;
            this.p02cell.Weight = 3D;
            // 
            // P03Table
            // 
            this.P03Table.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.P03Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P03Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.P03Table.Name = "P03Table";
            this.P03Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.P03Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.P03Table.StylePriority.UseBorders = false;
            this.P03Table.StylePriority.UseFont = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.p03cell});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // p03cell
            // 
            this.p03cell.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.p03cell.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[StepDescription]")});
            this.p03cell.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.p03cell.Multiline = true;
            this.p03cell.Name = "p03cell";
            this.p03cell.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.p03cell.StylePriority.UseBorders = false;
            this.p03cell.StylePriority.UseFont = false;
            this.p03cell.StylePriority.UseTextAlignment = false;
            this.p03cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.p03cell.Weight = 3D;
            // 
            // T10Table
            // 
            this.T10Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T10Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T10Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T10Table.Name = "T10Table";
            this.T10Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.T10Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T10Table.StylePriority.UseBorders = false;
            this.T10Table.StylePriority.UseFont = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.t10cell});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // t10cell
            // 
            this.t10cell.BackColor = System.Drawing.Color.Silver;
            this.t10cell.BorderColor = System.Drawing.Color.Black;
            this.t10cell.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.t10cell.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t10cell.Multiline = true;
            this.t10cell.Name = "t10cell";
            this.t10cell.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t10cell.StylePriority.UseBackColor = false;
            this.t10cell.StylePriority.UseBorderColor = false;
            this.t10cell.StylePriority.UseFont = false;
            this.t10cell.StylePriority.UseTextAlignment = false;
            this.t10cell.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t10cell.Weight = 3D;
            // 
            // T20Table
            // 
            this.T20Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T20Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T20Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T20Table.Name = "T20Table";
            this.T20Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.T20Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T20Table.StylePriority.UseBorders = false;
            this.T20Table.StylePriority.UseFont = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.t20c1,
            this.t20c2});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // t20c1
            // 
            this.t20c1.BackColor = System.Drawing.Color.Silver;
            this.t20c1.BorderColor = System.Drawing.Color.Black;
            this.t20c1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.t20c1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t20c1.Multiline = true;
            this.t20c1.Name = "t20c1";
            this.t20c1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t20c1.StylePriority.UseBackColor = false;
            this.t20c1.StylePriority.UseBorderColor = false;
            this.t20c1.StylePriority.UseFont = false;
            this.t20c1.StylePriority.UseTextAlignment = false;
            this.t20c1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t20c1.Weight = 3D;
            // 
            // t20c2
            // 
            this.t20c2.BackColor = System.Drawing.Color.Silver;
            this.t20c2.BorderColor = System.Drawing.Color.Black;
            this.t20c2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.t20c2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t20c2.Multiline = true;
            this.t20c2.Name = "t20c2";
            this.t20c2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t20c2.StylePriority.UseBackColor = false;
            this.t20c2.StylePriority.UseBorderColor = false;
            this.t20c2.StylePriority.UseFont = false;
            this.t20c2.StylePriority.UsePadding = false;
            this.t20c2.StylePriority.UseTextAlignment = false;
            this.t20c2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t20c2.Weight = 3D;
            // 
            // T21Table
            // 
            this.T21Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T21Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T21Table.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.T21Table.Name = "T21Table";
            this.T21Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.T21Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T21Table.StylePriority.UseBorders = false;
            this.T21Table.StylePriority.UseFont = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.t21c1,
            this.t21c2});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // t21c1
            // 
            this.t21c1.BackColor = System.Drawing.Color.Silver;
            this.t21c1.BorderColor = System.Drawing.Color.Black;
            this.t21c1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.t21c1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t21c1.Multiline = true;
            this.t21c1.Name = "t21c1";
            this.t21c1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t21c1.StylePriority.UseBackColor = false;
            this.t21c1.StylePriority.UseBorderColor = false;
            this.t21c1.StylePriority.UseFont = false;
            this.t21c1.StylePriority.UseTextAlignment = false;
            this.t21c1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t21c1.Weight = 4.9999998465979933D;
            // 
            // t21c2
            // 
            this.t21c2.BackColor = System.Drawing.Color.Silver;
            this.t21c2.BorderColor = System.Drawing.Color.Black;
            this.t21c2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.t21c2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t21c2.Multiline = true;
            this.t21c2.Name = "t21c2";
            this.t21c2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t21c2.StylePriority.UseBackColor = false;
            this.t21c2.StylePriority.UseBorderColor = false;
            this.t21c2.StylePriority.UseFont = false;
            this.t21c2.StylePriority.UsePadding = false;
            this.t21c2.StylePriority.UseTextAlignment = false;
            this.t21c2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t21c2.Weight = 1.0000001534020069D;
            // 
            // T30Table
            // 
            this.T30Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T30Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T30Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T30Table.Name = "T30Table";
            this.T30Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.T30Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T30Table.StylePriority.UseBorders = false;
            this.T30Table.StylePriority.UseFont = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.t30c1,
            this.t30c2,
            this.t30c3});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // t30c1
            // 
            this.t30c1.BackColor = System.Drawing.Color.Silver;
            this.t30c1.BorderColor = System.Drawing.Color.Black;
            this.t30c1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.t30c1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t30c1.Multiline = true;
            this.t30c1.Name = "t30c1";
            this.t30c1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t30c1.StylePriority.UseBackColor = false;
            this.t30c1.StylePriority.UseBorderColor = false;
            this.t30c1.StylePriority.UseFont = false;
            this.t30c1.StylePriority.UseTextAlignment = false;
            this.t30c1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t30c1.Weight = 3.661918319417655D;
            // 
            // t30c2
            // 
            this.t30c2.BackColor = System.Drawing.Color.Silver;
            this.t30c2.BorderColor = System.Drawing.Color.Black;
            this.t30c2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.t30c2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t30c2.Multiline = true;
            this.t30c2.Name = "t30c2";
            this.t30c2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t30c2.StylePriority.UseBackColor = false;
            this.t30c2.StylePriority.UseBorderColor = false;
            this.t30c2.StylePriority.UseFont = false;
            this.t30c2.StylePriority.UsePadding = false;
            this.t30c2.StylePriority.UseTextAlignment = false;
            this.t30c2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t30c2.Weight = 3.6619183194176554D;
            // 
            // t30c3
            // 
            this.t30c3.BackColor = System.Drawing.Color.Silver;
            this.t30c3.BorderColor = System.Drawing.Color.Black;
            this.t30c3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column3]")});
            this.t30c3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t30c3.Multiline = true;
            this.t30c3.Name = "t30c3";
            this.t30c3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t30c3.StylePriority.UseBackColor = false;
            this.t30c3.StylePriority.UseBorderColor = false;
            this.t30c3.StylePriority.UseFont = false;
            this.t30c3.StylePriority.UsePadding = false;
            this.t30c3.StylePriority.UseTextAlignment = false;
            this.t30c3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t30c3.Weight = 3.6619179774808184D;
            // 
            // T31Table
            // 
            this.T31Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T31Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T31Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T31Table.Name = "T31Table";
            this.T31Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.T31Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T31Table.StylePriority.UseBorders = false;
            this.T31Table.StylePriority.UseFont = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.t31c1,
            this.t31c2,
            this.t31c3});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // t31c1
            // 
            this.t31c1.BackColor = System.Drawing.Color.Silver;
            this.t31c1.BorderColor = System.Drawing.Color.Black;
            this.t31c1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.t31c1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t31c1.Multiline = true;
            this.t31c1.Name = "t31c1";
            this.t31c1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t31c1.StylePriority.UseBackColor = false;
            this.t31c1.StylePriority.UseBorderColor = false;
            this.t31c1.StylePriority.UseFont = false;
            this.t31c1.StylePriority.UseTextAlignment = false;
            this.t31c1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t31c1.Weight = 5.49287742213701D;
            // 
            // t31c2
            // 
            this.t31c2.BackColor = System.Drawing.Color.Silver;
            this.t31c2.BorderColor = System.Drawing.Color.Black;
            this.t31c2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.t31c2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t31c2.Multiline = true;
            this.t31c2.Name = "t31c2";
            this.t31c2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t31c2.StylePriority.UseBackColor = false;
            this.t31c2.StylePriority.UseBorderColor = false;
            this.t31c2.StylePriority.UseFont = false;
            this.t31c2.StylePriority.UsePadding = false;
            this.t31c2.StylePriority.UseTextAlignment = false;
            this.t31c2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t31c2.Weight = 3.661918101999611D;
            // 
            // t31c3
            // 
            this.t31c3.BackColor = System.Drawing.Color.Silver;
            this.t31c3.BorderColor = System.Drawing.Color.Black;
            this.t31c3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column3]")});
            this.t31c3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t31c3.Multiline = true;
            this.t31c3.Name = "t31c3";
            this.t31c3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t31c3.StylePriority.UseBackColor = false;
            this.t31c3.StylePriority.UseBorderColor = false;
            this.t31c3.StylePriority.UseFont = false;
            this.t31c3.StylePriority.UsePadding = false;
            this.t31c3.StylePriority.UseTextAlignment = false;
            this.t31c3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t31c3.Weight = 1.830959092179508D;
            // 
            // T32Table
            // 
            this.T32Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T32Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T32Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T32Table.Name = "T32Table";
            this.T32Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.T32Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T32Table.StylePriority.UseBorders = false;
            this.T32Table.StylePriority.UseFont = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.t32c1,
            this.t32c2,
            this.t32c3});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // t32c1
            // 
            this.t32c1.BackColor = System.Drawing.Color.Silver;
            this.t32c1.BorderColor = System.Drawing.Color.Black;
            this.t32c1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.t32c1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t32c1.Multiline = true;
            this.t32c1.Name = "t32c1";
            this.t32c1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t32c1.StylePriority.UseBackColor = false;
            this.t32c1.StylePriority.UseBorderColor = false;
            this.t32c1.StylePriority.UseFont = false;
            this.t32c1.StylePriority.UseTextAlignment = false;
            this.t32c1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t32c1.Weight = 8.239316076216042D;
            // 
            // t32c2
            // 
            this.t32c2.BackColor = System.Drawing.Color.Silver;
            this.t32c2.BorderColor = System.Drawing.Color.Black;
            this.t32c2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.t32c2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t32c2.Multiline = true;
            this.t32c2.Name = "t32c2";
            this.t32c2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t32c2.StylePriority.UseBackColor = false;
            this.t32c2.StylePriority.UseBorderColor = false;
            this.t32c2.StylePriority.UseFont = false;
            this.t32c2.StylePriority.UsePadding = false;
            this.t32c2.StylePriority.UseTextAlignment = false;
            this.t32c2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t32c2.Weight = 0.91547944792057878D;
            // 
            // t32c3
            // 
            this.t32c3.BackColor = System.Drawing.Color.Silver;
            this.t32c3.BorderColor = System.Drawing.Color.Black;
            this.t32c3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column3]")});
            this.t32c3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.t32c3.Multiline = true;
            this.t32c3.Name = "t32c3";
            this.t32c3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.t32c3.StylePriority.UseBackColor = false;
            this.t32c3.StylePriority.UseBorderColor = false;
            this.t32c3.StylePriority.UseFont = false;
            this.t32c3.StylePriority.UsePadding = false;
            this.t32c3.StylePriority.UseTextAlignment = false;
            this.t32c3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.t32c3.Weight = 1.830959092179508D;
            // 
            // T40Table
            // 
            this.T40Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T40Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T40Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T40Table.Name = "T40Table";
            this.T40Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.T40Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T40Table.StylePriority.UseBorders = false;
            this.T40Table.StylePriority.UseFont = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.T40C1,
            this.T40C2,
            this.T40C3,
            this.T40C4});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // T40C1
            // 
            this.T40C1.BackColor = System.Drawing.Color.Silver;
            this.T40C1.BorderColor = System.Drawing.Color.Black;
            this.T40C1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.T40C1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T40C1.Multiline = true;
            this.T40C1.Name = "T40C1";
            this.T40C1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T40C1.StylePriority.UseBackColor = false;
            this.T40C1.StylePriority.UseBorderColor = false;
            this.T40C1.StylePriority.UseFont = false;
            this.T40C1.StylePriority.UseTextAlignment = false;
            this.T40C1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T40C1.Weight = 4.8062677653032058D;
            // 
            // T40C2
            // 
            this.T40C2.BackColor = System.Drawing.Color.Silver;
            this.T40C2.BorderColor = System.Drawing.Color.Black;
            this.T40C2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.T40C2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T40C2.Multiline = true;
            this.T40C2.Name = "T40C2";
            this.T40C2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T40C2.StylePriority.UseBackColor = false;
            this.T40C2.StylePriority.UseBorderColor = false;
            this.T40C2.StylePriority.UseFont = false;
            this.T40C2.StylePriority.UsePadding = false;
            this.T40C2.StylePriority.UseTextAlignment = false;
            this.T40C2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T40C2.Weight = 4.8062677653032058D;
            // 
            // T40C3
            // 
            this.T40C3.BackColor = System.Drawing.Color.Silver;
            this.T40C3.BorderColor = System.Drawing.Color.Black;
            this.T40C3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column3]")});
            this.T40C3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T40C3.Multiline = true;
            this.T40C3.Name = "T40C3";
            this.T40C3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T40C3.StylePriority.UseBackColor = false;
            this.T40C3.StylePriority.UseBorderColor = false;
            this.T40C3.StylePriority.UseFont = false;
            this.T40C3.StylePriority.UsePadding = false;
            this.T40C3.StylePriority.UseTextAlignment = false;
            this.T40C3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T40C3.Weight = 4.8062678084421968D;
            // 
            // T40C4
            // 
            this.T40C4.BackColor = System.Drawing.Color.Silver;
            this.T40C4.BorderColor = System.Drawing.Color.Black;
            this.T40C4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column4]")});
            this.T40C4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T40C4.Multiline = true;
            this.T40C4.Name = "T40C4";
            this.T40C4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T40C4.StylePriority.UseBackColor = false;
            this.T40C4.StylePriority.UseBorderColor = false;
            this.T40C4.StylePriority.UseFont = false;
            this.T40C4.StylePriority.UsePadding = false;
            this.T40C4.StylePriority.UseTextAlignment = false;
            this.T40C4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T40C4.Weight = 4.8062673534835625D;
            // 
            // T41Table
            // 
            this.T41Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T41Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T41Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T41Table.Name = "T41Table";
            this.T41Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.T41Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T41Table.StylePriority.UseBorders = false;
            this.T41Table.StylePriority.UseFont = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.T41C1,
            this.T41C2,
            this.T41C3,
            this.T41C4});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // T41C1
            // 
            this.T41C1.BackColor = System.Drawing.Color.Silver;
            this.T41C1.BorderColor = System.Drawing.Color.Black;
            this.T41C1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.T41C1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T41C1.Multiline = true;
            this.T41C1.Name = "T41C1";
            this.T41C1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T41C1.StylePriority.UseBackColor = false;
            this.T41C1.StylePriority.UseBorderColor = false;
            this.T41C1.StylePriority.UseFont = false;
            this.T41C1.StylePriority.UseTextAlignment = false;
            this.T41C1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T41C1.Weight = 9.5876002607567532D;
            // 
            // T41C2
            // 
            this.T41C2.BackColor = System.Drawing.Color.Silver;
            this.T41C2.BorderColor = System.Drawing.Color.Black;
            this.T41C2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.T41C2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T41C2.Multiline = true;
            this.T41C2.Name = "T41C2";
            this.T41C2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T41C2.StylePriority.UseBackColor = false;
            this.T41C2.StylePriority.UseBorderColor = false;
            this.T41C2.StylePriority.UseFont = false;
            this.T41C2.StylePriority.UsePadding = false;
            this.T41C2.StylePriority.UseTextAlignment = false;
            this.T41C2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T41C2.Weight = 4.7937999973262162D;
            // 
            // T41C3
            // 
            this.T41C3.BackColor = System.Drawing.Color.Silver;
            this.T41C3.BorderColor = System.Drawing.Color.Black;
            this.T41C3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column3]")});
            this.T41C3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T41C3.Multiline = true;
            this.T41C3.Name = "T41C3";
            this.T41C3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T41C3.StylePriority.UseBackColor = false;
            this.T41C3.StylePriority.UseBorderColor = false;
            this.T41C3.StylePriority.UseFont = false;
            this.T41C3.StylePriority.UsePadding = false;
            this.T41C3.StylePriority.UseTextAlignment = false;
            this.T41C3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T41C3.Weight = 1.5979333243473879D;
            // 
            // T41C4
            // 
            this.T41C4.BackColor = System.Drawing.Color.Silver;
            this.T41C4.BorderColor = System.Drawing.Color.Black;
            this.T41C4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column4]")});
            this.T41C4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T41C4.Multiline = true;
            this.T41C4.Name = "T41C4";
            this.T41C4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T41C4.StylePriority.UseBackColor = false;
            this.T41C4.StylePriority.UseBorderColor = false;
            this.T41C4.StylePriority.UseFont = false;
            this.T41C4.StylePriority.UsePadding = false;
            this.T41C4.StylePriority.UseTextAlignment = false;
            this.T41C4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T41C4.Weight = 3.1958666284091213D;
            // 
            // T42Table
            // 
            this.T42Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T42Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T42Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T42Table.Name = "T42Table";
            this.T42Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.T42Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T42Table.StylePriority.UseBorders = false;
            this.T42Table.StylePriority.UseFont = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.T42C1,
            this.T42C2,
            this.T42C3,
            this.T42C4});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // T42C1
            // 
            this.T42C1.BackColor = System.Drawing.Color.Silver;
            this.T42C1.BorderColor = System.Drawing.Color.Black;
            this.T42C1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.T42C1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T42C1.Multiline = true;
            this.T42C1.Name = "T42C1";
            this.T42C1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T42C1.StylePriority.UseBackColor = false;
            this.T42C1.StylePriority.UseBorderColor = false;
            this.T42C1.StylePriority.UseFont = false;
            this.T42C1.StylePriority.UseTextAlignment = false;
            this.T42C1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T42C1.Weight = 4.7938002080468838D;
            // 
            // T42C2
            // 
            this.T42C2.BackColor = System.Drawing.Color.Silver;
            this.T42C2.BorderColor = System.Drawing.Color.Black;
            this.T42C2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.T42C2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T42C2.Multiline = true;
            this.T42C2.Name = "T42C2";
            this.T42C2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T42C2.StylePriority.UseBackColor = false;
            this.T42C2.StylePriority.UseBorderColor = false;
            this.T42C2.StylePriority.UseFont = false;
            this.T42C2.StylePriority.UsePadding = false;
            this.T42C2.StylePriority.UseTextAlignment = false;
            this.T42C2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T42C2.Weight = 9.5876000500360856D;
            // 
            // T42C3
            // 
            this.T42C3.BackColor = System.Drawing.Color.Silver;
            this.T42C3.BorderColor = System.Drawing.Color.Black;
            this.T42C3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column3]")});
            this.T42C3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T42C3.Multiline = true;
            this.T42C3.Name = "T42C3";
            this.T42C3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T42C3.StylePriority.UseBackColor = false;
            this.T42C3.StylePriority.UseBorderColor = false;
            this.T42C3.StylePriority.UseFont = false;
            this.T42C3.StylePriority.UsePadding = false;
            this.T42C3.StylePriority.UseTextAlignment = false;
            this.T42C3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T42C3.Weight = 1.5979333243473879D;
            // 
            // T42C4
            // 
            this.T42C4.BackColor = System.Drawing.Color.Silver;
            this.T42C4.BorderColor = System.Drawing.Color.Black;
            this.T42C4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column4]")});
            this.T42C4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T42C4.Multiline = true;
            this.T42C4.Name = "T42C4";
            this.T42C4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T42C4.StylePriority.UseBackColor = false;
            this.T42C4.StylePriority.UseBorderColor = false;
            this.T42C4.StylePriority.UseFont = false;
            this.T42C4.StylePriority.UsePadding = false;
            this.T42C4.StylePriority.UseTextAlignment = false;
            this.T42C4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T42C4.Weight = 3.1958666284091213D;
            // 
            // T51Table
            // 
            this.T51Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T51Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T51Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T51Table.Name = "T51Table";
            this.T51Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.T51Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T51Table.StylePriority.UseBorders = false;
            this.T51Table.StylePriority.UseFont = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.T5C1,
            this.T5C2,
            this.T5C3,
            this.T5C4,
            this.T5C5});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // T5C1
            // 
            this.T5C1.BackColor = System.Drawing.Color.Silver;
            this.T5C1.BorderColor = System.Drawing.Color.Black;
            this.T5C1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.T5C1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T5C1.Multiline = true;
            this.T5C1.Name = "T5C1";
            this.T5C1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T5C1.StylePriority.UseBackColor = false;
            this.T5C1.StylePriority.UseBorderColor = false;
            this.T5C1.StylePriority.UseFont = false;
            this.T5C1.StylePriority.UseTextAlignment = false;
            this.T5C1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T5C1.Weight = 4.7938002080468838D;
            // 
            // T5C2
            // 
            this.T5C2.BackColor = System.Drawing.Color.Silver;
            this.T5C2.BorderColor = System.Drawing.Color.Black;
            this.T5C2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.T5C2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T5C2.Multiline = true;
            this.T5C2.Name = "T5C2";
            this.T5C2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T5C2.StylePriority.UseBackColor = false;
            this.T5C2.StylePriority.UseBorderColor = false;
            this.T5C2.StylePriority.UseFont = false;
            this.T5C2.StylePriority.UsePadding = false;
            this.T5C2.StylePriority.UseTextAlignment = false;
            this.T5C2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T5C2.Weight = 4.7937999973262162D;
            // 
            // T5C3
            // 
            this.T5C3.BackColor = System.Drawing.Color.Silver;
            this.T5C3.BorderColor = System.Drawing.Color.Black;
            this.T5C3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column3]")});
            this.T5C3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T5C3.Multiline = true;
            this.T5C3.Name = "T5C3";
            this.T5C3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T5C3.StylePriority.UseBackColor = false;
            this.T5C3.StylePriority.UseBorderColor = false;
            this.T5C3.StylePriority.UseFont = false;
            this.T5C3.StylePriority.UsePadding = false;
            this.T5C3.StylePriority.UseTextAlignment = false;
            this.T5C3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T5C3.Weight = 4.7938000261539671D;
            // 
            // T5C4
            // 
            this.T5C4.BackColor = System.Drawing.Color.Silver;
            this.T5C4.BorderColor = System.Drawing.Color.Black;
            this.T5C4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column4]")});
            this.T5C4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T5C4.Multiline = true;
            this.T5C4.Name = "T5C4";
            this.T5C4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T5C4.StylePriority.UseBackColor = false;
            this.T5C4.StylePriority.UseBorderColor = false;
            this.T5C4.StylePriority.UseFont = false;
            this.T5C4.StylePriority.UsePadding = false;
            this.T5C4.StylePriority.UseTextAlignment = false;
            this.T5C4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T5C4.Weight = 1.597933277505831D;
            // 
            // T5C5
            // 
            this.T5C5.BackColor = System.Drawing.Color.Silver;
            this.T5C5.BorderColor = System.Drawing.Color.Black;
            this.T5C5.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column5]")});
            this.T5C5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T5C5.Multiline = true;
            this.T5C5.Name = "T5C5";
            this.T5C5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T5C5.StylePriority.UseBackColor = false;
            this.T5C5.StylePriority.UseBorderColor = false;
            this.T5C5.StylePriority.UseFont = false;
            this.T5C5.StylePriority.UsePadding = false;
            this.T5C5.StylePriority.UseTextAlignment = false;
            this.T5C5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T5C5.Weight = 3.1958666217366245D;
            // 
            // T60Table
            // 
            this.T60Table.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.T60Table.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.T60Table.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0F);
            this.T60Table.Name = "T60Table";
            this.T60Table.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.T60Table.SizeF = new System.Drawing.SizeF(771F, 17F);
            this.T60Table.StylePriority.UseBorders = false;
            this.T60Table.StylePriority.UseFont = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.T6C1,
            this.T6C2,
            this.T6C3,
            this.T6C4,
            this.T6C5,
            this.T6C6});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // T6C1
            // 
            this.T6C1.BackColor = System.Drawing.Color.Silver;
            this.T6C1.BorderColor = System.Drawing.Color.Black;
            this.T6C1.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column1]")});
            this.T6C1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T6C1.Multiline = true;
            this.T6C1.Name = "T6C1";
            this.T6C1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T6C1.StylePriority.UseBackColor = false;
            this.T6C1.StylePriority.UseBorderColor = false;
            this.T6C1.StylePriority.UseFont = false;
            this.T6C1.StylePriority.UseTextAlignment = false;
            this.T6C1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T6C1.Weight = 3.1958668638160903D;
            // 
            // T6C2
            // 
            this.T6C2.BackColor = System.Drawing.Color.Silver;
            this.T6C2.BorderColor = System.Drawing.Color.Black;
            this.T6C2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column2]")});
            this.T6C2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T6C2.Multiline = true;
            this.T6C2.Name = "T6C2";
            this.T6C2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T6C2.StylePriority.UseBackColor = false;
            this.T6C2.StylePriority.UseBorderColor = false;
            this.T6C2.StylePriority.UseFont = false;
            this.T6C2.StylePriority.UsePadding = false;
            this.T6C2.StylePriority.UseTextAlignment = false;
            this.T6C2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T6C2.Weight = 3.1958666530954227D;
            // 
            // T6C3
            // 
            this.T6C3.BackColor = System.Drawing.Color.Silver;
            this.T6C3.BorderColor = System.Drawing.Color.Black;
            this.T6C3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column3]")});
            this.T6C3.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T6C3.Multiline = true;
            this.T6C3.Name = "T6C3";
            this.T6C3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T6C3.StylePriority.UseBackColor = false;
            this.T6C3.StylePriority.UseBorderColor = false;
            this.T6C3.StylePriority.UseFont = false;
            this.T6C3.StylePriority.UsePadding = false;
            this.T6C3.StylePriority.UseTextAlignment = false;
            this.T6C3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T6C3.Weight = 3.1958666819231736D;
            // 
            // T6C4
            // 
            this.T6C4.BackColor = System.Drawing.Color.Silver;
            this.T6C4.BorderColor = System.Drawing.Color.Black;
            this.T6C4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column4]")});
            this.T6C4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T6C4.Multiline = true;
            this.T6C4.Name = "T6C4";
            this.T6C4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T6C4.StylePriority.UseBackColor = false;
            this.T6C4.StylePriority.UseBorderColor = false;
            this.T6C4.StylePriority.UseFont = false;
            this.T6C4.StylePriority.UsePadding = false;
            this.T6C4.StylePriority.UseTextAlignment = false;
            this.T6C4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T6C4.Weight = 3.1958666217366245D;
            // 
            // T6C5
            // 
            this.T6C5.BackColor = System.Drawing.Color.Silver;
            this.T6C5.BorderColor = System.Drawing.Color.Black;
            this.T6C5.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column5]")});
            this.T6C5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T6C5.Multiline = true;
            this.T6C5.Name = "T6C5";
            this.T6C5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T6C5.StylePriority.UseBackColor = false;
            this.T6C5.StylePriority.UseBorderColor = false;
            this.T6C5.StylePriority.UseFont = false;
            this.T6C5.StylePriority.UsePadding = false;
            this.T6C5.StylePriority.UseTextAlignment = false;
            this.T6C5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T6C5.Weight = 3.1958666217366245D;
            // 
            // T6C6
            // 
            this.T6C6.BackColor = System.Drawing.Color.Silver;
            this.T6C6.BorderColor = System.Drawing.Color.Black;
            this.T6C6.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Column6]")});
            this.T6C6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.T6C6.Multiline = true;
            this.T6C6.Name = "T6C6";
            this.T6C6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.T6C6.StylePriority.UseBackColor = false;
            this.T6C6.StylePriority.UseBorderColor = false;
            this.T6C6.StylePriority.UseFont = false;
            this.T6C6.StylePriority.UsePadding = false;
            this.T6C6.StylePriority.UseTextAlignment = false;
            this.T6C6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.T6C6.Weight = 3.1958666217366245D;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.HeightF = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Visible = false;
            // 
            // DetailReport2
            // 
            this.DetailReport2.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail3,
            this.ReportHeader1});
            this.DetailReport2.DataMember = "Visit.InstrumentsOnVisit";
            this.DetailReport2.DataSource = this.collectionDataSource1;
            this.DetailReport2.Level = 3;
            this.DetailReport2.Name = "DetailReport2";
            this.DetailReport2.ReportPrintOptions.PrintOnEmptyDataSource = false;
            // 
            // Detail3
            // 
            this.Detail3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail3.HeightF = 25F;
            this.Detail3.Name = "Detail3";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable3.SizeF = new System.Drawing.SizeF(770F, 25F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Family].[Name]")});
            this.xrTableCell7.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell7.Weight = 0.98591551249524845D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Brand].[Name]")});
            this.xrTableCell66.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell66.Multiline = true;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.StylePriority.UsePadding = false;
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "xrTableCell66";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell66.Weight = 0.98591551249524845D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Model].[Name]")});
            this.xrTableCell67.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell67.Multiline = true;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell67.StylePriority.UseFont = false;
            this.xrTableCell67.StylePriority.UsePadding = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.Text = "xrTableCell67";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell67.Weight = 0.98591551249524856D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SerialNumber]")});
            this.xrTableCell68.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell68.Multiline = true;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell68.StylePriority.UseFont = false;
            this.xrTableCell68.StylePriority.UsePadding = false;
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.Text = "xrTableCell68";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell68.Weight = 0.98591551249524845D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[LastInterventionDate]")});
            this.xrTableCell69.Font = new System.Drawing.Font("Calibri", 11F);
            this.xrTableCell69.Multiline = true;
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell69.StylePriority.UseFont = false;
            this.xrTableCell69.StylePriority.UsePadding = false;
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell69.TextFormatString = "{0:dd/MM/yyyy}";
            this.xrTableCell69.Weight = 0.98591541480773837D;
            // 
            // ReportHeader1
            // 
            this.ReportHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.ReportHeader1.HeightF = 67.70834F;
            this.ReportHeader1.Name = "ReportHeader1";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 17.70833F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12,
            this.xrTableRow13});
            this.xrTable4.SizeF = new System.Drawing.SizeF(770F, 50F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.BackColor = System.Drawing.Color.Silver;
            this.xrTableCell61.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell61.Multiline = true;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell61.StylePriority.UseBackColor = false;
            this.xrTableCell61.StylePriority.UseFont = false;
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "Strumenti di analisi utilizzati";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell61.Weight = 11.832321493583626D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Tipo";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell6.Weight = 0.98600460962517589D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell70.Multiline = true;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell70.StylePriority.UseFont = false;
            this.xrTableCell70.StylePriority.UsePadding = false;
            this.xrTableCell70.StylePriority.UseTextAlignment = false;
            this.xrTableCell70.Text = "Marca";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell70.Weight = 0.98600451192884375D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell71.Multiline = true;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.StylePriority.UsePadding = false;
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.Text = "Modello";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell71.Weight = 0.98600451192884375D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell72.Multiline = true;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.StylePriority.UsePadding = false;
            this.xrTableCell72.StylePriority.UseTextAlignment = false;
            this.xrTableCell72.Text = "Matricola";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell72.Weight = 0.98600451192884353D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell73.Multiline = true;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell73.StylePriority.UseFont = false;
            this.xrTableCell73.StylePriority.UsePadding = false;
            this.xrTableCell73.StylePriority.UseTextAlignment = false;
            this.xrTableCell73.Text = "Data verifica";
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell73.Weight = 0.98600451207391449D;
            // 
            // DetailReport3
            // 
            this.DetailReport3.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader2});
            this.DetailReport3.DataMember = "Equipment.ExternalAccessories";
            this.DetailReport3.DataSource = this.collectionDataSource1;
            this.DetailReport3.FilterString = "[Dismissed] = False";
            this.DetailReport3.Level = 2;
            this.DetailReport3.Name = "DetailReport3";
            this.DetailReport3.ReportPrintOptions.PrintOnEmptyDataSource = false;
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail2.HeightF = 25F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(7.947286E-06F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable2.SizeF = new System.Drawing.SizeF(770F, 25F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.xrTableCell2,
            this.xrTableCell18});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[AccessoryType].[Description]")});
            this.xrTableCell15.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell15.Multiline = true;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UsePadding = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell15.Weight = 0.985803994045009D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Equipment].[Brand].[Name]")});
            this.xrTableCell16.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell16.Multiline = true;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell16.StylePriority.UseFont = false;
            this.xrTableCell16.StylePriority.UsePadding = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell16.Weight = 0.98580409172148475D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Model]")});
            this.xrTableCell17.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell17.Multiline = true;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell17.StylePriority.UseFont = false;
            this.xrTableCell17.StylePriority.UsePadding = false;
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell17.Weight = 0.98580389636853327D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SerialNumber]")});
            this.xrTableCell2.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell2.Weight = 0.98580389636853327D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ExpirationDate]")});
            this.xrTableCell18.Font = new System.Drawing.Font("Calibri", 11F);
            this.xrTableCell18.Multiline = true;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UseFont = false;
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell18.TextFormatString = "{0:dd/MM/yyyy}";
            this.xrTableCell18.Weight = 0.985804767478154D;
            // 
            // ReportHeader2
            // 
            this.ReportHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.ReportHeader2.HeightF = 68F;
            this.ReportHeader2.Name = "ReportHeader2";
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 18F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow37});
            this.xrTable5.SizeF = new System.Drawing.SizeF(770F, 50F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.BackColor = System.Drawing.Color.Silver;
            this.xrTableCell19.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell19.Multiline = true;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell19.StylePriority.UseBackColor = false;
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "Accessori esterni";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell19.Weight = 5.9154943379772176D;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell1,
            this.xrTableCell23});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell20.Multiline = true;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell20.StylePriority.UseFont = false;
            this.xrTableCell20.StylePriority.UsePadding = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "Tipo";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell20.Weight = 0.985914158437368D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell21.Multiline = true;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "Marca";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell21.Weight = 0.98591570436074727D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell22.Multiline = true;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "Modello";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell22.Weight = 0.98591550898567859D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Matricola";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell1.Weight = 0.9859155089856787D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell23.Multiline = true;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell23.StylePriority.UseFont = false;
            this.xrTableCell23.StylePriority.UsePadding = false;
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "Data scadenza";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell23.Weight = 0.985917812370396D;
            // 
            // DetailReport4
            // 
            this.DetailReport4.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail4,
            this.ReportHeader3});
            this.DetailReport4.DataMember = "Equipment.InternalModules";
            this.DetailReport4.DataSource = this.collectionDataSource1;
            this.DetailReport4.FilterString = "[Dismissed] = False";
            this.DetailReport4.Level = 1;
            this.DetailReport4.Name = "DetailReport4";
            this.DetailReport4.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            this.DetailReport4.ReportPrintOptions.PrintOnEmptyDataSource = false;
            // 
            // Detail4
            // 
            this.Detail4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.Detail4.HeightF = 25F;
            this.Detail4.Name = "Detail4";
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(7.947286E-06F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable6.SizeF = new System.Drawing.SizeF(770F, 25F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseFont = false;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell5});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[AccessoryType].[Description]")});
            this.xrTableCell24.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.StylePriority.UsePadding = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell24.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Equipment].[Brand].[Name]")});
            this.xrTableCell25.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell25.Multiline = true;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell25.StylePriority.UseFont = false;
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell25.Weight = 1D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Model]")});
            this.xrTableCell26.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UseFont = false;
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell26.Weight = 1D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[SerialNumber]")});
            this.xrTableCell27.Font = new System.Drawing.Font("Calibri", 11F);
            this.xrTableCell27.Multiline = true;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UseFont = false;
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell27.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[ExpirationDate]")});
            this.xrTableCell5.Font = new System.Drawing.Font("Calibri", 11F);
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell5.TextFormatString = "{0:dd/MM/yyyy}";
            this.xrTableCell5.Weight = 1D;
            // 
            // ReportHeader3
            // 
            this.ReportHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.ReportHeader3.HeightF = 68F;
            this.ReportHeader3.Name = "ReportHeader3";
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 18F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39,
            this.xrTableRow40});
            this.xrTable7.SizeF = new System.Drawing.SizeF(770F, 50F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseFont = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.BackColor = System.Drawing.Color.Silver;
            this.xrTableCell28.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.xrTableCell28.Multiline = true;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell28.StylePriority.UseBackColor = false;
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "Moduli interni";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell28.Weight = 11.999999048695381D;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell4});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.StylePriority.UsePadding = false;
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "Tipo";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell29.Weight = 0.99886869194183081D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell30.Multiline = true;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "Marca";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell30.Weight = 0.9988702448379031D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell31.Multiline = true;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.StylePriority.UsePadding = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "Modello";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell31.Weight = 0.99887004689569336D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell32.Multiline = true;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell32.StylePriority.UseFont = false;
            this.xrTableCell32.StylePriority.UsePadding = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "Matricola";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell32.Weight = 0.99886999606586246D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Data scadenza";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell4.Weight = 0.99887217343016965D;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox3});
            this.PageHeader.HeightF = 70F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.ImageSource = new DevExpress.XtraPrinting.Drawing.ImageSource("img", resources.GetString("xrPictureBox3.ImageSource"));
            this.xrPictureBox3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.SizeF = new System.Drawing.SizeF(772F, 67.37514F);
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel14,
            this.xrLabel15,
            this.xrPictureBox2,
            this.xrLabel30,
            this.xrLabel29,
            this.xrPictureBox4,
            this.xrLabel28,
            this.xrLabel27,
            this.xrBarCode1,
            this.xrLabel1,
            this.xrLabel3,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel6,
            this.xrLabel7,
            this.xrLabel8,
            this.xrLabel9,
            this.xrLabel10,
            this.xrLabel11,
            this.xrLabel12,
            this.xrLabel13,
            this.xrLabel20,
            this.xrLabel21,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel25,
            this.xrLabel26});
            this.GroupHeader2.HeightF = 320F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // xrLabel14
            // 
            this.xrLabel14.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Visit].[Company].[Title]")});
            this.xrLabel14.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(514.0003F, 201.0417F);
            this.xrLabel14.Multiline = true;
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(258.3481F, 25F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UsePadding = false;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(386.0439F, 201.0417F);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(127.9564F, 25.00002F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Firma  cliente:";
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.EditOptions.EditorName = "Image";
            this.xrPictureBox2.EditOptions.ID = "CustomerSignature";
            this.xrPictureBox2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ImageSource", "[Visit].[CustomerSignature]")});
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(386.0439F, 226.0417F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(385.96F, 87F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLabel30
            // 
            this.xrLabel30.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Visit].[Technician].[FirstName] +\' \'+ [Visit].[Technician].[LastName]")});
            this.xrLabel30.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(100.0001F, 201.0417F);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(258.3481F, 25F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UsePadding = false;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(0.0002384186F, 201.0417F);
            this.xrLabel29.Multiline = true;
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(99.99984F, 25F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.Text = "Firma tecnico";
            // 
            // xrPictureBox4
            // 
            this.xrPictureBox4.EditOptions.EditorName = "Image";
            this.xrPictureBox4.EditOptions.ID = "TechnicianSignature";
            this.xrPictureBox4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "ImageSource", "[Visit].[TechnicianSignature]")});
            this.xrPictureBox4.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 226.0417F);
            this.xrPictureBox4.Name = "xrPictureBox4";
            this.xrPictureBox4.SizeF = new System.Drawing.SizeF(358.348F, 87.00002F);
            this.xrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLabel28
            // 
            this.xrLabel28.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Equipment].[QrCode]")});
            this.xrLabel28.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(642.5002F, 176.0417F);
            this.xrLabel28.Multiline = true;
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(129.4998F, 25F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UsePadding = false;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(589.5688F, 176.0417F);
            this.xrLabel27.Multiline = true;
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(52.41888F, 25F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UsePadding = false;
            this.xrLabel27.Text = "QR:";
            // 
            // xrBarCode1
            // 
            this.xrBarCode1.Alignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrBarCode1.AutoModule = true;
            this.xrBarCode1.LocationFloat = new DevExpress.Utils.PointFloat(589.5688F, 76.04166F);
            this.xrBarCode1.Name = "xrBarCode1";
            this.xrBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
            this.xrBarCode1.SizeF = new System.Drawing.SizeF(182.4312F, 100F);
            this.xrBarCode1.Symbology = qrCodeGenerator1;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.041672F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(200.0001F, 25F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Cliente:";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Visit].[Company].[Title]")});
            this.xrLabel3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(200.0001F, 1.041672F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(249.5416F, 25F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UsePadding = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(449.5417F, 1.041672F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(140.0271F, 25F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UsePadding = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Data intervento:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Visit].[VisitDate]")});
            this.xrLabel5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(589.5688F, 1.041672F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(182.4312F, 25F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26.04167F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(200.0001F, 25F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UsePadding = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Sede:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Visit].[Location].[City]")});
            this.xrLabel7.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(200.0001F, 26.04167F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(249.5416F, 25F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UsePadding = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(449.5417F, 26.04167F);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(140.0271F, 25F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UsePadding = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Nome tecnico:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Visit].[Technician].[FirstName] +\' \'+ [Visit].[Technician].[LastName]")});
            this.xrLabel9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(589.5688F, 26.04167F);
            this.xrLabel9.Multiline = true;
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(182.4312F, 25F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UsePadding = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 51.04167F);
            this.xrLabel10.Multiline = true;
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(772F, 25F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UsePadding = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Apparecchiatura verificata";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 76.04167F);
            this.xrLabel11.Multiline = true;
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(200F, 25F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UsePadding = false;
            this.xrLabel11.Text = "Famiglia:";
            // 
            // xrLabel12
            // 
            this.xrLabel12.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Equipment].[Family].[Name]")});
            this.xrLabel12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(200F, 76.04166F);
            this.xrLabel12.Multiline = true;
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(389.5688F, 25F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UsePadding = false;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 101.0417F);
            this.xrLabel13.Multiline = true;
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(200F, 25F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UsePadding = false;
            this.xrLabel13.Text = "Marca:";
            // 
            // xrLabel20
            // 
            this.xrLabel20.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Equipment].[Brand].[Name]")});
            this.xrLabel20.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(200F, 101.0417F);
            this.xrLabel20.Multiline = true;
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(389.5688F, 25F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UsePadding = false;
            this.xrLabel20.Text = "xrTableCell3";
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 126.0417F);
            this.xrLabel21.Multiline = true;
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(200F, 25F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UsePadding = false;
            this.xrLabel21.Text = "Modello:";
            // 
            // xrLabel22
            // 
            this.xrLabel22.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Equipment].[Model].[Name]")});
            this.xrLabel22.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(200F, 126.0417F);
            this.xrLabel22.Multiline = true;
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(389.5688F, 25.00001F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UsePadding = false;
            this.xrLabel22.Text = "xrTableCell7";
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 151.0417F);
            this.xrLabel23.Multiline = true;
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(200F, 25F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UsePadding = false;
            this.xrLabel23.Text = "Inventario:";
            // 
            // xrLabel24
            // 
            this.xrLabel24.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Equipment].[InventoryNumber]")});
            this.xrLabel24.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(200F, 151.0417F);
            this.xrLabel24.Multiline = true;
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(389.5688F, 25F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UsePadding = false;
            this.xrLabel24.Text = "xrTableCell9";
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(0F, 176.0417F);
            this.xrLabel25.Multiline = true;
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(200F, 25F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UsePadding = false;
            this.xrLabel25.Text = "Matricola:";
            // 
            // xrLabel26
            // 
            this.xrLabel26.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[Equipment].[SerialNumber]")});
            this.xrLabel26.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(200F, 176.0417F);
            this.xrLabel26.Multiline = true;
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(389.5688F, 25F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UsePadding = false;
            this.xrLabel26.Text = "xrTableCell11";
            // 
            // collectionDataSource1
            // 
            this.collectionDataSource1.Name = "collectionDataSource1";
            this.collectionDataSource1.ObjectTypeName = "AmbulApp.Module.BusinessObjects.Intervention";
            this.collectionDataSource1.TopReturnedRecords = 0;
            // 
            // CheckListReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageFooter,
            this.DetailReport,
            this.DetailReport2,
            this.DetailReport3,
            this.DetailReport4,
            this.PageHeader,
            this.GroupHeader2});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.collectionDataSource1});
            this.DataSource = this.collectionDataSource1;
            this.Margins = new System.Drawing.Printing.Margins(23, 23, 13, 5);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "18.2";
            ((System.ComponentModel.ISupportInitialize)(this.M01Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.M02Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.M03Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.M04Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A01Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A02Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A03Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A04Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A05Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.N01Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.N02Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.P01Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.P02Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.P03Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T10Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T20Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T21Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T30Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T31Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T32Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T40Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T41Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T42Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T51Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T60Table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.collectionDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.Persistent.Base.ReportsV2.CollectionDataSource collectionDataSource1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport2;
        private DevExpress.XtraReports.UI.DetailBand Detail3;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTable M01Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell m01SD;
        private DevExpress.XtraReports.UI.XRTableCell m01Ok;
        private DevExpress.XtraReports.UI.XRTable M02Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell m02SD;
        private DevExpress.XtraReports.UI.XRTableCell m02Ok;
        private DevExpress.XtraReports.UI.XRTable M03Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell m03SD;
        private DevExpress.XtraReports.UI.XRTableCell m03RT;
        private DevExpress.XtraReports.UI.XRTableCell m03Ok;
        private DevExpress.XtraReports.UI.XRTable M04Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell m04SD;
        private DevExpress.XtraReports.UI.XRTableCell m04RT;
        private DevExpress.XtraReports.UI.XRTableCell m04Ok;
        private DevExpress.XtraReports.UI.XRTable A01Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell a01SD;
        private DevExpress.XtraReports.UI.XRTableCell a01ReadV;
        private DevExpress.XtraReports.UI.XRTableCell a01Ok;
        private DevExpress.XtraReports.UI.XRTable A02Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell a02Sd;
        private DevExpress.XtraReports.UI.XRTableCell a02RT;
        private DevExpress.XtraReports.UI.XRTableCell a02ReadV;
        private DevExpress.XtraReports.UI.XRTableCell a02Ok;
        private DevExpress.XtraReports.UI.XRTable A03Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell a03SD;
        private DevExpress.XtraReports.UI.XRTableCell a03Lim;
        private DevExpress.XtraReports.UI.XRTableCell a03ReadV;
        private DevExpress.XtraReports.UI.XRTableCell a03Ok;
        private DevExpress.XtraReports.UI.XRTable A04Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell a04Sd;
        private DevExpress.XtraReports.UI.XRTableCell a04RT;
        private DevExpress.XtraReports.UI.XRTableCell a04Lim;
        private DevExpress.XtraReports.UI.XRTableCell a04ReadV;
        private DevExpress.XtraReports.UI.XRTableCell a04Ok;
        private DevExpress.XtraReports.UI.XRTable A05Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell a05SD;
        private DevExpress.XtraReports.UI.XRTableCell a05ValueInp;
        private DevExpress.XtraReports.UI.XRTableCell a05ReadV;
        private DevExpress.XtraReports.UI.XRTableCell a05Ok;
        private DevExpress.XtraReports.UI.XRTable N01Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell n01SD;
        private DevExpress.XtraReports.UI.XRTableCell n01RT;
        private DevExpress.XtraReports.UI.XRTable N02Table;
        private DevExpress.XtraReports.UI.XRTableRow n02SD;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTable P01Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell p01cell;
        private DevExpress.XtraReports.UI.XRTable P02Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell p02cell;
        private DevExpress.XtraReports.UI.XRTable P03Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell p03cell;
        private DevExpress.XtraReports.UI.XRTable T10Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell t10cell;
        private DevExpress.XtraReports.UI.XRTable T20Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell t20c1;
        private DevExpress.XtraReports.UI.XRTableCell t20c2;
        private DevExpress.XtraReports.UI.XRTable T21Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell t21c1;
        private DevExpress.XtraReports.UI.XRTableCell t21c2;
        private DevExpress.XtraReports.UI.XRTable T30Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell t30c1;
        private DevExpress.XtraReports.UI.XRTableCell t30c2;
        private DevExpress.XtraReports.UI.XRTableCell t30c3;
        private DevExpress.XtraReports.UI.XRTable T31Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell t31c1;
        private DevExpress.XtraReports.UI.XRTableCell t31c2;
        private DevExpress.XtraReports.UI.XRTableCell t31c3;
        private DevExpress.XtraReports.UI.XRTable T32Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell t32c1;
        private DevExpress.XtraReports.UI.XRTableCell t32c2;
        private DevExpress.XtraReports.UI.XRTableCell t32c3;
        private DevExpress.XtraReports.UI.XRTable T40Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell T40C1;
        private DevExpress.XtraReports.UI.XRTableCell T40C2;
        private DevExpress.XtraReports.UI.XRTableCell T40C3;
        private DevExpress.XtraReports.UI.XRTableCell T40C4;
        private DevExpress.XtraReports.UI.XRTable T41Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell T41C1;
        private DevExpress.XtraReports.UI.XRTableCell T41C2;
        private DevExpress.XtraReports.UI.XRTableCell T41C3;
        private DevExpress.XtraReports.UI.XRTableCell T41C4;
        private DevExpress.XtraReports.UI.XRTable T42Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell T42C1;
        private DevExpress.XtraReports.UI.XRTableCell T42C2;
        private DevExpress.XtraReports.UI.XRTableCell T42C3;
        private DevExpress.XtraReports.UI.XRTableCell T42C4;
        private DevExpress.XtraReports.UI.XRTable T51Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell T5C1;
        private DevExpress.XtraReports.UI.XRTableCell T5C2;
        private DevExpress.XtraReports.UI.XRTableCell T5C3;
        private DevExpress.XtraReports.UI.XRTableCell T5C4;
        private DevExpress.XtraReports.UI.XRTableCell T5C5;
        private DevExpress.XtraReports.UI.XRTable T60Table;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell T6C1;
        private DevExpress.XtraReports.UI.XRTableCell T6C2;
        private DevExpress.XtraReports.UI.XRTableCell T6C3;
        private DevExpress.XtraReports.UI.XRTableCell T6C4;
        private DevExpress.XtraReports.UI.XRTableCell T6C5;
        private DevExpress.XtraReports.UI.XRTableCell T6C6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport3;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader2;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport4;
        private DevExpress.XtraReports.UI.DetailBand Detail4;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader3;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRPageBreak xrPageBreak1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
    }
}
