﻿using System;
using System.Drawing;
using DevExpress.ExpressApp;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;

namespace AmbulApp.Module.Reports
{
    public partial class CheckListReport : DevExpress.XtraReports.UI.XtraReport
    {
        public IObjectSpace ObjectSpace { get; set; }
        public Visit CurrentVisit { get; set; }

        public CheckListReport()
        {
            InitializeComponent();
        }

        private void Detail1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var currentRow = DetailReport.GetCurrentRow();
            if (currentRow is null)
            {
                e.Cancel = true;
                return;
            }

            var stepTypeCode = ((Check) currentRow).StepType.Code;

            SetDefaultVisible();

            switch (stepTypeCode)
            {
                case @"M01":
                    SetM01Visible(true);
                    break;
                case @"M02":
                    SetM02Visible(true);
                    break;
                case @"M03":
                    SetM03Visible(true);
                    break;
                case @"M04":
                    SetM04Visible(true);
                    break;
                case @"A01":
                    SetA01Visible(true);
                    break;
                case @"A02":
                    SetA02Visible(true);
                    break;
                case @"A03":
                    SetA03Visible(true);
                    break;
                case @"A04":
                    SetA04Visible(true);
                    break;
                case @"A05":
                    SetA05Visible(true);
                    break;
                case @"N01":
                    SetN01Visible(true);
                    break;
                case @"N02":
                    SetN02Visible(true);
                    break;
                case @"P01":
                    SetP01Visible(true);
                    break;
                case @"P02":
                    SetP02Visible(true);
                    break;
                case @"P03":
                    SetP03Visible(true);
                    break;
                case @"T10":
                    SetT10Visible(true);
                    break;
                case @"T20":
                    SetT20Visible(true);
                    break;
                case @"T21":
                    SetT21Visible(true);
                    break;
                case @"T30":
                    SetT30Visible(true);
                    break;
                case @"T31":
                    SetT31Visible(true);
                    break;
                case @"T32":
                    SetT32Visible(true);
                    break;
                case @"T40":
                    SetT40Visible(true);
                    break;
                case @"T41":
                    SetT41Visible(true);
                    break;
                case @"T42":
                    SetT42Visible(true);
                    break;
                case @"T51":
                    SetT51Visible(true);
                    break;
                case @"T60":
                    SetT60Visible(true);
                    break;
                case @"P04":
                    xrPageBreak1.Visible = true;
                    break;
                default:
                    SetDefaultVisible();
                    break;
            }
        }

        private void SetT60Visible(bool value)
        {
            T60Table.Visible = value;
        }

        private void SetT51Visible(bool value)
        {
            T51Table.Visible = value;
        }

        private void SetT42Visible(bool value)
        {
            T42Table.Visible = value;
        }

        private void SetT41Visible(bool value)
        {
            T41Table.Visible = value;
        }

        private void SetT40Visible(bool value)
        {
            T40Table.Visible = value;
        }

        private void SetT32Visible(bool value)
        {
            T32Table.Visible = value;
        }

        private void SetT31Visible(bool value)
        {
            T31Table.Visible = value;
        }

        private void SetT30Visible(bool value)
        {
            T30Table.Visible = value;
        }

        private void SetT21Visible(bool value)
        {
            T21Table.Visible = value;
        }

        private void SetT20Visible(bool value)
        {
            T20Table.Visible = value;
        }

        private void SetT10Visible(bool value)
        {
            T10Table.Visible = value;
        }

        private void SetP03Visible(bool value)
        {
            P03Table.Visible = value;
        }

        private void SetP02Visible(bool value)
        {
            P02Table.Visible = value;
        }

        private void SetP01Visible(bool value)
        {
            P01Table.Visible = value;
        }

        private void SetN02Visible(bool value)
        {
            N02Table.Visible = value;
        }

        private void SetN01Visible(bool value)
        {
            N01Table.Visible = value;
        }

        private void SetA05Visible(bool value)
        {
            A05Table.Visible = value;
        }

        private void SetA04Visible(bool value)
        {
            A04Table.Visible = value;
        }

        private void SetA03Visible(bool value)
        {
            A03Table.Visible = value;
        }

        private void SetA02Visible(bool value)
        {
            A02Table.Visible = value;
        }

        private void SetA01Visible(bool value)
        {
            A01Table.Visible = value;
        }

        private void SetM04Visible(bool value)
        {
            M04Table.Visible = value;
        }

        private void SetM03Visible(bool value)
        {
            M03Table.Visible = value;
        }

        private void SetM02Visible(bool value)
        {
            M02Table.Visible = value;
        }

        private void SetM01Visible(bool value)
        {
            M01Table.Visible = value;
        }

        private void SetDefaultVisible()
        {
            SetM01Visible(false);
            SetM02Visible(false);
            SetM03Visible(false);
            SetM04Visible(false);
            SetA01Visible(false);
            SetA02Visible(false);
            SetA03Visible(false);
            SetA04Visible(false);
            SetA05Visible(false);
            SetN01Visible(false);
            SetN02Visible(false);
            SetP01Visible(false);
            SetP02Visible(false);
            SetP03Visible(false);
            SetT10Visible(false);
            SetT20Visible(false);
            SetT21Visible(false);
            SetT30Visible(false);
            SetT31Visible(false);
            SetT32Visible(false);
            SetT40Visible(false);
            SetT41Visible(false);
            SetT42Visible(false);
            SetT51Visible(false);
            SetT60Visible(false);
            xrPageBreak1.Visible = false;
        }

        private void AllOk_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var currentRow = DetailReport.GetCurrentRow();
            if (currentRow is null)
            {
                e.Cancel = true;
                return;
            }

            var xrTableCell = sender as XRTableCell;
            var okPrintStyle = ((Check) currentRow).OkPrintStyle;
            var ok = ((Check) DetailReport.GetCurrentRow()).OK;
            var stepTypeCode = ((Check) currentRow).StepType.Code;
            if (xrTableCell != null)
            {
                switch (okPrintStyle)
                {
                    case OkPrintStyle.DefaultValue:
                    {
                        xrTableCell.Text = string.Empty;
                        break;
                    }
                    case OkPrintStyle.OkNotOk:
                    {
                        if (stepTypeCode.Equals("M02") || stepTypeCode.Equals("M04"))
                        {
                            xrTableCell.Text = ok == OkType.Ok ? "OK[X] KO[ ] NA[ ]" :
                                ok == OkType.Ko ? "OK[ ] KO[X] NA[ ]" : "OK[ ] KO[ ] NA[X]";
                            xrTableCell.TextAlignment = TextAlignment.MiddleLeft;
                            xrTableCell.Font = new Font("Courier New", 8.5F, FontStyle.Regular, GraphicsUnit.Point, 0);
                        }
                        else
                        {
                            xrTableCell.Text = ok == OkType.Ok ? "OK[X] KO[ ]" : "OK[ ] KO[X]";
                            xrTableCell.TextAlignment = TextAlignment.MiddleLeft;
                            xrTableCell.Font = new Font("Courier New", 8.5F, FontStyle.Regular, GraphicsUnit.Point, 0);
                        }

                        break;
                    }
                    case OkPrintStyle.OnlyFlag:
                    {
                        xrTableCell.Text = ok == OkType.Ok ? "  [X]" : "  [ ]";
                        xrTableCell.TextAlignment = TextAlignment.MiddleLeft;
                        xrTableCell.Font = new Font("Courier New", 8.5F, FontStyle.Regular, GraphicsUnit.Point, 0);
                        break;
                    }
                    case OkPrintStyle.YesNo:
                    {
                        if (stepTypeCode.Equals("M02") || stepTypeCode.Equals("M04"))
                        {
                            xrTableCell.Text = ok == OkType.Ok ? "SI[X] NO[ ] NA[ ]" :
                                ok == OkType.Ko ? "SI[ ] NO[X] NA[ ]" : "SI[ ] NO[ ] NA[X]";
                            xrTableCell.TextAlignment = TextAlignment.MiddleLeft;
                            xrTableCell.Font = new Font("Courier New", 8.5F, FontStyle.Regular, GraphicsUnit.Point, 0);
                        }
                        else
                        {
                            xrTableCell.Text = ok == OkType.Ok ? "SI[X] NO[ ]" : "SI[ ] NO[X]";
                            xrTableCell.TextAlignment = TextAlignment.MiddleLeft;
                            xrTableCell.Font = new Font("Courier New", 8.5F, FontStyle.Regular, GraphicsUnit.Point, 0);
                        }

                        break;
                    }
                }
            }
        }

        private void xrTableCell14_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var currentRow = DetailReport.GetCurrentRow();
            if (currentRow is null)
            {
                e.Cancel = true;
                return;
            }

            var xrTableCell = sender as XRTableCell;
            var check = (Check) currentRow;
            var stepTypeCode = check.StepType.Code;
            var checkRunTimeText = check.RunTimeText;
            var checkStepDescription = check.StepDescription;
            if (xrTableCell != null)
            {
                if (stepTypeCode.Equals("N02"))
                {
                    xrTableCell.Text = String.IsNullOrEmpty(checkStepDescription)
                        ? checkRunTimeText
                        : checkStepDescription + Environment.NewLine + checkRunTimeText;
                }
            }
        }

        private void All_a01_05_SD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var currentRow = DetailReport.GetCurrentRow();
            if (currentRow is null)
            {
                e.Cancel = true;
                return;
            }

            var xrTableCell = sender as XRTableCell;
            var check = (Check) currentRow;
            var stepTypeCode = check.StepType.Code;
            var checkTargetType = check.TargetType;
            var checkTargetValue = check.TargetValue;
            var checkStepDescription = check.StepDescription;
            if (xrTableCell != null)
            {
                if (stepTypeCode.Equals("A01") || stepTypeCode.Equals("A02") || stepTypeCode.Equals("A03") ||
                    stepTypeCode.Equals("A04"))
                {
                    xrTableCell.Text = checkTargetType == TargetType.InputRuntime
                        ? checkStepDescription + " " + checkTargetValue
                        : checkStepDescription;
                }
            }
        }

        private void a03_a04Lim_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var currentRow = DetailReport.GetCurrentRow();
            if (currentRow is null)
            {
                e.Cancel = true;
                return;
            }

            var xrTableCell = sender as XRTableCell;
            var check = (Check) currentRow;
            var stepTypeCode = check.StepType.Code;
            var checkCalculatedTargeValue = check.CalculatedTargeValue;
            var checkLimitPrintStyle = check.LimitPrintStyle;
            var checkCalculatedLowPercentage = check.CalculatedLowPercentage;
            var checkCalculatedLowLimit = check.CalculatedLowLimit;
            var checkCalculatedHighLimit = check.CalculatedHighLimit;
            if (xrTableCell != null)
            {
                if (stepTypeCode.Equals("A03"))
                {
                    if (checkLimitPrintStyle == LimitPrintStyle.TargetVar)
                    {
                        xrTableCell.Text = $@"{checkCalculatedTargeValue} ± {checkCalculatedLowPercentage}%";
                    }
                    else if (checkLimitPrintStyle == LimitPrintStyle.MinMax)
                    {
                        xrTableCell.Text = $@"{checkCalculatedLowLimit} ÷ {checkCalculatedHighLimit}";
                    }
                }
                else if (stepTypeCode.Equals("A04"))
                {
                    if (checkLimitPrintStyle == LimitPrintStyle.TargetVar)
                    {
                        xrTableCell.Text = $@"{checkCalculatedTargeValue} ± {checkCalculatedLowPercentage}%";
                    }
                    else if (checkLimitPrintStyle == LimitPrintStyle.MinMax)
                    {
                        xrTableCell.Text = $@"{checkCalculatedLowLimit} ÷ {checkCalculatedHighLimit}";
                    }
                }
            }
        }
    }
}
