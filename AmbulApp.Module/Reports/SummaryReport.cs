﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.IO;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace AmbulApp.Module.Reports
{
    public partial class SummaryReport : XtraReport
    {
        public SummaryReport()
        {
            InitializeComponent();
        }
      
        public IObjectSpace ObjectSpace { get; set; }
        public Visit CurrentVisit { get; set; }

        private void xrPictureBox1_AfterPrint(object sender, EventArgs e)
        {
            PrintingSystem.EditingFieldChanged += PrintingSystem_EditingFieldChanged;
        }

        void PrintingSystem_EditingFieldChanged(object sender, EditingFieldEventArgs e)
        {
            var image = ((ImageBrick)e.EditingField.Brick).Image;

            if (e.EditingField.ID.Equals("CustomerSignatureId"))
            {
                CurrentVisit.CustomerSignature = ImageToByteArray(image);
            }

            ObjectSpace.CommitChanges();
        }

        private byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, ImageFormat.Png);
                return ms.ToArray();
            }
        }

        private void SummaryReport_AfterPrint(object sender, EventArgs e)
        {
            ObjectSpace = DevExpress.Persistent.Base.ReportsV2.DataSourceBase.CreateObjectSpace(typeof(Visit), (XtraReport)sender);
            var currentRow = this.GetCurrentRow();
            var visit = (Visit)currentRow;
            CurrentVisit = visit;
        }

        private void xrTableCell18_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var currentRow = DetailReport.GetCurrentRow();
            if (currentRow is null)
            {
                e.Cancel = true;
                return;
            }

            var xrTableCell = sender as XRTableCell;
            var intervention = (Intervention)currentRow;
            var interventionChecks = intervention.Checks;
            var maintenancePlan = intervention.MaintenancePlan;

            if (maintenancePlan != null)
            {
                foreach (Check check in interventionChecks)
                {
                    if (check.PrintSequence == maintenancePlan.PrintSequenceSummaryReport)
                    {
                        if (xrTableCell != null)
                        {
                            xrTableCell.Text = check.OK == OkType.Ok
                                ? "SI"
                                : "Vedi scheda";
                        }
                        break;
                    }
                }
            }
        }
    }
}
