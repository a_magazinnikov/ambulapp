﻿using System;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    public class ReadedManual : BaseObject
    {
        private OperatorManual _fOperatorManual;
        public OperatorManual OperatorManual
        {
            get => _fOperatorManual;
            set => SetPropertyValue(nameof(OperatorManual), ref _fOperatorManual, value);
        }

        DateTime _fReadOn;

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ReadOn
        {
            get => _fReadOn;
            set => SetPropertyValue<DateTime>(nameof(ReadOn), ref _fReadOn, value);
        }

        bool _fUnderstood;
        public bool Understood
        {
            get => _fUnderstood;
            set => SetPropertyValue(nameof(Understood), ref _fUnderstood, value);
        }

        private AmbulUser _fUser;
        [Association(@"ReadedManualReferencesUser")]
        public AmbulUser User
        {
            get => _fUser;
            set => SetPropertyValue(nameof(User), ref _fUser, value);
        }

        public ReadedManual(Session session) : base(session)
        {
        }
    }
}
