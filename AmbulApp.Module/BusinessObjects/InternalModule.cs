﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    public class InternalModule : BaseObject
    {
        private AccessoryType _fAccessoryType;

        [RuleRequiredField("InternalModuleAccessoryTypeRequired", DefaultContexts.Save,
             CustomMessageTemplate = "Accessory Type Is Required For Internal Module"), DataSourceCriteria("InternalModule = true And Family.Oid = '@This.Equipment.Family.Oid'")]
        public AccessoryType AccessoryType
        {
            get => _fAccessoryType;
            set => SetPropertyValue(nameof(AccessoryType), ref _fAccessoryType, value);
        }

        bool _fDismissed;
        public bool Dismissed
        {
            get => _fDismissed;
            set => SetPropertyValue(nameof(Dismissed), ref _fDismissed, value);
        }

        Equipment _fEquipment;
        [Association(@"EquipmentReferencesInternalModules"), DataSourceCriteria("Company.Oid = '@This.Company.Oid'")]
        public Equipment Equipment
        {
            get => _fEquipment;
            set => SetPropertyValue(nameof(Equipment), ref _fEquipment, value);
        }


        DateTime _fExpirationDate;

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ExpirationDate
        {
            get => _fExpirationDate;
            set => SetPropertyValue<DateTime>(nameof(ExpirationDate), ref _fExpirationDate, value);
        }

        private string _fSerialNumber;

        [Size(255)]
        [VisibleInListView(true)]
        public string SerialNumber
        {
            get => _fSerialNumber;
            set => SetPropertyValue(nameof(SerialNumber), ref _fSerialNumber, value);
        }

        public InternalModule(Session session) : base(session)
        {
        }
    }
}
