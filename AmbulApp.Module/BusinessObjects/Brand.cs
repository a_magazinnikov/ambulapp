﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Equipment settings"), XafDefaultProperty("Name")]
    public class Brand : BaseObject
    {
        string _fName;
        [Size(255)]
        public string Name
        {
            get => _fName;
            set => SetPropertyValue(nameof(Name), ref _fName, value);
        }

        [Association(@"EquipmentReferencesBrand")]
        public XPCollection<Equipment> Equipments => GetCollection<Equipment>(nameof(Equipments));

        [Association(@"ModelReferencesBrand"), DevExpress.Xpo.Aggregated]
        public XPCollection<Model> Models => GetCollection<Model>(nameof(Models));

        public Brand(Session session) : base(session)
        {
        }
    }
}
