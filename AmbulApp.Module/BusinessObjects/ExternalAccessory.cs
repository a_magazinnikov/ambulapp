﻿using System;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;


namespace AmbulApp.Module.BusinessObjects
{
    public class ExternalAccessory : BaseObject
    {
        private AccessoryType _fAccessoryType;

        [RuleRequiredField("ExternalAccessoryTypeRequired", DefaultContexts.Save,
            CustomMessageTemplate = "Accessory Type Is Required For External Accessory"), DataSourceCriteria("InternalModule = false And Family.Oid = '@This.Equipment.Family.Oid'")]
        public AccessoryType AccessoryType
        {
            get => _fAccessoryType;
            set => SetPropertyValue(nameof(AccessoryType), ref _fAccessoryType, value);
        }

        bool _fDismissed;
        public bool Dismissed
        {
            get => _fDismissed;
            set => SetPropertyValue(nameof(Dismissed), ref _fDismissed, value);
        }

        Equipment _fEquipment;
        [Association(@"EquipmentReferencesExternalAccessories"), DataSourceCriteria("Company.Oid = '@This.Company.Oid'")]
        public Equipment Equipment
        {
            get => _fEquipment;
            set => SetPropertyValue(nameof(Equipment), ref _fEquipment, value);
        }

        DateTime _fExpirationDate;

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ExpirationDate
        {
            get => _fExpirationDate;
            set => SetPropertyValue<DateTime>(nameof(ExpirationDate), ref _fExpirationDate, value);
        }

        private string _fSerialNumber;

        [Size(255)]
        [VisibleInListView(true)]
        public string SerialNumber
        {
            get => _fSerialNumber;
            set => SetPropertyValue(nameof(SerialNumber), ref _fSerialNumber, value);
        }

        public ExternalAccessory(Session session) : base(session)
        {
        }
    }
}
