﻿using System.ComponentModel;
using System.Linq;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Equipment settings")]
    [XafDefaultProperty("OptionalDescription")]
    public class MaintenancePlan : BaseObject
    {
        private int _fCounterRecurrency;
        public int CounterRecurrency
        {
            get => _fCounterRecurrency;
            set => SetPropertyValue<int>(nameof(CounterRecurrency), ref _fCounterRecurrency, value);
        }

        int _fDaysRecurrency;
        public int DaysRecurrency
        {
            get => _fDaysRecurrency;
            set => SetPropertyValue<int>(nameof(DaysRecurrency), ref _fDaysRecurrency, value);
        }

        double _fEstimatedEffort;
        [ModelDefault("DisplayFormat", "{0:N2}")]
        public double EstimatedEffort
        {
            get => _fEstimatedEffort;
            set => SetPropertyValue(nameof(EstimatedEffort), ref _fEstimatedEffort, value);
        }

        private Family _fFamily;
        [Association(@"MaintenancePlanReferencesFamily")]
        public Family Family
        {
            get => _fFamily;
            set => SetPropertyValue(nameof(Family), ref _fFamily, value);
        }

        string _fOptionalDescription;
        [Size(SizeAttribute.Unlimited)]
        public string OptionalDescription
        {
            get => _fOptionalDescription;
            set => SetPropertyValue(nameof(OptionalDescription), ref _fOptionalDescription, value);
        }

        int? _fPrintSequenceSummaryReport;
        [XafDisplayName("Print Sequence of the check to put in the summary")]
        public int? PrintSequenceSummaryReport
        {
            get => _fPrintSequenceSummaryReport;
            set => SetPropertyValue(nameof(PrintSequenceSummaryReport), ref _fPrintSequenceSummaryReport, value);
        }

        [Association(@"InterventionReferencesMaintenancePlan"), Browsable(false)]
        public XPCollection<Intervention> Interventions => GetCollection<Intervention>(nameof(Interventions));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           

        [Association(@"MaintenanceStepReferencesMaintenancePlan"), DevExpress.Xpo.Aggregated]
        public XPCollection<MaintenanceStep> MaintenanceSteps => GetCollection<MaintenanceStep>(nameof(MaintenanceSteps));

        public MaintenancePlan(Session session) : base(session)
        {
        }                                                               

        public override void AfterConstruction()
        {
            base.AfterConstruction();

            OptionalDescription = "standard";
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            Session.Delete(MaintenanceSteps);
        }

        public int GetNextLineSeqNo() => MaintenanceSteps.Max(s => s.Sequence) + 10;
    }
}
