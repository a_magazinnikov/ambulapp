﻿using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [ImageName("BO_FileAttachment"), XafDefaultProperty("FileName")]
    [RuleCriteria("RuleCriteria for Link and File", DefaultContexts.Save, "Link is not null Or File is not null", CustomMessageTemplate = "Link or File must be not null", SkipNullOrEmptyValues = false)]
    public class ModelAttachment : FileAttachmentBase
    {
        private string _fDescription;
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get => _fDescription;
            set => SetPropertyValue(nameof(Description), ref _fDescription, value);
        }

        private AttachmentCategory _attachmentCategory;
        [VisibleInDetailView(true), VisibleInListView(true), VisibleInLookupListView(true)]
        public AttachmentCategory Category
        {
            get => _attachmentCategory;
            set => SetPropertyValue(nameof(Category), ref _attachmentCategory, value);
        }

        private string _link;
        [RuleRegularExpression("HyperLinkDemoObject.Url.RuleRegularExpression", DefaultContexts.Save, @"(((http|https|ftp)\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;amp;%\$#\=~])*)|([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})", CustomMessageTemplate = "Invalid link")]
        [EditorAlias("HyperLinkStringPropertyEditor"), Size(SizeAttribute.Unlimited), VisibleInLookupListView(true), VisibleInListView(true)]
        public string Link
        {
            get => _link;
            set => SetPropertyValue(nameof(Link), ref _link, value);
        }

        [Association(@"ModelAttachmentReferencesModel"), VisibleInDetailView(false)]
        public XPCollection<Model> Models => GetCollection<Model>(nameof(Models));

        public ModelAttachment(Session session) : base(session)
        {
        }
    }
}
