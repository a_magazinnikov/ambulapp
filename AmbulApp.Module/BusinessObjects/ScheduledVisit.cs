﻿using System;
using System.ComponentModel;
using System.Reflection;
using AmbulApp.Module.Common;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;                                                                                                               

namespace AmbulApp.Module.BusinessObjects
{
    [DefaultClassOptions, Persistent("BaseForScheduledVisitsScreen"), CreatableItem(false)]
    public class ScheduledVisit : XPLiteObject
    {
        public ScheduledVisit(Session session) : base(session)
        {

        }

        [Key, Persistent, Browsable(false)]
        public ScheduledVisitViewKey Key;

        private string _fCompany;
        [VisibleInDetailView(false), VisibleInLookupListView(false)]
        [Persistent("CompanyTitle")]
        public string Company
        {
            get => _fCompany;
            set => SetPropertyValue(nameof(Company), ref _fCompany, value);
        }

        private string _fLocationCity;
        [VisibleInDetailView(false), VisibleInLookupListView(false)]
        [Persistent("LocationCity")]
        public string LocationCity
        {
            get => _fLocationCity;
            set => SetPropertyValue(nameof(LocationCity), ref _fLocationCity, value);
        }

        [VisibleInDetailView(false), VisibleInLookupListView(false)]

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime? DueDate
        {
            get
            {
                if (Key.DueDate <= new DateTime(1900, 1, 1))
                {
                    return null;
                }

                return Key.DueDate;
            }
        }

        [NonPersistent, VisibleInDetailView(true), VisibleInLookupListView(false), VisibleInListView(false)]
        public String CompanyLocation => $@"{Company} \ {LocationCity}";

        DateTime _fAgreedDate;

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime AgreedDate
        {
            get => _fAgreedDate;
            set => SetPropertyValue<DateTime>(nameof(AgreedDate), ref _fAgreedDate, value);
        }

        int _fVehiclesCount;
        [VisibleInDetailView(false)]
        public int VehiclesCount
        {
            get => _fVehiclesCount;
            set => SetPropertyValue<int>(nameof(VehiclesCount), ref _fVehiclesCount, value);
        }

        int _fEquipmentsCount;
        [VisibleInDetailView(false)]
        public int EquipmentsCount
        {
            get => _fEquipmentsCount;
            set => SetPropertyValue<int>(nameof(EquipmentsCount), ref _fEquipmentsCount, value);
        }

        double _fTotalEstimatedEffort;

        [VisibleInDetailView(false)]
        public double TotalEstimatedEffort
        {
            get => _fTotalEstimatedEffort;
            set => SetPropertyValue(nameof(TotalEstimatedEffort), ref _fTotalEstimatedEffort, value);
        }

        DateTime _fExpirationDate;

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Persistent("firstexpiration")]
        public DateTime ExpirationDate
        {
            get => _fExpirationDate;
            set => SetPropertyValue<DateTime>(nameof(ExpirationDate), ref _fExpirationDate, value);
        }
    }

    [TypeConverter(typeof(LocalStructTypeConverter<ScheduledVisitViewKey>))]
    public struct ScheduledVisitViewKey
    {
        [Persistent("LocationOid"), Browsable(false)]
        public Guid LocationOid;

        [Persistent("CompanyOid"), Browsable(false)]
        public Guid CompanyOid;

        [Persistent("DueDate"), Browsable(false)]
        public DateTime DueDate;
    }
}
