﻿using System;
using System.ComponentModel;
using System.Reflection;
using AmbulApp.Module.BusinessLogic;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [DefaultProperty("StepDescription")]
    [Appearance("ShowDefaultFields", AppearanceItemType.ViewItem,
        "IsNull(StepType)", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;", Context = "DetailView")]
    [Appearance("ShowN01_N02Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and StepType.Code = 'N01' or StepType.Code = 'N02'", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;StepDescription;InternalText", Context = "DetailView")]
    [Appearance("ShowM01_M04Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (StepType.Code = 'M01' or StepType.Code = 'M02' or StepType.Code = 'M03' or StepType.Code = 'M04')", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;InternalText;OkPrintStyle;StepDescription", Context = "DetailView")]
    [Appearance("ShowA01_A02Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (StepType.Code = 'A01' or StepType.Code = 'A02')", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;InternalText;OkPrintStyle;TargetType;TargetSequence;TargetValue;VarianceType;LoTolerance;HiTolerance;LoPercentage;HiPercentage;StepDescription;VarianceSeq;", Context = "DetailView")]
    [Appearance("ShowA03_A04Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (StepType.Code = 'A03' or StepType.Code = 'A04')", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;InternalText;OkPrintStyle;TargetType;TargetSequence;TargetValue;VarianceType;LoTolerance;HiTolerance;LoPercentage;HiPercentage;LimitPrintStyle;StepDescription;VarianceSeq;", Context = "DetailView")]
    [Appearance("ShowA05Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and StepType.Code = 'A05'", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;InternalText;OkPrintStyle;VarianceType;LoTolerance;HiTolerance;LoPercentage;HiPercentage;ValueSequence;StepDescription;TargetSequence;ValueInputs;VarianceSeq;", Context = "DetailView")]
    [Appearance("ShowP01_P04Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (StepType.Code = 'P01' Or StepType.Code = 'P04')", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;", Context = "DetailView")]
    [Appearance("ShowP02_P03Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (StepType.Code = 'P02' or StepType.Code = 'P03')", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;StepDescription;", Context = "DetailView")]
    [Appearance("ShowT10Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and StepType.Code = 'T10'", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;Column1;", Context = "DetailView")]
    [Appearance("ShowT20_T21Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (StepType.Code = 'T20' or StepType.Code = 'T21')", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;Column1;Column2;", Context = "DetailView")]
    [Appearance("ShowT30_T32Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (StepType.Code = 'T30' or StepType.Code = 'T31' or StepType.Code = 'T32')", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;Column1;Column2;Column3;", Context = "DetailView")]
    [Appearance("ShowT40_T42Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (StepType.Code = 'T40' or StepType.Code = 'T41' or StepType.Code = 'T42')", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;Column1;Column2;Column3;Column4;", Context = "DetailView")]
    [Appearance("ShowT51Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and StepType.Code = 'T51'", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;Column1;Column2;Column3;Column4;Column5;", Context = "DetailView")]
    [Appearance("ShowT60Fields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and StepType.Code = 'T60'", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "*;StepType;Sequence;PrintSequence;HideIfSequence;HideIfValue;Column1;Column2;Column3;Column4;Column5;Column6;", Context = "DetailView")]
    [Appearance("HideTargetSequenceField", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and TargetType <> 3 and (StepType.Code = 'A01' or StepType.Code = 'A02' or StepType.Code = 'A03' or StepType.Code = 'A04')", Visibility = ViewItemVisibility.Hide,
        TargetItems = "TargetSequence", Context = "DetailView")]
    [Appearance("HideVarianceSeqField", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and VarianceType < 5 and (StepType.Code = 'A01' or StepType.Code = 'A02' or StepType.Code = 'A03' or StepType.Code = 'A04' or StepType.Code = 'A05')", Visibility = ViewItemVisibility.Hide,
        TargetItems = "VarianceSeq", Context = "DetailView")]
    [Appearance("HideLoHiToleranceFields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (VarianceType = 0 or VarianceType = 1 or VarianceType = 5 or VarianceType = 6) and (StepType.Code = 'A01' or StepType.Code = 'A02' or StepType.Code = 'A03' or StepType.Code = 'A04' or StepType.Code = 'A05')", Visibility = ViewItemVisibility.Hide,
        TargetItems = "LoTolerance;HiTolerance", Context = "DetailView")]
    [Appearance("HideLoHiPercentageFields", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and (VarianceType = 0 or VarianceType = 2 or VarianceType = 5) and (StepType.Code = 'A01' or StepType.Code = 'A02' or StepType.Code = 'A03' or StepType.Code = 'A04' or StepType.Code = 'A05')", Visibility = ViewItemVisibility.Hide,
        TargetItems = "LoPercentage;HiPercentage", Context = "DetailView")]
    [Appearance("HideTargetValueField", AppearanceItemType.ViewItem,
        "!IsNull(StepType) and TargetType <> 1 and (StepType.Code = 'A01' or StepType.Code = 'A02' or StepType.Code = 'A03' or StepType.Code = 'A04')", Visibility = ViewItemVisibility.Hide,
        TargetItems = "TargetValue", Context = "DetailView")]
    public class MaintenanceStep : BaseObject
    {
        private Block _fBlock;

        [Size(1)]
        [Association(@"MaintenanceStepReferencesBlock")]
        [Browsable(false)]
        public Block Block
        {
            get => _fBlock;
            set => SetPropertyValue(nameof(Block), ref _fBlock, value);
        }

        private MaintenancePlan _fPlan;
        [Association(@"MaintenanceStepReferencesMaintenancePlan"), Browsable(false)]
        public MaintenancePlan Plan
        {
            get => _fPlan;
            set => SetPropertyValue(nameof(Plan), ref _fPlan, value);
        }

        StepType _fStepType;
        [Association(@"MaintenanceStepReferencesStepType"), ImmediatePostData(true)]
        public StepType StepType
        {
            get => _fStepType;
            set
            {
                SetPropertyValue(nameof(StepType), ref _fStepType, value);
                if (!IsLoading)
                {
                    TargetType = TargetType.DefaultValue;
                }
            }
        }

        int _fSequence;
        public int Sequence
        {
            get => _fSequence;
            set => SetPropertyValue(nameof(Sequence), ref _fSequence, value);
        }

        int? _fPrintSequence;
        public int? PrintSequence
        {
            get => _fPrintSequence;
            set => SetPropertyValue(nameof(PrintSequence), ref _fPrintSequence, value);
        }

        int? _fHideIfSequence;
        public int? HideIfSequence
        {
            get => _fHideIfSequence;
            set => SetPropertyValue(nameof(HideIfSequence), ref _fHideIfSequence, value);
        }

        OkType _fHideIfValue;
        public OkType HideIfValue
        {
            get => _fHideIfValue;
            set => SetPropertyValue(nameof(HideIfValue), ref _fHideIfValue, value);
        }

        string _fInternalText;
        [Size(SizeAttribute.Unlimited)]
        public string InternalText
        {
            get => _fInternalText;
            set => SetPropertyValue(nameof(InternalText), ref _fInternalText, value);
        }

        string _fValueInputs;
        [Size(255)]
        public string ValueInputs
        {
            get => _fValueInputs;
            set => SetPropertyValue(nameof(ValueInputs), ref _fValueInputs, value);
        }

        TargetType _fTargetType;
        [ImmediatePostData(true)]
        public TargetType TargetType
        {
            get => _fTargetType;
            set => SetPropertyValue(nameof(TargetType), ref _fTargetType, value);
        }

        double _fTargetValue; //TODO:StepId?
        public double TargetValue
        {
            get => _fTargetValue;
            set => SetPropertyValue(nameof(TargetValue), ref _fTargetValue, value);
        }

        VarianceType _fVarianceType = VarianceType.DefaultValue;
        [ImmediatePostData(true)]
        public VarianceType VarianceType
        {
            get => _fVarianceType;
            set => SetPropertyValue(nameof(VarianceType), ref _fVarianceType, value);
        }

        double _fLoPercentage; //TODO:StepId?
        public double LoPercentage
        {
            get => _fLoPercentage;
            set => SetPropertyValue(nameof(LoPercentage), ref _fLoPercentage, value);
        }

        double _fLoTolerance; //TODO:StepId?
        [VisibleInDetailView(false)]
        public double LoTolerance
        {
            get => _fLoTolerance;
            set => SetPropertyValue(nameof(LoTolerance), ref _fLoTolerance, value);
        }

        double _fHiPercentage; //TODO:StepId?
        public double HiPercentage
        {
            get => _fHiPercentage;
            set => SetPropertyValue(nameof(HiPercentage), ref _fHiPercentage, value);
        }

        double _fHiTolerance; //TODO:StepId?
        public double HiTolerance
        {
            get => _fHiTolerance;
            set => SetPropertyValue(nameof(HiTolerance), ref _fHiTolerance, value);
        }

        string _fStepDescription;
        [Size(SizeAttribute.Unlimited)]
        public string StepDescription
        {
            get => _fStepDescription;
            set => SetPropertyValue(nameof(StepDescription), ref _fStepDescription, value);
        }

        string _fColumn1;
        [Size(255)]
        public string Column1
        {
            get => _fColumn1;
            set => SetPropertyValue(nameof(Column1), ref _fColumn1, value);
        }

        string _fColumn2;
        [Size(255)]
        public string Column2
        {
            get => _fColumn2;
            set => SetPropertyValue(nameof(Column2), ref _fColumn2, value);
        }

        string _fColumn3;
        [Size(255)]
        public string Column3
        {
            get => _fColumn3;
            set => SetPropertyValue(nameof(Column3), ref _fColumn3, value);
        }

        string _fColumn4;
        [Size(255)]
        public string Column4
        {
            get => _fColumn4;
            set => SetPropertyValue(nameof(Column4), ref _fColumn4, value);
        }

        string _fColumn5;
        [Size(255)]
        public string Column5
        {
            get => _fColumn5;
            set => SetPropertyValue(nameof(Column5), ref _fColumn5, value);
        }

        string _fColumn6;
        [Size(255)]
        public string Column6
        {
            get => _fColumn6;
            set => SetPropertyValue(nameof(Column6), ref _fColumn6, value);
        }

        OkPrintStyle _fOkPrintStyle;
        public OkPrintStyle OkPrintStyle
        {
            get => _fOkPrintStyle;
            set => SetPropertyValue(nameof(OkPrintStyle), ref _fOkPrintStyle, value);
        }

        LimitPrintStyle _fLimitPrintStyle;
        public LimitPrintStyle LimitPrintStyle
        {
            get => _fLimitPrintStyle;
            set => SetPropertyValue(nameof(LimitPrintStyle), ref _fLimitPrintStyle, value);
        }

        int _fTargetSequence;
        public int TargetSequence
        {
            get => _fTargetSequence;
            set => SetPropertyValue(nameof(TargetSequence), ref _fTargetSequence, value);
        }

        int _fVarianceSeq;
        [XafDisplayName("Variance Sequence")]
        public int VarianceSeq
        {
            get => _fVarianceSeq;
            set => SetPropertyValue(nameof(VarianceSeq), ref _fVarianceSeq, value);
        }

        public MaintenanceStep(Session session) : base(session)
        {
        }
    }
}
