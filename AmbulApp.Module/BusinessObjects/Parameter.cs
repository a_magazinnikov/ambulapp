﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Other settings")]
    public class Parameter : BaseObject
    {
        string _fSendEmailAccessParameters;
        [Size(255)]
        public string SendEmailAccessParameters
        {
            get => _fSendEmailAccessParameters;
            set => SetPropertyValue<string>(nameof(SendEmailAccessParameters), ref _fSendEmailAccessParameters, value);
        }

        public Parameter(Session session) : base(session)
        {
        }
    }
}
