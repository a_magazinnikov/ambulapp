﻿using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [ImageName("BO_FileAttachment"), XafDefaultProperty("FileName")]
    public class OperatorManual : FileAttachmentBase
    {
         private Equipment _fEquipment;

        [Association(@"OperatorManualReferencesEquipment"), DataSourceCriteria("Company.Oid = '@This.Company.Oid'")]
        public Equipment Equipment
        {
            get => _fEquipment;
            set => SetPropertyValue(nameof(Equipment), ref _fEquipment, value);
        }

        public OperatorManual(Session session) : base(session)
        {
        }
    }
}
