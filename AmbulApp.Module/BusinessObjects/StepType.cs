﻿using AmbulApp.Module.BusinessLogic;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [XafDefaultProperty("DisplayName")]
    public class StepType : BaseObject
    {
        private string _fCode;
        [Size(5)]
        public string Code
        {
            get => _fCode;
            set => SetPropertyValue(nameof(Code), ref _fCode, value);
        }

        private StepTypeGroup _fType;
        public StepTypeGroup Type
        {
            get => _fType;
            set => SetPropertyValue(nameof(Type), ref _fType, value);
        }

        private string _fDescription;
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get => _fDescription;
            set => SetPropertyValue(nameof(Description), ref _fDescription, value);
        }

        [NonPersistent]
        public string DisplayName => $"{Code}-{Description}";

        [Association(@"MaintenanceStepReferencesStepType")]
        public XPCollection<MaintenanceStep> MaintenanceSteps => GetCollection<MaintenanceStep>(nameof(MaintenanceSteps));

        public StepType(Session session) : base(session)
        {
        }
    }
}
