﻿using System.Linq;
using AmbulApp.Module.BusinessLogic;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("User settings")]
    [XafDisplayName("User"), XafDefaultProperty("DisplayName")]
    [Appearance("AmbulUser_DisableSuperAdmin", AppearanceItemType.ViewItem, "Technician == true Or !IsCurrentUserInRole('Super Admin Role')", Enabled = false,
        TargetItems = "SuperAdmin")]
    [Appearance("AmbulUser_DisableTechnician", AppearanceItemType.ViewItem, "SuperAdmin == true", Enabled = false,
        TargetItems = "Technician")]
    [Appearance("HideTechnicianSignature", AppearanceItemType.ViewItem, "Technician == false", Visibility = ViewItemVisibility.ShowEmptySpace, TargetItems = "Signature", Context = "DetailView")]
    public class AmbulUser : PermissionPolicyUser
    {
        [VisibleInDetailView(false), VisibleInListView(false), VisibleInLookupListView(true)]
        public string DisplayName =>
            string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName) || string.IsNullOrEmpty(EmailAddress)
                ? string.Empty
                : $"{FirstName} {LastName} ({EmailAddress})";

        private string _fEmailAddress;
        [Size(255), ImmediatePostData]
        [RuleRequiredField("RuleRequiredField for EmailAddress", DefaultContexts.Save)]
        [RuleRegularExpression("UserEmailIsNotValid", DefaultContexts.Save, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", CustomMessageTemplate = "Email is not valid")]
        [RuleUniqueValue]
        public string EmailAddress
        {
            get => _fEmailAddress;
            set => SetPropertyValue(nameof(EmailAddress), ref _fEmailAddress, value);
        }

        string _fFirstName;
        [Size(255)]
        [RuleRequiredField("RuleRequiredField for FirstName", DefaultContexts.Save)]
        public string FirstName
        {
            get => _fFirstName;
            set => SetPropertyValue(nameof(FirstName), ref _fFirstName, value);
        }

        string _fLastName;
        [Size(255)]
        [RuleRequiredField("RuleRequiredField for LastName", DefaultContexts.Save)]
        public string LastName
        {
            get => _fLastName;
            set => SetPropertyValue(nameof(LastName), ref _fLastName, value);
        }

        Location _fLocation;
        public Location Location
        {
            get => _fLocation;
            set => SetPropertyValue(nameof(Location), ref _fLocation, value);
        }

        byte[] _fSignature;
        [Size(SizeAttribute.Unlimited)]
        public byte[] Signature
        {
            get => _fSignature;
            set => SetPropertyValue(nameof(Signature), ref _fSignature, value);
        }

        bool _fSuperAdmin;
        [ImmediatePostData]
        public bool SuperAdmin
        {
            get => _fSuperAdmin;
            set => SetPropertyValue(nameof(SuperAdmin), ref _fSuperAdmin, value);
        }

        bool _fTechnician;
        [ImmediatePostData]
        public bool Technician
        {
            get => _fTechnician;
            set => SetPropertyValue(nameof(Technician), ref _fTechnician, value);
        }

        string _fTelephoneNumber;
        [Size(255)]
        public string TelephoneNumber
        {
            get => _fTelephoneNumber;
            set => SetPropertyValue(nameof(TelephoneNumber), ref _fTelephoneNumber, value);
        }

        [Association(@"ReadedManualReferencesUser")]
        public XPCollection<ReadedManual> ReadedManuals => GetCollection<ReadedManual>(nameof(ReadedManuals));

        [Association(@"Users-Companies")]
        [RuleRequiredField]
        public XPCollection<Company> Companies => GetCollection<Company>("Companies");

        [VisibleInLookupListView(true), VisibleInDetailView(false), VisibleInListView(true)]
        [NonPersistent]
        public string Company
        {
            get { return string.Join(",", Companies.ToList().Select(c => c.Title).ToList()); }
        }

        public AmbulUser(Session session) : base(session)
        {
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);
            if (!IsDeleted && !IsLoading)
            {
                switch (propertyName)
                {
//                    case "SuperAdmin":
//                    {
//                        var superAdminRole =
//                            Session.FindObject<PermissionPolicyRole>(new BinaryOperator("Name", "Super Admin Role"));
//                        bool val = (bool) newValue;
//                        if (val)
//                        {
//                            Roles.Add(superAdminRole);
//                        }
//                        else
//                        {
//                            Roles.Remove(superAdminRole);
//                        }
//
//                        break;
//                    }
                    case "EmailAddress":
                    {
                        if (oldValue != newValue)
                        {
                            UserName = (string) newValue;
                        }

                        break;
                    }
                }
            }
        }

        protected override void OnSaving()
        {
            if (Companies.Count != 1 && (Technician || SuperAdmin))
            {
                throw new UserFriendlyException("Technician and SuperAdmin need to have only one linked Company.");
            }

            base.OnSaving();
        }
    }
}
