﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NonPersistent]
    public class SendReportParams : XPLiteObject
    {
        public SendReportParams(Session session) : base(session)
        {
        }

        private String _email;

        public String Email
        {
            get => _email;
            set => SetPropertyValue("Email", ref _email, value);
        }

//        private XPCollection<FileData> _files;
//        [CollectionOperationSet(AllowAdd = false, AllowRemove = false)]
//        public XPCollection<FileData> Files => _files ?? (_files = new XPCollection<FileData>(Session));
    }
}
