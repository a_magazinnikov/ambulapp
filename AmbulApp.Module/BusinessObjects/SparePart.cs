﻿using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Other settings")]
    public class SparePart : BaseObject
    {
        private SparePartCategory _fCategory;
        [Association(@"SparePartReferencesSparePartCategory")]
        public SparePartCategory Category
        {
            get => _fCategory;
            set => SetPropertyValue(nameof(Category), ref _fCategory, value);
        }

        string _fPartCode;
        [Indexed(Name = @"Codice")]
        [Size(255)]
        public string PartCode
        {
            get => _fPartCode;
            set => SetPropertyValue<string>(nameof(PartCode), ref _fPartCode, value);
        }

        string _fDescription;
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get => _fDescription;
            set => SetPropertyValue<string>(nameof(Description), ref _fDescription, value);
        }

        decimal _fSalesPrice;
        [ModelDefault("DisplayFormat", "{0:C}"), ModelDefault("EditFormat", "n2")]
        public decimal SalesPrice
        {
            get => _fSalesPrice;
            set => SetPropertyValue<decimal>(nameof(SalesPrice), ref _fSalesPrice, value);
        }

        public SparePart(Session session) : base(session)
        {
        }
    }
}
