﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [ModelDefault("AllowNew", "false"), ModelDefault("AllowDelete", "false")]
    public class ChangeMyCompany : BaseObject
    {
        private Company _company;
        [DataSourceProperty(nameof(AvailableCompanies), DataSourcePropertyIsNullMode.SelectNothing), RuleRequiredField]
        public Company Company
        {
            get => _company;
            set => SetPropertyValue(nameof(Company), ref _company, value);
        }

        [Browsable(false), NonPersistent]
        private IList<Company> AvailableCompanies
        {
            get
            {
                var user = Session.GetObjectByKey<AmbulUser>(SecuritySystem.CurrentUserId);
                return user.Companies;
            }
        }

  
        public ChangeMyCompany(Session session) : base(session)
        {
        }
    }
}
