﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.DC;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [XafDefaultProperty("Value")]
    public class FamilyType: XPLiteObject
    {
        public FamilyType(Session session) : base(session) { }

        [Key]
        public string Key { get; set; }

        public string Value
        {
            get => GetPropertyValue<string>("Value");
            set => SetPropertyValue("Value", value);
        }
    }
}
