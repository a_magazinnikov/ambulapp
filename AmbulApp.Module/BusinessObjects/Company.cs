﻿using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("User settings"), XafDefaultProperty("Title")]
    public class Company : BaseObject
    {
        private string _fTitle;
        [Size(255)]
        public string Title
        {
            get => _fTitle;
            set => SetPropertyValue("Title", ref _fTitle, value);
        }

        string _fEmailAddress;
        [Size(255)]
        [RuleRegularExpression("CompanyEmailIsNotValid", DefaultContexts.Save, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", CustomMessageTemplate = "Email is not valid")]
        public string EmailAddress
        {
            get => _fEmailAddress;
            set => SetPropertyValue(nameof(EmailAddress), ref _fEmailAddress, value);
        }

        AmbulUser _fEquipmentResponsibleUser;
        [DataSourceProperty("Users")]
        public AmbulUser EquipmentResponsibleUser
        {
            get => _fEquipmentResponsibleUser;
            set => SetPropertyValue(nameof(EquipmentResponsibleUser), ref _fEquipmentResponsibleUser, value);
        }

        string _fFiscalCode;
        [Size(255)]
        public string FiscalCode
        {
            get => _fFiscalCode;
            set => SetPropertyValue(nameof(FiscalCode), ref _fFiscalCode, value);
        }

        private bool _fInstaller;
        public bool Installer
        {
            get => _fInstaller;
            set => SetPropertyValue(nameof(Installer), ref _fInstaller, value);
        }

        Location _fLegalLocation;
        [DataSourceProperty("Locations")]
        public Location LegalLocation
        {
            get => _fLegalLocation;
            set => SetPropertyValue(nameof(LegalLocation), ref _fLegalLocation, value);
        }

        AmbulUser _fPresidentUser;
        [DataSourceProperty("Users")]
        public AmbulUser PresidentUser
        {
            get => _fPresidentUser;
            set => SetPropertyValue(nameof(PresidentUser), ref _fPresidentUser, value);
        }

        AmbulUser _fReferentUser;
        [DataSourceProperty("Users")]
        public AmbulUser ReferentUser
        {
            get => _fReferentUser;
            set => SetPropertyValue(nameof(ReferentUser), ref _fReferentUser, value);
        }

        AmbulUser _fShiftResponsibleUser;
        [DataSourceProperty("Users")]
        public AmbulUser ShiftResponsibleUser
        {
            get => _fShiftResponsibleUser;
            set => SetPropertyValue(nameof(ShiftResponsibleUser), ref _fShiftResponsibleUser, value);
        }

        string _fVatNumber;
        [Size(255)]
        // ReSharper disable once InconsistentNaming
        public string VATNumber
        {
            get => _fVatNumber;
            set => SetPropertyValue(nameof(VATNumber), ref _fVatNumber, value);
        }

        AmbulUser _fVehicleResponsibleUser;

        [DataSourceProperty("Users")]
        public AmbulUser VehicleResponsibleUser
        {
            get => _fVehicleResponsibleUser;
            set => SetPropertyValue(nameof(VehicleResponsibleUser), ref _fVehicleResponsibleUser, value);
        }

        [Association(@"EquipmentReferencesCompany")]
        public XPCollection<Equipment> Equipments => GetCollection<Equipment>(nameof(Equipments));

        [Association(@"LocationReferencesCompany"), DevExpress.Xpo.Aggregated]
        public XPCollection<Location> Locations => GetCollection<Location>(nameof(Locations));

        [Association(@"VisitReferencesCompany")]
        public XPCollection<Visit> Visits => GetCollection<Visit>(nameof(Visits));

        //        [Association(@"UserReferencesCompany")]
        //        public XPCollection<AmbulUser> Users => GetCollection<AmbulUser>("Users");

        [Association(@"Users-Companies")]
        public XPCollection<AmbulUser> Users => GetCollection<AmbulUser>("Users");

        public Company(Session session) : base(session)
        {
        }
    }
}
