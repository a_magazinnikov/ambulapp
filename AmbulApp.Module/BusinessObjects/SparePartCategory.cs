﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [XafDefaultProperty("Name")]
    public class SparePartCategory : BaseObject
    {
        string _fName;
        [Size(255)]
        public string Name
        {
            get => _fName;
            set => SetPropertyValue(nameof(Name), ref _fName, value);
        }

        private string _fDescription;
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get => _fDescription;
            set => SetPropertyValue<string>(nameof(Description), ref _fDescription, value);
        }

        [Association(@"SparePartReferencesSparePartCategory")]
        public XPCollection<SparePart> SpareParts => GetCollection<SparePart>(nameof(SpareParts));

        public SparePartCategory(Session session) : base(session)
        {
        }
    }
}
