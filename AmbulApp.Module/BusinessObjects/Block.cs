﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    public class Block : BaseObject
    {
        string _fBlockDescription;
        [Size(SizeAttribute.Unlimited)]
        public string BlockDescription
        {
            get => _fBlockDescription;
            set => SetPropertyValue<string>(nameof(BlockDescription), ref _fBlockDescription, value);
        }

        [Association(@"CheckReferencesBlock")]
        public XPCollection<Check> Checks => GetCollection<Check>(nameof(Checks));

        [Association(@"MaintenanceStepReferencesBlock")]
        public XPCollection<MaintenanceStep> MaintenanceSteps => GetCollection<MaintenanceStep>(nameof(MaintenanceSteps));

        public Block(Session session) : base(session)
        {
        }
    }
}
