﻿using System;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [XafDefaultProperty("DisplayName")]
    public class Location : BaseObject
    {
        [VisibleInDetailView(false)]
        public string DisplayName => String.IsNullOrEmpty(County) ? $"{City} - {Address}" : $"{City} ({County}) - {Address}";

        string _fAddress;
        [Size(255)]
        [RuleRequiredField("RuleRequiredField for Address", DefaultContexts.Save)]
        public string Address
        {
            get => _fAddress;
            set => SetPropertyValue(nameof(Address), ref _fAddress, value);
        }

        string _fCity;
        [Size(255)]
        [RuleRequiredField("RuleRequiredField for City", DefaultContexts.Save)]
        public string City
        {
            get => _fCity;
            set => SetPropertyValue(nameof(City), ref _fCity, value);
        }

        private Company _fCompany;
        [Association(@"LocationReferencesCompany"), ImmediatePostData, RuleRequiredField]
        public Company Company
        {
            get => _fCompany;
            set => SetPropertyValue(nameof(Company), ref _fCompany, value);
        }

        string _fCounty;
        [Size(255)]
        public string County
        {
            get => _fCounty;
            set => SetPropertyValue(nameof(County), ref _fCounty, value);
        }

        string _fZipCode;
        [Size(255)]
        public string ZipCode
        {
            get => _fZipCode;
            set => SetPropertyValue(nameof(ZipCode), ref _fZipCode, value);
        }

        AmbulUser _fReferenceUser;
//        [Indexed(Name = @"IDReferente")]
        [DataSourceProperty("Company.Users")]
        public AmbulUser ReferenceUser
        {
            get => _fReferenceUser;
            set => SetPropertyValue(nameof(ReferenceUser), ref _fReferenceUser, value);
        }

        string _fTelephone;
        [Size(255)]
        public string Telephone
        {
            get => _fTelephone;
            set => SetPropertyValue(nameof(Telephone), ref _fTelephone, value);
        }

        [Association(@"EquipmentReferencesLocation")]
        public XPCollection<Equipment> Equipments => GetCollection<Equipment>(nameof(Equipments));

        [Association(@"VisitReferencesLocation")]
        public XPCollection<Visit> Visits => GetCollection<Visit>(nameof(Visits));

        public Location(Session session) : base(session)
        {
        }
    }
}
