﻿using System;
using System.ComponentModel;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Interventions")]
    [DefaultProperty("ExecutionDate")]
    public class Intervention : BaseObject
    {
        private Equipment _fEquipment;
        [Association(@"InterventionReferencesEquipment"), ImmediatePostData]
        public Equipment Equipment
        {
            get => _fEquipment;
            set
            {
                SetPropertyValue(nameof(Equipment), ref _fEquipment, value);
                if (!IsLoading)
                {
                    MaintenancePlan = null;
                }
            }
        }

        DateTime _fExecutionDate;

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime ExecutionDate
        {
            get => _fExecutionDate;
            set => SetPropertyValue<DateTime>(nameof(ExecutionDate), ref _fExecutionDate, value);
        }

        FileData _fLinkToAttachment;
        [VisibleInListView(true)]
        public FileData LinkToAttachment
        {
            get => _fLinkToAttachment;
            set => SetPropertyValue(nameof(LinkToAttachment), ref _fLinkToAttachment, value);
        }

        private MaintenancePlan _fMaintenancePlan;
        [Association(@"InterventionReferencesMaintenancePlan"), ImmediatePostData, DataSourceCriteria("Family.Oid = '@This.Equipment.Family.Oid'")]
        public MaintenancePlan MaintenancePlan
        {
            get => _fMaintenancePlan;
            set => SetPropertyValue(nameof(MaintenancePlan), ref _fMaintenancePlan, value);
        }

        int _fReadCounter;
        public int ReadCounter
        {
            get => _fReadCounter;
            set => SetPropertyValue<int>(nameof(ReadCounter), ref _fReadCounter, value);
        }

        private Visit _fVisit;
        [Association(@"InterventionReferencesVisit")]
        public Visit Visit
        {
            get => _fVisit;
            set => SetPropertyValue(nameof(Visit), ref _fVisit, value);
        }

        byte[] _fSignature;
        [Size(SizeAttribute.Unlimited)]
        public byte[] Signature
        {
            get => _fSignature;
            set => SetPropertyValue(nameof(Signature), ref _fSignature, value);
        }

        [Association(@"CheckReferencesIntervention")]
        public XPCollection<Check> Checks => GetCollection<Check>(nameof(Checks));

        public Intervention(Session session) : base(session)
        {
        }
    }
}
