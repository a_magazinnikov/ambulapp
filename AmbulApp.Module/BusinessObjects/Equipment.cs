﻿using System;
using System.ComponentModel;
using System.Linq;
using AmbulApp.Module.BusinessLogic;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.XtraSpellChecker.Parser;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Equipment list"), XafDefaultProperty("DisplayName")]
    [Appearance("EquipmentVenicle_Visible", AppearanceItemType.ViewItem, "!IsNull(Family) and Family.Type.Key != 'E'",
            Visibility = ViewItemVisibility.ShowEmptySpace, TargetItems = "Vehicle")]
    [Appearance("Equipment_RadioCode_Installer_CompanyInstaller_Visible", AppearanceItemType.ViewItem,
        "!IsNull(Family) and Family.Type.Key != 'V'", Visibility = ViewItemVisibility.ShowEmptySpace,
        TargetItems = "RadioCode;Installer;CompanyInstaller")]
    [Appearance("Equipment_Plate_Enable", AppearanceItemType.ViewItem, "!IsNull(Family) and Family.Type.Key != 'V'", Enabled = false, TargetItems = "Plate")]
    [Appearance("Equipments_Visible", AppearanceItemType.ViewItem, "!IsNull(Family) and Family.Type.Key != 'V'",
        Visibility = ViewItemVisibility.ShowEmptySpace, TargetItems = "Equipments")]
    [Appearance("LastInterventionDate_Visible", AppearanceItemType.ViewItem, "!IsNull(Family) and Family.Type.Key != 'A'",
        Visibility = ViewItemVisibility.ShowEmptySpace, TargetItems = "LastInterventionDate")]
    [RuleCombinationOfPropertiesIsUnique("RuleUniqueQrCodeSerialNumberPlate", DefaultContexts.Save, "QrCode, SerialNumber, Plate")]
    public class Equipment : BaseObject
    {
        string familyTypeShorcut;
        int _fBuiltYear;

        public int BuiltYear
        {
            get => _fBuiltYear;
            set => SetPropertyValue<int>(nameof(BuiltYear), ref _fBuiltYear, value);
        }

        private Company _fCompany;

        [Association(@"EquipmentReferencesCompany"), RuleRequiredField, ImmediatePostData]
        public Company Company
        {
            get => _fCompany;
            set
            {
                SetPropertyValue(nameof(Company), ref _fCompany, value);
                RefreshAvailableOperatorManuals();
                if (!IsLoading && !IsDeleted)
                {
                    if (!Company.Locations.Contains(this.Location))
                    {
                        this.Location = null;
                    }

                    CurrentOperatorManual = null;
                    OperatorManuals.RemoveAll();
                    Interventions.RemoveAll();
                }
            }
        }

        string _fCompanyInstaller;

        [Size(255)]
        public string CompanyInstaller
        {
            get => _fCompanyInstaller;
            set => SetPropertyValue(nameof(CompanyInstaller), ref _fCompanyInstaller, value);
        }

        OperatorManual _fCurrentOperatorManual;

        [DataSourceProperty("OperatorManuals", DataSourcePropertyIsNullMode.SelectNothing)]
        public OperatorManual CurrentOperatorManual
        {
            get => _fCurrentOperatorManual;
            set => SetPropertyValue(nameof(CurrentOperatorManual), ref _fCurrentOperatorManual, value);
        }

        bool _fDismissed;

        public bool Dismissed
        {
            get => _fDismissed;
            set => SetPropertyValue(nameof(Dismissed), ref _fDismissed, value);
        }

        private Family _fFamily;

        [Association(@"EquipmentReferencesFamily"), ImmediatePostData, VisibleInListView(true),
         VisibleInLookupListView(true)]
        public Family Family
        {
            get => _fFamily;
            set
            {
                SetPropertyValue(nameof(Family), ref _fFamily, value);
                if (!IsLoading)
                {
                    Brand = null;
                    Model = null;
                }
            }
        }

        private Brand _fBrand;

        [Association(@"EquipmentReferencesBrand"), VisibleInListView(true), VisibleInLookupListView(true), ImmediatePostData]
        [DataSourceCriteria("[Models][Family.Oid = '@This.Family.Oid']")]
        public Brand Brand
        {
            get => _fBrand;
            set
            {
                SetPropertyValue(nameof(Brand), ref _fBrand, value);
                if (!IsLoading)
                {
                    Model = null;
                }
            }
        }

        private Model _fModel;

        [Association(@"EquipmentReferencesModel"), VisibleInListView(true), VisibleInLookupListView(true)]
        [DataSourceCriteria("Family.Oid = '@This.Family.Oid' and Brand.Oid = '@This.Brand.Oid'")]
        public Model Model
        {
            get => _fModel;
            set => SetPropertyValue(nameof(Model), ref _fModel, value);
        }

        Company _fInstaller;
        [DataSourceCriteria("Installer = true")]
        public Company Installer
        {
            get => _fInstaller;
            set => SetPropertyValue(nameof(Installer), ref _fInstaller, value);
        }

        string _fInventoryNumber;

        [Size(255)]
        public string InventoryNumber
        {
            get => _fInventoryNumber;
            set => SetPropertyValue(nameof(InventoryNumber), ref _fInventoryNumber, value);
        }

        [Size(SizeAttribute.Unlimited), ImageEditor, VisibleInListView(false), VisibleInDetailView(true), VisibleInLookupListView(false)]
        public byte[] LinkToPicture
        {
            get => GetPropertyValue<byte[]>("LinkToPicture");
            set => SetPropertyValue("LinkToPicture", value);
        }

        private Location _fLocation;

        [Association(@"EquipmentReferencesLocation")]
        [VisibleInListView(true), DataSourceProperty("Company.Locations", DataSourcePropertyIsNullMode.SelectNothing)]
        public Location Location
        {
            get => _fLocation;
            set => SetPropertyValue(nameof(Location), ref _fLocation, value);
        }

        string _fPlate;

        [Size(255), VisibleInLookupListView(true)]
        public string Plate
        {
            get => _fPlate;
            set => SetPropertyValue(nameof(Plate), ref _fPlate, value);
        }

        DateTime _fPurchaseInvoiceDate;

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime PurchaseInvoiceDate
        {
            get => _fPurchaseInvoiceDate;
            set => SetPropertyValue<DateTime>(nameof(PurchaseInvoiceDate), ref _fPurchaseInvoiceDate, value);
        }

        string _fPurchaseInvoiceNumber;

        [Size(255)]
        public string PurchaseInvoiceNumber
        {
            get => _fPurchaseInvoiceNumber;
            set => SetPropertyValue(nameof(PurchaseInvoiceNumber), ref _fPurchaseInvoiceNumber, value);
        }

        int _fQrCode;

        [VisibleInLookupListView(true)]
        public int QrCode
        {
            get => _fQrCode;
            set => SetPropertyValue<int>(nameof(QrCode), ref _fQrCode, value);
        }

        string _fRadioCode;

        [Indexed(Name = @"CodiceRadio")]
        [Size(255)]
        public string RadioCode
        {
            get => _fRadioCode;
            set => SetPropertyValue(nameof(RadioCode), ref _fRadioCode, value);
        }

        string _fSerialNumber;

        [Size(255)]
        [VisibleInListView(true), VisibleInLookupListView(true)]
        public string SerialNumber
        {
            get => _fSerialNumber;
            set => SetPropertyValue(nameof(SerialNumber), ref _fSerialNumber, value);
        }

        string _fSupplier;

        [Size(255)]
        public string Supplier
        {
            get => _fSupplier;
            set => SetPropertyValue(nameof(Supplier), ref _fSupplier, value);
        }

        Equipment _fVehicle;

        [Association(@"VehicleEquipment-Equipments"), DataSourceCriteria("Family.Type.Key = 'V' And Company.Oid = '@This.Company.Oid'"), ImmediatePostData]
        public Equipment Vehicle
        {
            get => _fVehicle;
            set => SetPropertyValue(nameof(Vehicle), ref _fVehicle, value);
        }

        [Association(@"Equipment-Visit"), Browsable(false)]
        public XPCollection<Visit> Visits =>
            GetCollection<Visit>(nameof(Visits));

        [Association(@"InterventionReferencesEquipment")]
        public XPCollection<Intervention> Interventions => GetCollection<Intervention>(nameof(Interventions));

        [Association(@"EquipmentReferencesExternalAccessories"), DevExpress.Xpo.Aggregated]
        public XPCollection<ExternalAccessory> ExternalAccessories => GetCollection<ExternalAccessory>(nameof(ExternalAccessories));

        [Association(@"EquipmentReferencesInternalModules"), DevExpress.Xpo.Aggregated]
        public XPCollection<InternalModule> InternalModules => GetCollection<InternalModule>(nameof(InternalModules));

        [Association(@"OperatorManualReferencesEquipment")]
        [DataSourceProperty("AvailableOperatorManuals")]
        public XPCollection<OperatorManual> OperatorManuals => GetCollection<OperatorManual>(nameof(OperatorManuals));

        private XPCollection<OperatorManual> _availableOperatorManuals;

        [Browsable(false)]
        public XPCollection<OperatorManual> AvailableOperatorManuals
        {
            get
            {
                if (_availableOperatorManuals == null)
                {
                    _availableOperatorManuals = new XPCollection<OperatorManual>(Session);
                }

                RefreshAvailableOperatorManuals();
                return _availableOperatorManuals;
            }
        }

        private void RefreshAvailableOperatorManuals()
        {
            if (_availableOperatorManuals == null)
                return;

            if (Company != null && Company.Equipments.Any())
            {
                _availableOperatorManuals.Criteria =
                    CriteriaOperator.Parse("Equipment.Company is not null and Equipment.Company.Oid = ?", Company.Oid);
            }
            else
            {
                _availableOperatorManuals.Criteria = null;
            }
        }

        [Association(@"VehicleEquipment-Equipments")]
        [DataSourceProperty(nameof(AvailableEquipments))]
        public XPCollection<Equipment> Equipments => GetCollection<Equipment>(nameof(Equipments));

        private XPCollection<Equipment> _availableEquipments;

        [Browsable(false)]
        public XPCollection<Equipment> AvailableEquipments
        {
            get
            {
                if (_availableEquipments == null)
                {
                    _availableEquipments = new XPCollection<Equipment>(Session);
                }

                RefreshAvailableEquipments();
                return _availableEquipments;
            }
        }

        private void RefreshAvailableEquipments()
        {
            if (_availableEquipments == null)
                return;

            if (Company != null && Company.Equipments.Any())
            {
                _availableEquipments.Criteria =
                    CriteriaOperator.Parse("Company is not null and Family is not null and Family.Type.Key = 'E' and Company.Oid = ?", Company.Oid);
            }
            else
            {
                _availableEquipments.Criteria = null;
            }
        }

        [VisibleInDetailView(false)]
        [VisibleInLookupListView(false)]
        [Size(1)]
        public string FamilyTypeShorcut
        {
            get => familyTypeShorcut;
            set => SetPropertyValue(nameof(FamilyTypeShorcut), ref familyTypeShorcut, value);
        }

        DateTime _fLastInterventionDate;

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime LastInterventionDate
        {
            get => _fLastInterventionDate;
            set => SetPropertyValue<DateTime>(nameof(LastInterventionDate), ref _fLastInterventionDate, value);
        }

        [NonPersistent, Browsable(false)]
        public string DisplayName => $"{Brand?.Name} {Model?.Name} QR:{QrCode}";

        public Equipment(Session session) : base(session)
        {
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (this.Family==null)
            {
                FamilyTypeShorcut = "";
                return;
            }

            if (this.Family.Type==null)
            {
                FamilyTypeShorcut = "";
                return;
            }
            if (this.Family.Type.Key=="E")
            {
                FamilyTypeShorcut = "E";
                return;
            }
            if (this.Family.Type.Key == "V")
            {
                FamilyTypeShorcut = "V";
                return;
            }
            if (this.Family.Type.Key == "A")
            {
                FamilyTypeShorcut = "A";
                return;
            }
        }
        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);
            if (!IsDeleted && !IsLoading)
            {
                switch (propertyName)
                {
                    case "Family":
                        {
                            if (newValue is Family)
                            {
                                Vehicle = null;
                                Plate = string.Empty;
                            }

                            break;
                        }

                    case "Vehicle":
                        {
                            if (newValue is Equipment vehicle)
                            {
                                if (Family != null && Family.Type.Key == "E")
                                {
                                    Plate = vehicle.Plate;
                                   
                                }
                                else
                                {
                                    Plate = string.Empty;
                                }
                            }
                            else
                            {
                                Plate = string.Empty;
                            }

                            break;
                        }
                }
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            Session.Delete(OperatorManuals);
            Session.Delete(Interventions);
        }
    }
}
