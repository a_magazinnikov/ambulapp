﻿using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Equipment settings"), XafDefaultProperty("Name")]
    public class Model : BaseObject
    {
        private Brand _fBrand;
        [Association(@"ModelReferencesBrand")]
        public Brand Brand
        {
            get => _fBrand;
            set => SetPropertyValue(nameof(Brand), ref _fBrand, value);
        }

        private Family _fFamily;
        [Association(@"ModelReferencesFamily")]
        public Family Family
        {
            get => _fFamily;
            set => SetPropertyValue(nameof(Family), ref _fFamily, value);
        }

        private string _fName;
        [Size(255)]
        public string Name
        {
            get => _fName;
            set => SetPropertyValue<string>(nameof(Name), ref _fName, value);
        }
        
        decimal _fSalesPrice;
        [ModelDefault("DisplayFormat", "{0:C2}"), ModelDefault("EditFormat", "n2")]
        public decimal SalesPrice
        {
            get => _fSalesPrice;
            set => SetPropertyValue(nameof(SalesPrice), ref _fSalesPrice, value);
        }

        [Association(@"EquipmentReferencesModel")]
        public XPCollection<Equipment> Equipments => GetCollection<Equipment>(nameof(Equipments));

        [Association(@"ModelAttachmentReferencesModel")]
        public XPCollection<ModelAttachment> ModelAttachments => GetCollection<ModelAttachment>(nameof(ModelAttachments));

        public Model(Session session) : base(session)
        {
        }
    }
}
