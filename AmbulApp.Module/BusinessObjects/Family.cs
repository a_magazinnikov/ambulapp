﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Equipment settings"), XafDefaultProperty("Name")]
    public class Family : BaseObject
    {
        private string _fName;
        [Size(255)]
        public string Name
        {
            get => _fName;
            set => SetPropertyValue(nameof(Name), ref _fName, value);
        }

        FamilyType _fType;
        public FamilyType Type
        {
            get => _fType;
            set => SetPropertyValue(nameof(Type), ref _fType, value);
        }

        [Association(@"AccessoryTypeReferencesFamily")]
        public XPCollection<AccessoryType> AccessoryTypes => GetCollection<AccessoryType>(nameof(AccessoryTypes));

        [Association(@"EquipmentReferencesFamily")]
        public XPCollection<Equipment> Equipments => GetCollection<Equipment>(nameof(Equipments));

        [Association(@"MaintenancePlanReferencesFamily")]
        public XPCollection<MaintenancePlan> MaintenancePlans => GetCollection<MaintenancePlan>(nameof(MaintenancePlans));

        [Association(@"ModelReferencesFamily")]
        public XPCollection<Model> Models => GetCollection<Model>(nameof(Models));

        public Family(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Type = Session.FindObject<FamilyType>(CriteriaOperator.Parse(
                "Key = 'E'"));
        }
    }
}
