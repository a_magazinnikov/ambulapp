﻿using System;
using System.ComponentModel;
using AmbulApp.Module.Common;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [Persistent("EquipmentInLocation"), CreatableItem(false)]
    public class EquipmentInLocation : XPLiteObject
    {
        public EquipmentInLocation(Session session) : base(session)
        {

        }

        [Key, Persistent, Browsable(false)]
        public EquipmentInLocationViewKey Key;

        [Persistent("LocationOid"), Browsable(false)]
        public Guid LocationOid;

        [Persistent("Family")]
        public string Family { get; set; }

        [Persistent("Brand")]
        public string Brand { get; set; }

        [Persistent("Model")]
        public string Model { get; set; }

        [Persistent("QrCode")]
        public int QrCode { get; set; }

        [Persistent("Plate")]
        public string Plate { get; set; }

        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        [Persistent("DueDate")]
        public DateTime? DueDate { get; set; }

        [Persistent("EstimatedEffort")]
        public double EstimatedEffort { get; set; }

        [Persistent("Plan")]
        public string Plan { get; set; }

        [Persistent("SerialNumber")]
        public string SerialNumber { get; set; }

        [TypeConverter(typeof(LocalStructTypeConverter<EquipmentInLocation>))]
        public struct EquipmentInLocationViewKey
        {
            [Persistent("EquipmentOid"), Browsable(false)]
            public Guid EquipmentOid;

            [Persistent("MaintenancePlanOid"), Browsable(false)]
            public Guid MaintenancePlanOid;
        }
    }
}
