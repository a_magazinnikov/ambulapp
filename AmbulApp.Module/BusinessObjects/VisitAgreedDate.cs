﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    public class VisitAgreedDate : XPLiteObject
    {
        public VisitAgreedDate(Session session) : base(session)
        {
        }

        [Key(AutoGenerate = false)]
        public Guid Location { get; set; }

        public DateTime AgreedDate
        {
            get => GetPropertyValue<DateTime>("AgreedDate");
            set => SetPropertyValue<DateTime>("AgreedDate", value);
        }
    }
}
