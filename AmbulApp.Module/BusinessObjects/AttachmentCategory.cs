﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
   [XafDefaultProperty("Name"), XafDisplayName("Category")]
   public class AttachmentCategory : BaseObject
    {
        string _fName;
        [Size(255)]
        public string Name
        {
            get => _fName;
            set => SetPropertyValue(nameof(Name), ref _fName, value);
        }

        string _fDescription;
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get => _fDescription;
            set => SetPropertyValue(nameof(Description), ref _fDescription, value);
        }

        public AttachmentCategory(Session session) : base(session)
        {
        }
    }
}
