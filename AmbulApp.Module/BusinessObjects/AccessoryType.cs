﻿using System.ComponentModel;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Equipment settings")]
    [XafDefaultProperty(nameof(AccessoryType.DisplayName))]
    [Appearance("AccessoryType_CanEditInternalModule", AppearanceItemType = "ViewItem", TargetItems = "InternalModule",
        Criteria = "!IsNewObject", Context = "DetailView", Enabled = false)]
    public class AccessoryType : BaseObject
    {
        private string _fDescription;

        [Size(255)]
        public string Description
        {
            get => _fDescription;
            set => SetPropertyValue<string>(nameof(Description), ref _fDescription, value);
        }

        Brand _fBrand;

        public Brand Brand
        {
            get => _fBrand;
            set => SetPropertyValue(nameof(Brand), ref _fBrand, value);
        }

        Family _fFamily;

        [Association(@"AccessoryTypeReferencesFamily")]
        [DataSourceCriteria("Type is not Null and Type.Key <> 'A'"), ImmediatePostData]
        public Family Family
        {
            get => _fFamily;
            set => SetPropertyValue<Family>(nameof(Family), ref _fFamily, value);
        }

        bool _fInternalModule;

        public bool InternalModule
        {
            get => _fInternalModule;
            set => SetPropertyValue(nameof(InternalModule), ref _fInternalModule, value);
        }

        decimal _fSalesPrice;

        [ModelDefault("DisplayFormat", "{0:C}"), ModelDefault("EditFormat", "n2")]
        public decimal SalesPrice
        {
            get => _fSalesPrice;
            set => SetPropertyValue<decimal>(nameof(SalesPrice), ref _fSalesPrice, value);
        }

        string _fAccessoryModel;

        [Size(255)]
        public string AccessoryModel
        {
            get => _fAccessoryModel;
            set => SetPropertyValue(nameof(AccessoryModel), ref _fAccessoryModel, value);
        }

        [NonPersistent, Browsable(false)]
        public string DisplayName => string.IsNullOrEmpty(AccessoryModel)
            ? $"{Description}"
            : $"{Description} - {AccessoryModel}";

        [NonPersistent] [Browsable(false)] public bool IsNewObject => Session.IsNewObject(this);

        public AccessoryType(Session session) : base(session)
        {
        }
    }
}
