﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using AmbulApp.Module.BusinessLogic;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Interventions")]
    public class Check : BaseObject
    {
        private Block _fBlock;

        [Association(@"CheckReferencesBlock"), Size(1)]
        [Browsable(false)]
        public Block Block
        {
            get => _fBlock;
            set => SetPropertyValue(nameof(Block), ref _fBlock, value);
        }

        private Intervention _fIntervention;
        [Association(@"CheckReferencesIntervention")]
        public Intervention Intervention
        {
            get => _fIntervention;
            set => SetPropertyValue(nameof(Intervention), ref _fIntervention, value);
        }

        string _fRunTimeText;
        [Size(255)]
        public string RunTimeText
        {
            get => _fRunTimeText;
            set => SetPropertyValue(nameof(RunTimeText), ref _fRunTimeText, value);
        }

        OkType _fOk;
        // ReSharper disable once InconsistentNaming
        public OkType OK
        {
            get => _fOk;
            set => SetPropertyValue(nameof(OK), ref _fOk, value);
        }

        double _fReadValue;
        public double ReadValue
        {
            get => _fReadValue;
            set => SetPropertyValue<double>(nameof(ReadValue), ref _fReadValue, value);
        }

        bool _fHidden;
        public bool Hidden
        {
            get => _fHidden;
            set => SetPropertyValue<bool>(nameof(Hidden), ref _fHidden, value);
        }

        double _fCalculatedTargeValue; 
        public double CalculatedTargeValue
        {
            get => _fCalculatedTargeValue;
            set => SetPropertyValue(nameof(CalculatedTargeValue), ref _fCalculatedTargeValue, value);
        }

        double _fCalculatedLowPercentage;
        public double CalculatedLowPercentage
        {
            get => _fCalculatedLowPercentage;
            set => SetPropertyValue(nameof(CalculatedLowPercentage), ref _fCalculatedLowPercentage, value);
        }

        double _fCalculatedHighPercentage;
        public double CalculatedHighPercentage
        {
            get => _fCalculatedHighPercentage;
            set => SetPropertyValue(nameof(CalculatedHighPercentage), ref _fCalculatedHighPercentage, value);
        }

        double _fCalculatedLowLimit;
        public double CalculatedLowLimit
        {
            get => _fCalculatedLowLimit;
            set => SetPropertyValue(nameof(CalculatedLowLimit), ref _fCalculatedLowLimit, value);
        }

        double _fCalculatedHighLimit;
        public double CalculatedHighLimit
        {
            get => _fCalculatedHighLimit;
            set => SetPropertyValue(nameof(CalculatedHighLimit), ref _fCalculatedHighLimit, value);
        }

        #region Data from MainteanceStep

        int _fTargetSequence;
        public int TargetSequence
        {
            get => _fTargetSequence;
            set => SetPropertyValue(nameof(TargetSequence), ref _fTargetSequence, value);
        }

        int _fVarianceSeq;
        public int VarianceSeq
        {
            get => _fVarianceSeq;
            set => SetPropertyValue(nameof(VarianceSeq), ref _fVarianceSeq, value);
        }

        int _fSequence;
        public int Sequence
        {
            get => _fSequence;
            set => SetPropertyValue(nameof(Sequence), ref _fSequence, value);
        }

        int? _fPrintSequence;
        public int? PrintSequence
        {
            get => _fPrintSequence;
            set => SetPropertyValue(nameof(PrintSequence), ref _fPrintSequence, value);
        }

        int? _fHideIfSequence;
        public int? HideIfSequence
        {
            get => _fHideIfSequence;
            set => SetPropertyValue(nameof(HideIfSequence), ref _fHideIfSequence, value);
        }

        OkType _fHideIfValue;
        public OkType HideIfValue
        {
            get => _fHideIfValue;
            set => SetPropertyValue(nameof(HideIfValue), ref _fHideIfValue, value);
        }

        string _fInternalText;
        [Size(SizeAttribute.Unlimited)]
        public string InternalText
        {
            get => _fInternalText;
            set => SetPropertyValue(nameof(InternalText), ref _fInternalText, value);
        }

        string _fValueInputs;
        [Size(255)]
        public string ValueInputs
        {
            get => _fValueInputs;
            set => SetPropertyValue(nameof(ValueInputs), ref _fValueInputs, value);
        }

        TargetType _fTargetType;
        public TargetType TargetType
        {
            get => _fTargetType;
            set => SetPropertyValue(nameof(TargetType), ref _fTargetType, value);
        }

        double _fTargetValue; //TODO:StepId?
        public double TargetValue
        {
            get => _fTargetValue;
            set => SetPropertyValue(nameof(TargetValue), ref _fTargetValue, value);
        }

        VarianceType _fVarianceType;
        public VarianceType VarianceType
        {
            get => _fVarianceType;
            set => SetPropertyValue(nameof(VarianceType), ref _fVarianceType, value);
        }

        double _fLoPercentage; //TODO:StepId?
        public double LoPercentage
        {
            get => _fLoPercentage;
            set => SetPropertyValue(nameof(LoPercentage), ref _fLoPercentage, value);
        }

        double _fLoTolerance; //TODO:StepId?
        public double LoTolerance
        {
            get => _fLoTolerance;
            set => SetPropertyValue(nameof(LoTolerance), ref _fLoTolerance, value);
        }

        double _fHiPercentage; //TODO:StepId?
        public double HiPercentage
        {
            get => _fHiPercentage;
            set => SetPropertyValue(nameof(HiPercentage), ref _fHiPercentage, value);
        }

        double _fHiTolerance; //TODO:StepId?
        public double HiTolerance
        {
            get => _fHiTolerance;
            set => SetPropertyValue(nameof(HiTolerance), ref _fHiTolerance, value);
        }

        string _fStepDescription;
        [Size(SizeAttribute.Unlimited)]
        public string StepDescription
        {
            get => _fStepDescription;
            set => SetPropertyValue(nameof(StepDescription), ref _fStepDescription, value);
        }

        string _fColumn1;
        [Size(255)]
        public string Column1
        {
            get => _fColumn1;
            set => SetPropertyValue(nameof(Column1), ref _fColumn1, value);
        }

        string _fColumn2;
        [Size(255)]
        public string Column2
        {
            get => _fColumn2;
            set => SetPropertyValue(nameof(Column2), ref _fColumn2, value);
        }

        string _fColumn3;
        [Size(255)]
        public string Column3
        {
            get => _fColumn3;
            set => SetPropertyValue(nameof(Column3), ref _fColumn3, value);
        }

        string _fColumn4;
        [Size(255)]
        public string Column4
        {
            get => _fColumn4;
            set => SetPropertyValue(nameof(Column4), ref _fColumn4, value);
        }

        string _fColumn5;
        [Size(255)]
        public string Column5
        {
            get => _fColumn5;
            set => SetPropertyValue(nameof(Column5), ref _fColumn5, value);
        }

        string _fColumn6;
        [Size(255)]
        public string Column6
        {
            get => _fColumn6;
            set => SetPropertyValue(nameof(Column6), ref _fColumn6, value);
        }

        OkPrintStyle _fOkPrintStyle;
        public OkPrintStyle OkPrintStyle
        {
            get => _fOkPrintStyle;
            set => SetPropertyValue(nameof(OkPrintStyle), ref _fOkPrintStyle, value);
        }

        LimitPrintStyle _fLimitPrintStyle;
        public LimitPrintStyle LimitPrintStyle
        {
            get => _fLimitPrintStyle;
            set => SetPropertyValue(nameof(LimitPrintStyle), ref _fLimitPrintStyle, value);
        }

        StepType _fStepType;
        public StepType StepType
        {
            get => _fStepType;
            set => SetPropertyValue(nameof(StepType), ref _fStepType, value);
        }

        #endregion

        public Check(Session session) : base(session) { }
    }
}
