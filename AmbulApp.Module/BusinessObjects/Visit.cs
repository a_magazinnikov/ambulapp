﻿using System;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessObjects
{
    [NavigationItem("Interventions")]
    [Appearance("Company_Editable", AppearanceItemType.ViewItem, "InstrumentsOnVisit.Count() > 0", Enabled = false, TargetItems = "Company")]
    [XafDefaultProperty("DisplayName")]
    public class Visit : BaseObject
    {
        [VisibleInDetailView(false)]
        public string DisplayName =>$"{Company} {Location}  {VisitDate.ToShortDateString()}";

        private Company _fCompany;
        [Association(@"VisitReferencesCompany"), VisibleInListView(true), ImmediatePostData, RuleRequiredField]
        public Company Company
        {
            get => _fCompany;
            set => SetPropertyValue(nameof(Company), ref _fCompany, value);
        }

        byte[] _fCustomerSignature;
        [Size(SizeAttribute.Unlimited)]
        [VisibleInListView(false)]
        public byte[] CustomerSignature
        {
            get => _fCustomerSignature;
            set => SetPropertyValue(nameof(CustomerSignature), ref _fCustomerSignature, value);
        }

        string _fCustomerSignerName;
        [VisibleInListView(false)]
        public string CustomerSignerName
        {
            get => _fCustomerSignerName;
            set => SetPropertyValue(nameof(CustomerSignerName), ref _fCustomerSignerName, value);
        }
        
        FileData _fLinkToAttachment;
        public FileData LinkToAttachment
        {
            get => _fLinkToAttachment;
            set => SetPropertyValue(nameof(LinkToAttachment), ref _fLinkToAttachment, value);
        }

        private Location _fLocation;
        [Association(@"VisitReferencesLocation"), DataSourceProperty("Company.Locations"), VisibleInListView(true)]
        public Location Location
        {
            get => _fLocation;
            set => SetPropertyValue(nameof(Location), ref _fLocation, value);
        }

        AmbulUser _fTechnician;
        [VisibleInListView(false), DataSourceCriteria("Iif(IsCurrentUserInRole('Super Admin Role'), Technician = 'true', Technician = 'true' And Oid = CurrentUserId())")]
        public AmbulUser Technician
        {
            get => _fTechnician;
            set => SetPropertyValue(nameof(Technician), ref _fTechnician, value);
        }

        [Size(SizeAttribute.Unlimited), VisibleInListView(false)]
        public byte[] TechnicianSignature => Technician?.Signature;

        DateTime _fVisitDate;

        [VisibleInListView(false)]
        [ModelDefault("DisplayFormat", "{0:dd/MM/yyyy}")]
        [ModelDefault("EditMask", "dd/MM/yyyy")]
        public DateTime VisitDate
        {
            get => _fVisitDate;
            set => SetPropertyValue<DateTime>(nameof(VisitDate), ref _fVisitDate, value);
        }

        [VisibleInLookupListView(true), VisibleInDetailView(false), VisibleInListView(true)]
        public int NumberOfVehicles => Interventions.Count(i => i.Equipment?.Family?.Type != null && i.Equipment.Family != null && i.Equipment.Family?.Type?.Key == "V");

        //TODO: check null reference exception in AmbulApp.Win
        [VisibleInLookupListView(true), VisibleInDetailView(false), VisibleInListView(true)]
        public int NumberOfEquipments => Interventions.Count(i => i.Equipment?.Family?.Type != null && i.Equipment.Family != null && i.Equipment.Family?.Type?.Key == "E");

//        [VisibleInLookupListView(true), VisibleInDetailView(false), VisibleInListView(true)]
//        public double TotalEstimatedTimeForTheVisit => Interventions.Where(i => i.MaintenancePlan != null).Sum(m => m.MaintenancePlan.EstimatedEffort);

        [Association(@"Equipment-Visit"), DataSourceCriteria("!IsNull(Family) and Family.Type.Key = 'A'")]
        public XPCollection<Equipment> InstrumentsOnVisit => GetCollection<Equipment>(nameof(InstrumentsOnVisit));

        [Association(@"InterventionReferencesVisit"), DevExpress.Xpo.Aggregated]
        public XPCollection<Intervention> Interventions => GetCollection<Intervention>(nameof(Interventions));

        [NonPersistent]
        [XafDisplayName("Equipments In the Location")]
        public XPCollection<EquipmentInLocation> EquipmentsInLocations => new XPCollection<EquipmentInLocation>(Session, CriteriaOperator.Parse($"LocationOid='{Location.Oid}'"));

        public Visit(Session session) : base(session)
        {
        }

        public override void AfterConstruction()
        {
            var user = Session.GetObjectByKey<AmbulUser>(SecuritySystem.CurrentUserId);
            this.Technician = user != null && user.Technician ? user : null;
            base.AfterConstruction();

        }
    }
}
