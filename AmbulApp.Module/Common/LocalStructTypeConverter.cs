﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AmbulApp.Module.Common
{
    public class LocalStructTypeConverter<T> : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(String))
                return true;

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string s)
            {
                object result = Activator.CreateInstance(typeof(T));
                string[] values = s.Split(';');
                foreach (string keyString in values)
                {
                    string propertyName = keyString.Remove(keyString.IndexOf('='));
                    string keyValue = keyString.Substring(keyString.IndexOf('=') + 1);
                    PropertyInfo info = typeof(T).GetProperty(propertyName);
                    if (info != null)
                    {
                        info.SetValue(result,
                            info.PropertyType.IsEnum
                                ? Enum.Parse(info.PropertyType, keyValue)
                                : Convert.ChangeType(keyValue, info.PropertyType), null);
                    }
                    else
                    {
                        FieldInfo fieldInfo = typeof(T).GetField(propertyName);
                        if (fieldInfo.FieldType.IsEnum)
                        {
                            fieldInfo.SetValue(result, Enum.Parse(fieldInfo.FieldType, keyValue));
                        }

                        fieldInfo.SetValue(result,
                            fieldInfo.FieldType == typeof(Guid)
                                ? Guid.Parse(keyValue)
                                : Convert.ChangeType(keyValue, fieldInfo.FieldType));
                    }
                }
                return result;

            }
            return base.ConvertFrom(context, culture, value);
        }

        public override Object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, Object value, Type destinationType)
        {
            if (destinationType == typeof(String))
            {
                string result = "";
                foreach (PropertyInfo property in typeof(T).GetProperties())
                {
                    result += property.Name + "=" + property.GetValue(value, null) + ";";
                }
                foreach (FieldInfo property in typeof(T).GetFields())
                {
                    result += property.Name + "=" + property.GetValue(value) + ";";
                }
                return result.TrimEnd(';');
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
