﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AmbulApp.Module.BusinessLogic
{
    public class EmailSettings
    {
        public EmailSettings()
        {
            IsHtml = true;
        }

        public String From { get; set; }

        public String To { get; set; }

        public String Subject { get; set; }

        public String Body { get; set; }

        public String CC { get; set; }

        public String BCC { get; set; }

        public List<Attachment> Attachments { get; set; }

        public bool IsHtml { get; set; }
    }
}
