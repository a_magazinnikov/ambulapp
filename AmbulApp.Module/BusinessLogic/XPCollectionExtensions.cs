﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;

namespace AmbulApp.Module.BusinessLogic
{
    // ReSharper disable once InconsistentNaming
    public static class XPCollectionExtensions
    {
        public static void RemoveAll<T>(this XPCollection<T> source)
        {
            try
            {
                var count = source.Count;
                for (var i = count - 1; i >= 0; i--)
                {
                    source.Remove(source[i]);
                }
            }
            catch (Exception e)
            {
                Tracing.Tracer.LogWarning("[XPCollectionExtensions] exception: {0}", e.Message);
            }
        }
    }
}
