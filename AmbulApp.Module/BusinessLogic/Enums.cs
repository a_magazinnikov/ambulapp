﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp.DC;

namespace AmbulApp.Module.BusinessLogic
{
    public enum OkType
    {
        [XafDisplayName("Please select")]
        DefaultValue = 0,
        [XafDisplayName("OK")]
        Ok,
        [XafDisplayName("KO")]
        Ko,
        [XafDisplayName("NA")]
        Na
    }

    public enum TargetType
    {
        [XafDisplayName("Please select")]
        DefaultValue = 0,
        Predefined,
        InputRuntime,
        FromOtherStep,
        InputRuntimeFromText
    }

    public enum VarianceType
    {
        [XafDisplayName("Please select")]
        DefaultValue = 0,
        Percentage,
        Tollerance,
        MinOfBoth,
        MaxOfBoth,
        FromOtherStep,
        [XafDisplayName("Percentage + % From Other Step")]
        PercentagePlusFromOtherStep
    }

    public enum OkPrintStyle
    {
        [XafDisplayName("Please select")]
        DefaultValue = 0,
        [XafDisplayName("OK/Not OK")]
        OkNotOk,
        [XafDisplayName("Yes/No")]
        YesNo,
        OnlyFlag
    }

    public enum LimitPrintStyle
    {
        [XafDisplayName("Please select")]
        DefaultValue = 0,
        [XafDisplayName("Target + Var")]
        TargetVar,
        [XafDisplayName("Min + Max")]
        MinMax
    }

    public enum StepTypeGroup
    {
        ManualOk = 0,
        AutomaticOk,
        NoNeedForOk,
        PrintOnly
    }
}
