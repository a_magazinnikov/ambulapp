﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DevExpress.DataProcessing;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;

namespace AmbulApp.Module.BusinessLogic
{
    public class EmailHelper
    {
        private static volatile EmailHelper instance;

        private static object syncRoot = new object();

        private EmailHelper()
        {
        }

        public static EmailHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new EmailHelper();
                        }
                    }
                }

                return instance;
            }
        }

        public bool SendEmail(EmailSettings settings)
        {
            bool sendSuccess = false;

            using (var mailMessage = new MailMessage())
            {
                if (!String.IsNullOrEmpty(settings.To) && IsValidEmail(settings.To))
                {
                    mailMessage.Subject = settings.Subject;
                    FillAddresses(mailMessage.To, settings.To);
                    if (!String.IsNullOrEmpty(settings.CC) && IsValidEmail(settings.CC))
                        FillAddresses(mailMessage.CC, settings.CC);
                    if (!String.IsNullOrEmpty(settings.BCC) && IsValidEmail(settings.BCC))
                        FillAddresses(mailMessage.Bcc, settings.BCC);
                    if (!String.IsNullOrEmpty(settings.From) && IsValidEmail(settings.From))
                        mailMessage.From = new MailAddress(settings.From);
                    mailMessage.Body = settings.Body;
                    mailMessage.IsBodyHtml = settings.IsHtml;
                    mailMessage.Attachments.AddRange(settings.Attachments ?? new List<Attachment>());
                    sendSuccess = SendWithRetries(mailMessage);
                }
            }
            return sendSuccess;
        }

        private void FillAddresses(MailAddressCollection mailAddressCollection, string email)
        {
            var parts = email.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var part in parts)
            {
                mailAddressCollection.Add(new MailAddress(part.Trim()));
            }
        }

        private bool SendWithRetries(MailMessage mailMessage)
        {
            const int retryCount = 4;
            int retry = 0;
            while (retry < retryCount)
            {
                retry++;
                try
                {
                    using (var smtpClient = new SmtpClient())
                    {
                        smtpClient.Send(mailMessage);
                        return true;
                    }
                }
                catch (SmtpException ex)
                {
                    Tracing.Tracer.LogWarning("[Send Email] SMTP exception with status code {0}.", ex.StatusCode);
                    Tracing.Tracer.LogError(ex.Message);
                    Thread.Sleep(5000);
                }
            }
            return false;
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                var parts = email.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 0 || (from part in parts let addr = new MailAddress(part.Trim()) where addr.Address != part.Trim() select part).Any())
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool SendRegistrationEmail(string username, string email)
        {
            var url = "http://94.177.182.171/AmbulApp";
            return SendEmail(new EmailSettings
            {
                To = email,
                Subject = "Welcome to Ambul App",
                Body = String.Format(@"<p>Hello,</p>
                    <p>Please click on the link below to login to your profile.</p>
                    <p>Use your email address as your Username and the empty password until you have updated your profile.</p>
                    <p>Username: {0}</p>
                    <p>url: <a href='{1}'>{1}</a></p>
                    <p>Thank you,</p>", username, url)
            });
        }

        private String ToHtml(String data)
        {
            if (!String.IsNullOrEmpty(data))
            {
                var lines = data.Split(new[] { Environment.NewLine }, StringSplitOptions.None).ToList();
                for (int index = 0; index < lines.Count; index++)
                {
                    lines[index] = "<p>" + lines[index] + "</p>";
                }
                return String.Concat(lines);
            }
            return String.Empty;
        }
    }
}
