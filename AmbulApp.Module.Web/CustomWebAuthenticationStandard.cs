﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;

namespace AmbulApp.Module.Web
{
    public class CustomWebAuthenticationStandard : AuthenticationStandard
    {
        public override object Authenticate(IObjectSpace objectSpace)
        {
            if (this.LogonParameters is AuthenticationStandardLogonParameters authenticationStandardLogonParameters)
            {
                var user = objectSpace.FindObject<AmbulUser>(CriteriaOperator.Parse($"UserName == '{authenticationStandardLogonParameters.UserName}'"));
                if (user != null)
                {
                    return user.Technician ? throw new AuthenticationException(user.UserName) : base.Authenticate(objectSpace);
                }
            }

            return base.Authenticate(objectSpace);
        }
    }
}
