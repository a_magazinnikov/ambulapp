﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web;

namespace AmbulApp.Module.Web.Editors
{
    [PropertyEditor(typeof(DateTime), false)]
    public class CustomDateTimeEditor : ASPxDateTimePropertyEditor
    {
        public CustomDateTimeEditor(Type objectType, IModelMemberViewItem info) :
            base(objectType, info)
        {
        }

        protected override void SetupControl(WebControl control)
        {
            base.SetupControl(control);
            if (ViewEditMode == ViewEditMode.Edit)
            {
                ASPxDateEdit dateEdit = (ASPxDateEdit) control;
                var script = "function(s, e) {" +
                             "   ASPxClientUtils.AttachEventToElement(s.GetInputElement(), 'click', function(event) {" +
                             "        s.ShowDropDown();" +
                             "    });" +
                             "}";
                dateEdit.ClientSideEvents.Init = script;
                //                dateEdit.ClientSideEvents.GotFocus = "function(s, e) { s.ShowDropDown(); }";
            }
        }
    }
}
