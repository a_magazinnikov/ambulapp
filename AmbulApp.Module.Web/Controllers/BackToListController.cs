﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.Base;
using System;
using System.Linq;

namespace AmbulApp.Module.Web.Controllers
{
    public class BackToListController : ViewController
    {
        public BackToListController()
        {

            SimpleAction backToList =
                new SimpleAction(this, "BackToList", PredefinedCategory.View) {Caption = "Back to list"};
            backToList.Execute += BackToList_Execute;
            this.TargetViewType = ViewType.DetailView;
            this.TargetViewNesting = Nesting.Root;
        }

        void BackToList_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            Frame.GetController<NavigationBackController>().NewNavigateBack.DoExecute();
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
