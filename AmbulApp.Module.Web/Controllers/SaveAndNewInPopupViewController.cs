﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

namespace AmbulApp.Module.Web.Controllers
{
    //Todo:remove it if CustomShowViewStrategy works properly for showing pop-up window in main window
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SaveAndNewInPopupViewController : ViewController
    {
        public SaveAndNewInPopupViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void MySaveAndNew_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (View.ObjectSpace.IsModified)
            {
                View.ObjectSpace.CommitChanges();
            }

            View.Close(false);
//            DetailView parentDetailView = (DetailView)Application.MainWindow.View;
            var frameRoudMap = Application.MainWindow.GetController<FrameRoudMapController>();
            var parentFrame = frameRoudMap.ParentFrame;
            if (parentFrame != null)
            {
                var targetNewController = parentFrame.GetController<NewObjectViewController>();
                var targetNewItem = targetNewController.NewObjectAction.Items[0];
                targetNewController.NewObjectAction.DoExecute(targetNewItem);
            }
        }

        private void MySave_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (View.ObjectSpace.IsModified)
            {
                View.ObjectSpace.CommitChanges();
            }
        }
    }
}
