﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Web.Templates.ActionContainers;

namespace AmbulApp.Module.Web.Controllers
{
    public class ToolbarCustomizationController : WindowController
    {
        public ToolbarCustomizationController()
        {
            TargetWindowType = WindowType.Main;
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            Frame.ProcessActionContainer += Frame_ProcessActionContainer;
        }
        void Frame_ProcessActionContainer(object sender, ProcessActionContainerEventArgs e)
        {
            if (e.ActionContainer.ContainerId == "Save" && e.ActionContainer is WebActionContainer container)
            {
                container.IsDropDown = false;
            }
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            Frame.ProcessActionContainer -= Frame_ProcessActionContainer;
        }
    }
}
