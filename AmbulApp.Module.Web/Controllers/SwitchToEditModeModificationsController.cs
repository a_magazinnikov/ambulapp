﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using System;
using System.Linq;

namespace AmbulApp.Module.Web.Controllers
{
    public class SwitchToEditModeModificationsController : ViewController<DetailView>
    {
        protected override void OnActivated()
        {
            base.OnActivated();


            if (View.ViewEditMode == ViewEditMode.View)
            {
                View.ViewEditMode = ViewEditMode.Edit;
            }
        }
    }
}
