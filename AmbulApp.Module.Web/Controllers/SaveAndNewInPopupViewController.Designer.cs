﻿namespace AmbulApp.Module.Web.Controllers
{
    partial class SaveAndNewInPopupViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MySaveAndNew = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.MySave = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // MySaveAndNew
            // 
            this.MySaveAndNew.Caption = "Save And New";
            this.MySaveAndNew.Category = "PopupActions";
            this.MySaveAndNew.ConfirmationMessage = null;
            this.MySaveAndNew.Id = "MySaveAndNewId";
            this.MySaveAndNew.TargetObjectType = typeof(AmbulApp.Module.BusinessObjects.MaintenanceStep);
            this.MySaveAndNew.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.MySaveAndNew.ToolTip = null;
            this.MySaveAndNew.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.MySaveAndNew.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.MySaveAndNew_Execute);
            // 
            // MySave
            // 
            this.MySave.Caption = "Save";
            this.MySave.Category = "PopupActions";
            this.MySave.ConfirmationMessage = null;
            this.MySave.Id = "MySaveId";
            this.MySave.TargetObjectType = typeof(AmbulApp.Module.BusinessObjects.MaintenanceStep);
            this.MySave.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.MySave.ToolTip = null;
            this.MySave.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.MySave.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.MySave_Execute);
            // 
            // SaveAndNewInPopupViewController
            // 
            this.Actions.Add(this.MySaveAndNew);
            this.Actions.Add(this.MySave);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction MySaveAndNew;
        private DevExpress.ExpressApp.Actions.SimpleAction MySave;
    }
}
