﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Web.SystemModule;

namespace AmbulApp.Module.Web.Controllers
{
    public class CustomNewObjectViewController : WebNewObjectViewController
    {
        protected override void New(DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventArgs args)
        {
            if (Frame is NestedFrame)
            {
                FrameRoudMapController frameRoudMap = Application.MainWindow.GetController<FrameRoudMapController>();
                frameRoudMap.AddFrame(Frame);
            }
            base.New(args);
        }
    }
}
