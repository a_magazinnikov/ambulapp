﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Web.Templates.ActionContainers;
using DevExpress.Web;

namespace AmbulApp.Module.Web.Controllers
{
    public class WebCustomizeNavBarController : WindowController
    {
        private ShowNavigationItemController _navigationItemController;

        public WebCustomizeNavBarController()
        {
            TargetWindowType = WindowType.Main;
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            _navigationItemController = Frame.GetController<ShowNavigationItemController>();
            if (_navigationItemController != null)
            {
                _navigationItemController.ShowNavigationItemAction.CustomizeControl +=
                    ShowNavigationItemAction_CustomizeControl;
                _navigationItemController.CustomShowNavigationItem += NavigationItemController_CustomShowNavigationItem;
            }
        }

        private void NavigationItemController_CustomShowNavigationItem(object sender, CustomShowNavigationItemEventArgs e)
        {
            _navigationItemController.CustomShowNavigationItem -= NavigationItemController_CustomShowNavigationItem; //It is important to unsubscribe from this event immediately.
            if (SecuritySystem.CurrentUser is AmbulUser user && !(user.SuperAdmin || user.Technician) && user.Companies.Count > 1)
            {
                var showViewParameters = e.ActionArguments.ShowViewParameters;
                var objectSpace = Application.CreateObjectSpace();
                var settings = objectSpace.FindObject<ChangeMyCompany>(null);
                var detailView = Application.CreateDetailView(objectSpace, settings, true);
                detailView.ViewEditMode = ViewEditMode.Edit;
                detailView.Caption = "Choose My Company";
                if (detailView.CurrentObject is ChangeMyCompany changeMyCompany)
                    changeMyCompany.Company = null;
                showViewParameters.CreatedView = detailView;
            }

            e.Handled = true;
        }

        private void ShowNavigationItemAction_CustomizeControl(object sender,
            DevExpress.ExpressApp.Actions.CustomizeControlEventArgs e)
        {
            if (e.Control is ASPxTreeView mainTreeView)
            {
                mainTreeView.ExpandAll();
                if (SecuritySystem.CurrentUser is AmbulUser user &&
                    (user.SuperAdmin || user.Technician || user.Companies.Count == 1))
                {
                    var viewNode = mainTreeView.Nodes.FirstOrDefault(n => n.Name.Equals("User settings"))?.Nodes
                        .FirstOrDefault(n => n.Name.Equals("User settings/ChangeMyCompanyId"));
                    if (viewNode != null)
                        viewNode.Visible = false;
                }
            }
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            _navigationItemController = Frame.GetController<ShowNavigationItemController>();
            if (_navigationItemController != null)
            {
                _navigationItemController.ShowNavigationItemAction.CustomizeControl -=
                    ShowNavigationItemAction_CustomizeControl;
                _navigationItemController.CustomShowNavigationItem -= NavigationItemController_CustomShowNavigationItem;
            }
        }
    }
}

