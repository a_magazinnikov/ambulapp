﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessLogic;
using AmbulApp.Module.BusinessObjects;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web;

namespace AmbulApp.Module.Web.Controllers
{
    public class ManageEnumValuesController : ViewController<DetailView>
    {
        public ManageEnumValuesController()
        {
            TargetObjectType = typeof(MaintenanceStep);
        }

        private ASPxEnumPropertyEditor _enumPropertyEditor;

        protected override void OnActivated()
        {
            base.OnActivated();
            _enumPropertyEditor = View.FindItem("TargetType") as ASPxEnumPropertyEditor;
            if (_enumPropertyEditor != null)
            {
                _enumPropertyEditor.ControlCreated += enumPropertyEditor_ControlCreated;
            }

            if (View.FindItem("StepType") is PropertyEditor propertyEditor)
            {
                propertyEditor.ControlValueChanged += PropertyEditorOnControlValueChanged;
                propertyEditor.ControlCreated += propertyEditor_ControlCreated;
            }
        }

        private void propertyEditor_ControlCreated(object sender, EventArgs e)
        {
            //            var propertyValue = ((PropertyEditor)sender).PropertyValue;
            //            ManageEnumValues();
        }

        private void PropertyEditorOnControlValueChanged(object sender, EventArgs eventArgs)
        {
            var controlValue = ((PropertyEditor)sender).ControlValue;
            ManageEnumValues((StepType)controlValue);
        }

        protected override void OnDeactivated()
        {
            if (_enumPropertyEditor != null)
            {
                _enumPropertyEditor.ControlCreated -= enumPropertyEditor_ControlCreated;
                _enumPropertyEditor = null;
            }
            base.OnDeactivated();
        }

        private void enumPropertyEditor_ControlCreated(object sender, EventArgs e)
        {
            if (_enumPropertyEditor.Editor != null)
            {
                _enumPropertyEditor.Editor.Init += Editor_Init;
            }
        }

        private void Editor_Init(object sender, EventArgs eventArgs)
        {
            ManageEnumValues();
        }

        private void ManageEnumValues(StepType stepTypeCode = null)
        {
            ASPxComboBox comboBox = _enumPropertyEditor.Editor;
            if (View.CurrentObject is MaintenanceStep maintenanceStep)
            {
                if (maintenanceStep.StepType != null || stepTypeCode != null)
                {
                    var code = stepTypeCode != null ? stepTypeCode.Code : maintenanceStep.StepType.Code;
                    if (comboBox != null)
                    {
                        var item = comboBox.Items.FindByValue(TargetType.InputRuntimeFromText);
                        if (code == "A01" || code == "A03")
                        {
                            if (item != null) comboBox.Items.Remove(item);
                        }
                        else if (code == "A02" || code == "A04")
                        {
                            if (item == null) comboBox.Items.Add(new ListEditItem("Input runtime from text", TargetType.InputRuntimeFromText));
                        }
                    }
                }
            }
        }
    }
}
