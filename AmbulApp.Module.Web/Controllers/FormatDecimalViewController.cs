﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web;

namespace AmbulApp.Module.Web.Controllers
{
    public class FormatDecimalViewController : ViewController<DetailView>
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            foreach (var item in View.GetItems<ASPxDecimalPropertyEditor>())
            {
                if (item.Editor != null)
                {
                    CustomizeEditor(item);
                }
                else
                {
                    item.ControlCreated += (s, e) => { CustomizeEditor((ASPxDecimalPropertyEditor) s); };
                }
            }
        }

        private void CustomizeEditor(ASPxDecimalPropertyEditor propertyEditor)
        {
            if (propertyEditor.ViewEditMode == ViewEditMode.Edit)
            {
                var spinEdit = propertyEditor.Editor;
                if (spinEdit != null)
                {
                    spinEdit.DecimalPlaces = 2;
                    // Optional customization to show 1.50 even for 1.5. See https://www.devexpress.com/Support/Center/Question/Details/T159206 for more details.
                    spinEdit.DisplayFormatString = propertyEditor.DisplayFormat;
                }
            }
        }
    }
}
