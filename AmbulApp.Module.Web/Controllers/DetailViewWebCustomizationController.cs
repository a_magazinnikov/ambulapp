﻿using System;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Web.Editors.ASPx;
using DevExpress.Web;

namespace AmbulApp.Module.Web.Controllers
{
    public class DetailViewWebCustomizationController : ViewController<DetailView>
    {
        readonly SimpleAction _closeViewAction;

        public DetailViewWebCustomizationController()
        {
//            TargetViewType = ViewType.DetailView;
            _closeViewAction =
                new SimpleAction(this, "Web.Close", DevExpress.Persistent.Base.PredefinedCategory.Edit)
                {
                    Caption = "Close"
                };
            _closeViewAction.Execute += CloseViewAction_Execute;
        }

        void CloseViewAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            this.View.Close();
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            foreach (ASPxIntPropertyEditor propertyEditor in View.GetItems<ASPxIntPropertyEditor>())
            {
                propertyEditor.ControlCreated += PropertyEditorControlCreated;
            }
        }

        void PropertyEditorControlCreated(object sender, EventArgs e)
        {
            ASPxIntPropertyEditor propertyEditor = (ASPxIntPropertyEditor) sender;
            if (propertyEditor.Editor != null)
            {
                ((ASPxSpinEdit) propertyEditor.Editor).SpinButtons.ShowIncrementButtons = false;
                ((ASPxSpinEdit) propertyEditor.Editor).AllowMouseWheel = false;
            }
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            foreach (ASPxIntPropertyEditor propertyEditor in View.GetItems<ASPxIntPropertyEditor>())
            {
                propertyEditor.ControlCreated -= PropertyEditorControlCreated;
            }
        }
    }
}
