﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;

namespace AmbulApp.Module.Web.Controllers
{
    public class FrameRoudMapController : WindowController
    {
        private Stack<Frame> _framesMap = new Stack<Frame>();

        public void AddFrame(Frame frame)
        {
            if (!_framesMap.Contains(frame))
            {
                _framesMap.Push(frame);
                frame.Disposed += frame_Disposed;
            }
        }

        public Frame ParentFrame => _framesMap.Peek();

        private void frame_Disposed(object sender, EventArgs e)
        {
            Frame frame = _framesMap.Pop();
            frame.Disposed -= frame_Disposed;
        }
    }
}
