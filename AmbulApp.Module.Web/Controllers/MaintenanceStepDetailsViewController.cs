﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;

namespace AmbulApp.Module.Web.Controllers
{
    public class MaintenanceStepDetailsViewController : ObjectViewController<DetailView, MaintenanceStep>
    {
        readonly SimpleAction _closeViewAction;

        public MaintenanceStepDetailsViewController()
        {
            TargetViewNesting = Nesting.Root;
            _closeViewAction =
                new SimpleAction(this, "Web.MaintenanceStep.Close", DevExpress.Persistent.Base.PredefinedCategory.Edit)
                {
                    Caption = "Close"
                };
            _closeViewAction.Execute += CloseViewAction_Execute;
        }

        void CloseViewAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace(typeof(MaintenancePlan));
            if (View.CurrentObject is MaintenanceStep maintenanceStep)
            {
                var objectToShow = objectSpace.GetObjectByKey<MaintenancePlan>(maintenanceStep.Plan.Oid);
                {
                    var createdView = Application.CreateDetailView(objectSpace, objectToShow, true);
                    Frame.SetView(createdView);
                    ShowNavigationItemController controller = Application.MainWindow.GetController<ShowNavigationItemController>();
                    controller?.UpdateSelectedItem(createdView);
                }
            }
        }
    }
}
