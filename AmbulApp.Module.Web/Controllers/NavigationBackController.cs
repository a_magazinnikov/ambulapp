﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Web.SystemModule;
using DevExpress.Persistent.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmbulApp.Module.Web.Controllers
{
    public  class NavigationBackController : WebViewNavigationController
    {
        SimpleAction newNavigateBack;

        public SimpleAction NewNavigateBack { get => newNavigateBack; set => newNavigateBack = value; }

        public NavigationBackController()
        {
           
            NewNavigateBack = new SimpleAction(this, "NewNavigateBack", "HiddenAction");
            NewNavigateBack.Caption = "Back to list";
            NewNavigateBack.Execute += NavigateBack_Execute;
            NewNavigateBack.TargetViewType = ViewType.DetailView;
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        private void NavigateBack_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (HistoryItem<ViewShortcut> viewShortcut in navigationHistory.Reverse())
            {
                var View = this.Application.Model.Views.Where(v => v.Id == viewShortcut.Item.ViewId).FirstOrDefault();
                if (View != null)
                {
                    if ((View as IModelListView) != null)
                    {
                        this.NavigateTo(viewShortcut.Item);
                        return;
                    }
                }

            }

        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
