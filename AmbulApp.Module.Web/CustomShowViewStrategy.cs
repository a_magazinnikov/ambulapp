﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Web;

namespace AmbulApp.Module.Web
{
    public class CustomShowViewStrategy : ShowViewStrategy
    {
        public CustomShowViewStrategy(XafApplication application)
            : base(application)
        {
        }

        protected override void ShowViewFromNestedView(ShowViewParameters parameters, ShowViewSource showViewSource)
        {
            var webWin = this.GetCurrentWindow(showViewSource);
            var isPopupWindow = webWin is PopupWindow;

            if (!isPopupWindow && parameters.CreatedView.Id.Equals("MaintenanceStep_DetailView"))
            {
                this.Application.MainWindow.SetView(parameters.CreatedView, showViewSource.SourceFrame);
            }
            else
            {
                base.ShowViewFromNestedView(parameters, showViewSource);
            }
        }
    }
}
