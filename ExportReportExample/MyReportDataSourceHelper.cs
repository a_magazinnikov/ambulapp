﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.Persistent.Base.ReportsV2;

namespace ExportReportExample
{
    public class MyReportDataSourceHelper : ReportDataSourceHelper
    {
        readonly IObjectSpaceProvider _objectSpaceProvider;

        public MyReportDataSourceHelper(IObjectSpaceProvider objectSpaceProvider)
            : base(null)
        {
            this._objectSpaceProvider = objectSpaceProvider;
        }

        protected override IReportObjectSpaceProvider CreateReportObjectSpaceProvider()
        {
            return new MyReportObjectSpaceProvider(_objectSpaceProvider);
        }
    }
}
