﻿using System.Configuration;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.DC.Xpo;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraReports.UI;
using AmbulApp.Module.BusinessObjects;
using DevExpress.Xpf.Core.Native;


namespace ExportReportExample
{
    static class Program
    {
        static void Main(string[] args)
        {
            using (XPObjectSpaceProvider objectSpaceProvider = CreateObjectSpaceProvider())
            {
                IObjectSpace objectSpace = objectSpaceProvider.CreateObjectSpace();
                ReportDataV2 reportData = objectSpace.FindObject<ReportDataV2>(new BinaryOperator("DisplayName", "CheckList Report"));
                ExportReport(reportData, objectSpaceProvider);
            }
        }

        private static void ExportReport(IReportDataV2 reportData, IObjectSpaceProvider objectSpaceProvider)
        {
            XtraReport report = ReportDataProvider.ReportsStorage.LoadReport(reportData);
            MyReportDataSourceHelper reportDataSourceHelper = new MyReportDataSourceHelper(objectSpaceProvider);
            ReportDataProvider.ReportObjectSpaceProvider = new MyReportObjectSpaceProvider(objectSpaceProvider);
            reportDataSourceHelper.SetupBeforePrint(report);
            report.ExportToPdf("CheckListReport.pdf");
        }

        private static void RegisterBoTypes(ITypesInfo typesInfo)
        {
            typesInfo.RegisterEntity(typeof(Visit));
            typesInfo.RegisterEntity(typeof(ReportDataV2));
        }

        private static XPObjectSpaceProvider CreateObjectSpaceProvider()
        {
            XpoTypesInfoHelper.ForceInitialize();
            ITypesInfo typesInfo = XpoTypesInfoHelper.GetTypesInfo();
            XpoTypeInfoSource xpoTypeInfoSource = XpoTypesInfoHelper.GetXpoTypeInfoSource();
            RegisterBoTypes(typesInfo);
            ConnectionStringDataStoreProvider dataStoreProvider = new ConnectionStringDataStoreProvider(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            return new XPObjectSpaceProvider(dataStoreProvider, typesInfo, xpoTypeInfoSource);
        }
    }
}
