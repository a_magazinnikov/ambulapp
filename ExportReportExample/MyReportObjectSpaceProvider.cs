﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.ReportsV2;
using DevExpress.Persistent.Base.ReportsV2;

namespace ExportReportExample
{
    public class MyReportObjectSpaceProvider : IReportObjectSpaceProvider, IObjectSpaceCreator
    {
        readonly IObjectSpaceProvider _objectSpaceProvider;
        IObjectSpace _objectSpace;

        public MyReportObjectSpaceProvider(IObjectSpaceProvider objectSpaceProvider)
        {
            this._objectSpaceProvider = objectSpaceProvider;
        }

        public void DisposeObjectSpaces()
        {
            if (_objectSpace != null)
            {
                _objectSpace.Dispose();
                _objectSpace = null;
            }
        }

        public IObjectSpace GetObjectSpace(Type type)
        {
            return _objectSpace ?? (_objectSpace = _objectSpaceProvider.CreateObjectSpace());
        }

        public IObjectSpace CreateObjectSpace(Type type)
        {
            return _objectSpaceProvider.CreateObjectSpace();
        }
    }
}
